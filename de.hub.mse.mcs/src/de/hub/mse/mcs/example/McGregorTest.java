package de.hub.mse.mcs.example;

import de.hub.mse.mcs.algorithm.MCSAdapter;
import de.hub.mse.mcs.util.GraphViewer;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedPseudograph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple example implementation of the usage of the MCSAdatper for two JGraphT graphs. 
 * <br><br>
 * This class creates two instances of the same graph with two cliques of three nodes each, and two isolated nodes. 
 * Afterwards, the MCS and MCCS of the two graphs is calculated by calling the MCSAdapter, the the graphs are shown
 * with the help of the GraphViewer. 
 *
 */
public class McGregorTest {;
	public static void main (String... args) throws Exception {
		
		
		/*DirectedPseudograph<SimpleLabeledNode,SimpleLabeledNode> graphA = getGraph();
		DirectedPseudograph<SimpleLabeledNode,SimpleLabeledNode> graphTemp = getGraph();

		EdgeFactory<SimpleLabeledNode, SimpleLabeledNode> factory = (arg0, arg1) ->
				new SimpleLabeledNode("_to_");

		DirectedPseudograph<SimpleLabeledNode, SimpleLabeledNode> graphB = new DirectedPseudograph<>(factory);

		List<SimpleLabeledNode> nodes = new ArrayList<>(graphTemp.vertexSet());
		List<SimpleLabeledNode> edges = new ArrayList<>(graphTemp.edgeSet());

		Collections.shuffle(nodes);
		Collections.shuffle(edges);

		for (SimpleLabeledNode n : nodes) {
			graphB.addVertex(n);
		}

		for (SimpleLabeledNode e : edges) {
			graphB.addEdge(graphTemp.getEdgeSource(e), graphTemp.getEdgeTarget(e), e);
		}

		show(graphA, "A");
		show(graphB, "B");

		MCSAdapter<SimpleLabeledNode, SimpleLabeledNode> adapter = new MCSAdapter<>(graphA, graphB, new SimpleCandidateChecker(), 60_000);

		Graph<SimpleLabeledNode, SimpleLabeledNode> mcs = adapter.getMCS();
		Graph<SimpleLabeledNode, SimpleLabeledNode> mccs = adapter.getMCCS();

		show(mcs, "MCS");
		show(mccs, "MCCS");

		System.out.println("MCS Node Mapping");
		adapter.getMCSNodeMapping().forEach(System.out::println);
		System.out.println("MCS Edge Mapping");
		adapter.getMCSEdgeMapping().forEach(System.out::println);
		System.out.println("MCCS Node Mapping");
		adapter.getMCCSNodeMapping().forEach(System.out::println);
		System.out.println("MCCS Edge Mapping");
		adapter.getMCCSEdgeMapping().forEach(System.out::println);*/
	}

	private static DirectedPseudograph<SimpleLabeledNode, SimpleLabeledNode> getGraph() {
		/*EdgeFactory<SimpleLabeledNode, SimpleLabeledNode> factory = (arg0, arg1) ->
				new SimpleLabeledNode(arg0.getGroupLabel() + "_to_" + arg1.getGroupLabel());*/
		EdgeFactory<SimpleLabeledNode, SimpleLabeledNode> factory = (arg0, arg1) ->
				new SimpleLabeledNode("_to_");

		DirectedPseudograph<SimpleLabeledNode, SimpleLabeledNode> graph = new DirectedPseudograph<>(factory);

		ArrayList<SimpleLabeledNode> nodeList = new ArrayList<>();

		addNode(nodeList, graph, "RemoveReference");
		addNode(nodeList, graph, "EReference");
		addNode(nodeList, graph, "RemoveReference");

		addNode(nodeList, graph, "Feature");
		addNode(nodeList, graph, "Correspondence");
		addNode(nodeList, graph, "Feature");

		addNode(nodeList, graph, "RemoveObject");
		addNode(nodeList, graph, "Group");
		addNode(nodeList, graph, "RemoveReference");

		addNode(nodeList, graph, "SymmetricDifference");
		addNode(nodeList, graph, "RemoveReference");
		addNode(nodeList, graph, "FeatureModel");

		addNode(nodeList, graph, "EReference");
		addNode(nodeList, graph, "EReference");
		addNode(nodeList, graph, "Feature");

		addNode(nodeList, graph, "RemoveReference");
		addNode(nodeList, graph, "Correspondence");
		addNode(nodeList, graph, "Feature");

		addEdge(nodeList, graph, 0, 1);
		addEdge(nodeList, graph, 2, 1);
		addEdge(nodeList, graph, 2, 3);
		addEdge(nodeList, graph, 4, 3);
		addEdge(nodeList, graph, 4, 5);

		addEdge(nodeList, graph, 0, 7);
		addEdge(nodeList, graph, 0, 14);
		addEdge(nodeList, graph, 2, 7);
		addEdge(nodeList, graph, 3, 7);
		addEdge(nodeList, graph, 7, 3);

		addEdge(nodeList, graph, 8, 3);
		addEdge(nodeList, graph, 6, 7);
		addEdge(nodeList, graph, 8, 7);
		addEdge(nodeList, graph, 8, 12);
		addEdge(nodeList, graph, 10, 7);

		addEdge(nodeList, graph, 11, 7);
		addEdge(nodeList, graph, 10, 11);
		addEdge(nodeList, graph, 10, 13);
		addEdge(nodeList, graph, 7, 14);
		addEdge(nodeList, graph, 14, 7);

		addEdge(nodeList, graph, 15, 7);
		addEdge(nodeList, graph, 15, 14);
		addEdge(nodeList, graph, 15, 12);
		addEdge(nodeList, graph, 16, 14);
		addEdge(nodeList, graph, 17, 16);

		return graph;
	}

	private static void addNode(ArrayList<SimpleLabeledNode> nodeList, DirectedPseudograph<SimpleLabeledNode, SimpleLabeledNode> graph,
						 String label) {
		SimpleLabeledNode node = new SimpleLabeledNode(label);
		nodeList.add(node);
		graph.addVertex(node);
	}

	private static void addEdge(ArrayList<SimpleLabeledNode> nodeList, DirectedPseudograph<SimpleLabeledNode, SimpleLabeledNode> graph,
						 int idSrc, int idTarget) {
		graph.addEdge(nodeList.get(idSrc), nodeList.get(idTarget));
	}
	
	private static void show(Graph<SimpleLabeledNode, SimpleLabeledNode> graph, String name) {
		GraphViewer.viewGraph(graph, name);
	}
	
}
