package de.hub.mse.mcs.example;

import de.hub.mse.mcs.algorithm.CandidateChecker;

/**
 * Simple implementation of the CandidateChecker interface for SimpleLabeledNode. This CandidateChecker is used in McGregorTest.java
 *
 */
class SimpleCandidateChecker implements CandidateChecker<SimpleLabeledNode, SimpleLabeledNode>{

	@Override
	public boolean checkNodes(SimpleLabeledNode vertex1, SimpleLabeledNode vertex2) {
		return vertex1.getGroupLabel().equals(vertex2.getGroupLabel()) ;
	}

	@Override
	public boolean checkEdges(SimpleLabeledNode edge1, SimpleLabeledNode edge2) {
		return edge1.getGroupLabel().equals(edge2.getGroupLabel()) ;
	}

}
