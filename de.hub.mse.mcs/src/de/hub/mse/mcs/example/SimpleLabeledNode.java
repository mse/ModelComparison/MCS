package de.hub.mse.mcs.example;

import de.hub.mse.mcs.data.LabeledObject;

/**
 * Simple implementation of the LabeledObject interface to be used in McGregorTest.java
 */
public class SimpleLabeledNode implements LabeledObject {
	private final String label;
	
	
	public SimpleLabeledNode(String label) {
		this.label = label;
	}
	
	@Override
	public String getGroupLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}
}
