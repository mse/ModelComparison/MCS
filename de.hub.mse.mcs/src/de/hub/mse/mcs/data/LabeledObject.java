package de.hub.mse.mcs.data;

/**
 * The LabeledObject Interface has to be implemented by any class representing nodes in graphs for which the MCS should be calculated. 
 * <br><br>
 * The label is used by the McGregor Algorithm to speed up the matching of nodes. Only nodes with the same group label are checked for their compatibility, 
 * therefore it is essential that compatible nodes have the same label. 
 * <br>It is possible that two nodes with the same label are not compatible. In the MCS algorithm a CandidateChecker is responsible for the final decision 
 * of whether two nodes can be matched.
 *
 */
public interface LabeledObject {

	String getGroupLabel();
}

