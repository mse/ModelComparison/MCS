package de.hub.mse.mcs.data;

/**
 * Default interface for the LabeledObject interface. This interface should be used instead of LabeledObject, 
 * if the nodes in the graphs can not be grouped by a specific label.  
 * <br><br>
 * The label is used by the McGregor Algorithm to speed up the matching of nodes. 
 * Only nodes with the same label are checked for their compatibility, 
 * therefore it is essential that compatible nodes have the same label. 
 * <br>The DefaultLabeledNode interface assigns all nodes the same label "default". 
 * Therefore, all nodes are compared to each other during the MCS algorithm, due to belonging to the same 
 * group, and no speed-up is achieved. 
 *
 */
public interface DefaultLabeledNode extends LabeledObject{
	@Override
	default String getGroupLabel() {
		return "default";
	}
}
