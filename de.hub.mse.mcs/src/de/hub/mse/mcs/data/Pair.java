package de.hub.mse.mcs.data;

/**
 * A pair for storing two objects that stand in relation to each other, where both objects have the same class. 
 * Pairs are used to store graph elements that have been matched to each other by the MCS algorithm. 
 * <br><br>
 * For example, the MCS algorithm searches for the MCS of a graph A and a graph B. The first element of a pair 
 * might then contain a node v1 from A and the second element a node v2 from B that has been matched to v1 
 * by the MCS algorithm. 
 * 
 * @author Code: Ruud Welling. A Performance Analysis on Maximal Common Subgraph
Algorithms. 2011 <br> JavaDoc: Alexander Schultheiss, HU-Berlin. 2019
 *
 * @param <E> - The Class of the objects that are to be stored in a Pair object
 */
 public class Pair<E>{
	
	private E e1, e2;
	
	/**
	 * Create a new Pair object that contains the two given objects. 
	 * @param v1 The first element of the pair
	 * @param v2 The second element of the pair
	 */
	public Pair(E v1, E v2){
		this.e1 = v1;
		this.e2 = v2;
	}
	
	/**
	 * Return the first element of the pair. In case of our implementation of the MCS algorithm, this is always an element of the first graph given to the MCSAdapter.
	 * @return The first element of this Pair object.
	 */
	public E getFirst() {
		return e1;
	}
	
	/**
	 * Return the second element of the pair. In case of our implementation of the MCS algorithm, this is always an element of the second graph given to the MCSAdapter. 
	 * @return The second element of this Pair object.
	 */
	public E getSecond() {
		return e2;
	}
	
	/**
	 * Return a new Pair object that contains the elements of this pair in inverse order, meaning that the first element is now the second, and the second element now the first. 
	 * <br> The elements contained in the returned pair are not copied and therefore refer to the same objects as in this pair.  
	 * @return A new Pair object with inverted elements.
	 */
	public Pair<E> getInverse(){
		return new Pair<>(e2, e1);
	}
	
	/**
	 * @return "FirstElement <--> SecondElement"
	 */
	@Override
	public String toString() {
		return (e1 + " <--> " + e2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((e1 == null) ? 0 : e1.hashCode());
		result = prime * result + ((e2 == null) ? 0 : e2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (obj == this) {
			return true;
		}
		
		if (!obj.getClass().equals(getClass())) {
			return false;
		}
		
		Pair<?> other = (Pair<?>) obj;
		
		return elementsMatch(this.e1, other.e1) && elementsMatch(this.e2, other.e2);
	}
	
	private <T> boolean elementsMatch(E first, T second) {
		if (first == null) {
			return second == null;
		} else {
			return first.equals(second);
			
		}
	}
	
	
}