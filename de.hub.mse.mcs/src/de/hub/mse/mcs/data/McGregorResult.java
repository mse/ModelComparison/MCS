package de.hub.mse.mcs.data;

import java.util.HashSet;

import org.jgrapht.Graph;

/**
 * A McGregorResult instance contains two representations of the MCS calculated for two graphs with the McGregor algorithm, as well as the node 
 * and edge mapping between the two graphs. 
 * <br><br>
 * We store two representations of the MCS in order to have the object to the nodes and edges of each input graph. Here, the firstMCS instance 
 * contains the MCS consisting of the node and edge objects of the first graph, while the secondMCS instance the same for the second graph.
 * <br><br>
 * The node and edge mappings are defined by HashSets containing Pair< T > objects. Each pair maps a node or edge of the first graph to an 
 * node or edge of the second graph. A match in this case means, that both graph elements correspond to the same element in the MCS. 
 *
 * @param <V> The class representing the nodes of a graph
 * @param <E> The class representing the edges of a graph
 */
public class McGregorResult <V, E>{
	private Graph<V, E> firstMCS;
	private Graph<V, E> secondMCS;

	private HashSet<Pair<V>> nodeMap;
	private HashSet<Pair<E>> edgeMap;
	
	/**
	 * Create a new McGregorResult that contains the given parameters. 
	 * 
	 * @param mcsPair A Pair of MCS graphs. One mcs containing the objects of the first input graph and one mcs containing the objects of the second 
	 * input graph.
	 * @param vMap A HashSet of Pairs that map the nodes of the first graph to nodes of the second graph.
	 * @param eMap A HashSet of Pairs that map the edges of the first graph to edges of the second graph.
	 */
	public McGregorResult (Pair<Graph<V, E>> mcsPair, HashSet<Pair<V>> vMap, HashSet<Pair<E>> eMap) {
		if (mcsPair == null) {
			this.firstMCS = null;
			this.secondMCS = null;
		} else {
		this.firstMCS = mcsPair.getFirst();
		this.secondMCS = mcsPair.getSecond();
		}
		this.nodeMap = vMap;
		this.edgeMap = eMap;
	}
	
	/**
	 * Create a new McGregorResult that contains the given MCS pair and empty HashSets for the mappings of nodes and edges
	 * 
	 * @param mcsPair A Pair of MCS graphs. One MCS containing the objects of the first input graph and one MCS containing the objects of the second 
	 * input graph.
	 */
	public McGregorResult(Pair<Graph<V, E>> mcsPair) {
		if (mcsPair == null) {
			this.firstMCS = null;
			this.secondMCS = null;
		} else {
		this.firstMCS = mcsPair.getFirst();
		this.secondMCS = mcsPair.getSecond();
		}
		this.nodeMap = new HashSet<>();
		this.edgeMap = new HashSet<>();
	}
	
	/**
	 * 
	 * @return The HashSet of Pair objects that represent the mappings of edges. 
	 */
	public HashSet<Pair<E>> getEdgeMap() {
		return edgeMap;
	}
	
	/**
	 * 
	 * @return The HashSet of Pair objects that represent the mappings of nodes.
	 */
	public HashSet<Pair<V>> getNodeMap() {
		return nodeMap;
	}
	
	/**
	 * 
	 * @return The MCS graph that consists of the node and edge objects of the first input graph.
	 */
	public Graph<V, E> getFirstMCS() {
		return firstMCS;
	}
	
	/**
	 * 
	 * @return The MCS graph that consists of the node and edge objects of the second input graph.
	 */
	public Graph<V, E> getSecondMCS() {
		return secondMCS;
	}
	
	/**
	 * Swap the mappings of the pairs for nodes and edges, and swap the first and second graph.
	 * 
	 * This method is used in order to correct the mappings and graph order, in case the input graphs
	 * have been swapped during the calculation of the MCS. This is necessary to provide the correct
	 * order of results to the caller of the McGregor algorithm. 
	 */
	public void swapGraphResults() {
		this.nodeMap = swapPairs(this.nodeMap);
		this.edgeMap = swapPairs(this.edgeMap);
		Graph<V,E> temp = firstMCS;
		firstMCS = secondMCS;
		secondMCS = temp;
	}
	
	/**
	 * Swap the first and second element in each pair of the given HashSet.
	 * @param pairs The HashSet of pairs that are to be swapped.
	 * @return A new HashSet containing the swapped pairs
	 */
	private <T> HashSet<Pair<T>> swapPairs(HashSet<Pair<T>> pairs) {
		HashSet<Pair<T>> swappedPairs = new HashSet<>();
		pairs.stream()
			 .map(Pair::getInverse)
			 .forEach(swappedPairs::add);
		
		return swappedPairs;
	}
	
}
