package de.hub.mse.mcs.algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.Pseudograph;
import org.jgrapht.graph.Subgraph;

import de.hub.mse.mcs.data.McGregorResult;
import de.hub.mse.mcs.data.Pair;


/**
 * Implementation of the MARCS part of the algorithm for the detection of the edge-maximal MCS without isolated 
 * nodes that was proposed by James J. McGregor in "Backtrack Search Algorithms and the Maximal Common 
 * Subgraph Problem" (1982). 
 * 
 * @author Code: Ruud Welling. A Performance Analysis on Maximal Common Subgraph Algorithms. 2011 
 * <br> Edited: Alexander Schultheiss, HU-Berlin. 2019
 * 
 * @param <V> The class representing the nodes of the graphs
 * @param <E> The class representing the edges of the graphs
 */
class Marcs<V,E> {
	
	private List<E> edgeList1, edgeList2;
	private List<V> mappedNodesFromG1;
	private List<V> mappedNodesFromG2;
	private Graph<V,E> g1, g2;
	private boolean[][] matrix;
	private boolean[] rowOrs;
	private int dimx, dimy;
	private boolean directed, connectedOnly;
	//remembers the amount of times a label has been used in the mappings
	private int[] labelCount;
	
	private CandidateChecker<V, E> candidateChecker;
	 
	
	public boolean twotruesoneline() {
		for(int x=0;x<dimx;x++){
			boolean b = false;
			for(int y=0;y<dimy;y++){
				if (this.matrix[x][y])
					if (b)
						return true;
					else
						b = true;
			}
		}
		return false;
	}
	
	public Marcs(Marcs<V,E> other){
		this.candidateChecker = other.candidateChecker;
		this.dimx = other.dimx;
		this.dimy = other.dimy;
		this.edgeList1 = new ArrayList<E>(other.edgeList1);
		this.edgeList2 = new ArrayList<E>(other.edgeList2);
		this.mappedNodesFromG1 = new ArrayList<V>(other.mappedNodesFromG1);
		this.mappedNodesFromG2 = new ArrayList<V>(other.mappedNodesFromG2);
		this.matrix = new boolean[dimx][dimy];
		this.rowOrs = other.rowOrs.clone();
		for(int x=0;x<dimx;x++){
			for(int y=0;y<dimy;y++){
				this.matrix[x][y] = other.matrix[x][y];
			}
		}
		this.directed = other.directed;
		this.connectedOnly = other.connectedOnly;
		this.g1 = other.g1;
		this.g2 = other.g2;
		this.labelCount = other.labelCount.clone();
	}
	
	public Marcs(Graph<V,E> g1, Graph<V,E> g2, boolean connectedOnly, int labelSize, CandidateChecker<V, E> candidateChecker){
		this.candidateChecker = candidateChecker;
		this.g1 = g1;
		this.g2 = g2;
		this.edgeList1 = new ArrayList<E>(g1.edgeSet());
		this.edgeList2 = new ArrayList<E>(g2.edgeSet());
		this.mappedNodesFromG1 = new ArrayList<V>();
		this.mappedNodesFromG2 = new ArrayList<V>();
		this.dimx = edgeList1.size();
		this.dimy = edgeList2.size();
		this.directed = g1 instanceof DirectedGraph && g2 instanceof DirectedGraph;
		this.connectedOnly = connectedOnly;
		this.matrix = new boolean[dimx][dimy];
		
		for(int x=0;x<dimx;x++){
			for(int y=0;y<dimy;y++){
				this.matrix[x][y] = edgesCompatible(x,y);
			}
		}
		this.rowOrs = new boolean[dimx];
		//updateOrs();
		this.labelCount = new int[labelSize];
		for(int i=0;i<labelSize;i++){
			this.labelCount[i] =0;
		}
		
	}
	
	public void incrementLabelCount(int i){
		labelCount[i]++;
	}
	
	public int getLabelCount(int index){
		return labelCount[index];
	}
	
	/**
	 * Returns a List with unique elements, 
	 * @param v1 node from g1
	 * @return
	 */
	public List<V> getPrioritySubset(V v1){
		//The resulting list
		List<V> result = new ArrayList<V>();
		
		//list of already mapped nodes that neighbor v1
		List<V> v1Others = new ArrayList<V>();
		
		V v1other;
		V v2other;
		for(E e1 : g1.edgesOf(v1)){
			v1other = Graphs.getOppositeVertex(g1, e1, v1);
			if(mappedNodesFromG1.contains(v1other)){
				v1Others.add(v1other);
			}
		}
		for(V v2 : g2.vertexSet()){
			//if v2 and v1 are candidates and v2 has not been mapped yet
			if(candidateChecker.checkNodes(v1, v2) && !mappedNodesFromG2.contains(v2)){
				//test if there is an edge to a node which has already been mapped
				for(E e2 : g2.edgesOf(v2)){
					v2other = Graphs.getOppositeVertex(g2, e2, v2);
					//if the node v2other has already been mapped
					if(mappedNodesFromG2.contains(v2other)){
						//labels are not checked, this is done at a later stage anyway and doing it twice is not needed and takes too much time
						result.add(v2);
						break;
					}
				}
			}
		}
		return result;
	}
	
	private boolean edgesCompatible(int edge1, int edge2){
		E e1 = edgeList1.get(edge1);
		E e2 = edgeList2.get(edge2);
		boolean result = false;
		// The edges themselves have to be candidates
		if(candidateChecker.checkEdges(e1, e2)){
			// Get the source and target nodes of edge 1
			V e1Source = g1.getEdgeSource(e1);
			V e1Target = g1.getEdgeTarget(e1);
			
			// Get the source and target nodes of edge 2
			V e2Source = g2.getEdgeSource(e2);
			V e2Target = g2.getEdgeTarget(e2);
			//checks if the pair of source nodes are candidates, and checks the same for the target nodes
			boolean sourceTargetMatch = candidateChecker.checkNodes(e1Source, e2Source) && candidateChecker.checkNodes(e1Target, e2Target); 
			if(directed){
				result = sourceTargetMatch;
			} else{
				//checks if source1,target2 are candidates and if target1,source2 are candidates
				boolean sourceTargetInverseMatch = candidateChecker.checkNodes(e1Source, e2Target) && candidateChecker.checkNodes(e1Target, e2Source); 
				result = (sourceTargetMatch || sourceTargetInverseMatch);
			}
		}
		return result;
	}
	
	/**
	 * @return the arcsLeft
	 * The variable arcsleft is used to keep track of the number of arcs which
	 * could still correspond in a node correspondeces based on the current partial correspondence.
	 * Whenever a refinement of MARCS results in a row being set to zero (this happens when two adjacent nodes in G1,
	 * have been tentatively mapped to nonadjacent nodes in G2) arcsleft is decremented by one.
	 */
	public int getArcsLeft() {
		//this method takes the maximum value of arcsleft ( edgeList1.size() ) and
		//decrements for every row that is completely set to false
		int result = edgeList1.size();
		updateOrs();
		for (int x = 0; x<dimx;x++)
			if(!rowOrs[x])
				result--;
		return result;
	}
	
	public void updateOrs() {
		for(int x=0;x<dimx;x++){
			boolean rowOrs = false;
			for(int y=0;y<dimy;y++){
				rowOrs |= matrix[x][y];
			}
			this.rowOrs[x] = rowOrs;
		}	
	}
	
	public int getArcsInBiggestConnectedGraph(){
		return getResult().getFirstMCS().edgeSet().size();
	}
	
	/**
	 * @return the amount of arcs that are connected on both sides to nodes that have been mapped so far
	 */
	public int getArcsLeftForMappedNodes() {
		int result = 0;
		boolean columnOr;
		for(int y=0;y<dimy;y++){
			columnOr = false;
			for(int x=0;x<dimx;x++){
				columnOr|= matrix[x][y];
			}
			if(columnOr){
				//if Edge edgeList2.get(y) is connected (on both sides) to nodes in mappedNodesFromG2
				if(mappedNodesFromG2.contains(g2.getEdgeSource(edgeList2.get(y)))
						&& mappedNodesFromG2.contains(g2.getEdgeSource(edgeList2.get(y)))){
					result++;
				}
			}
			
		}
		return result;
	}
	
	/**
	 * 
	 * @param v1 node from G1
	 * @param v2 new node from G2, this node is remembered as an added node
	 */
	/* Each 
	time a node in G1 is tentatively paired with a node in G2, MARCS is refined on the 
	basis of this node correspondence. Say node i  in G1 is tentatively paired with node j in 
	G2. Then any arc r connected to node i in G1 can correspond only to arcs which are 
	connected to node j in G2. This is represented in MARCS by setting to zero any bit 
	(r, s) such that arc r is connected to node i in G, and arc s is not connected to node j in 
	G2. */
	public void refine(V v1, V v2){
		mappedNodesFromG1.add(v1);
		mappedNodesFromG2.add(v2);
		List<Integer> edgesV1Ints = null;
		List<Integer> edgesV2Ints = null;
		List<Integer> edgesV1OutInts = null;
		List<Integer> edgesV1InInts = null;
		List<Integer> edgesV2OutInts = null;
		List<Integer> edgesV2InInts = null;
		if(!directed){
			edgesV1Ints = new ArrayList<Integer>();
			edgesV2Ints = new ArrayList<Integer>();
			for(E e1 : g1.edgesOf(v1)){
				edgesV1Ints.add(edgeList1.indexOf(e1));
			}
			for(E e2 : g2.edgesOf(v2)){
				edgesV2Ints.add(edgeList2.indexOf(e2));
			}
		} else { //directed
			edgesV1OutInts = new ArrayList<Integer>();
			edgesV1InInts = new ArrayList<Integer>();
			edgesV2OutInts = new ArrayList<Integer>();
			edgesV2InInts = new ArrayList<Integer>();
			for(E e1 : ((DirectedGraph<V,E>) g1).outgoingEdgesOf(v1)){
				edgesV1OutInts.add(edgeList1.indexOf(e1));
			}
			for(E e1 : ((DirectedGraph<V,E>) g1).incomingEdgesOf(v1)){
				edgesV1InInts.add(edgeList1.indexOf(e1));
			}
			for(E e2 : ((DirectedGraph<V,E>) g2).outgoingEdgesOf(v2)){
				edgesV2OutInts.add(edgeList2.indexOf(e2));
			}
			for(E e2 : ((DirectedGraph<V,E>) g2).incomingEdgesOf(v2)){
				edgesV2InInts.add(edgeList2.indexOf(e2));
			}
		}
		for(int x=0;x<dimx;x++){
			if(!rowOrs[x]) continue;
			for(int y=0;y<dimy;y++){
				//if the only one of the two edges x and y are connected to v1 or v2 then x and y are not compatible and the matrix should be set to false
				if(matrix[x][y]){
					if(directed){
						if((edgesV1OutInts.contains(x) != edgesV2OutInts.contains(y))
								|| (edgesV1InInts.contains(x) != edgesV2InInts.contains(y))){
							matrix[x][y] = false;
							
						}
					} else { //undirected
						if((edgesV1Ints.contains(x) && !edgesV2Ints.contains(y))
								|| (!edgesV1Ints.contains(x) && edgesV2Ints.contains(y))){
							matrix[x][y] = false;
						}
					}
				}
			}
		}
		updateOrs();
	}
	
	/**
	 * 
	 * @param connectedOnly if true, the result will be a connected graph
	 * @return
	 */
	public McGregorResult<V,E> getResult(){
		Graph<V,E> subgraphG1 = null;
		Graph<V,E> subgraphG2 = null;
		HashSet<Pair<V>> nodeMapping = new HashSet<>();
		HashSet<Pair<E>> edgeMapping = new HashSet<>();
		
		if(directed){
			subgraphG1 = new DirectedPseudograph<V,E>(g1.getEdgeFactory());
			subgraphG2 = new DirectedPseudograph<V,E>(g2.getEdgeFactory());
		} else {
			subgraphG1 = new Pseudograph<V,E>(g1.getEdgeFactory());
			subgraphG2 = new Pseudograph<V,E>(g2.getEdgeFactory());
		}
		int i = 0;

		
		for(int x=0;x<dimx;x++){
			for(int y=0;y<dimy;y++){
				if(matrix[x][y]){
					// We need the edge and the nodes from the first graph in order to save the mapping
					E edgeG1 = edgeList1.get(x);
					V sourceG1 = g1.getEdgeSource(edgeG1);
					V targetG1 = g1.getEdgeTarget(edgeG1);

				    // We use the edge and nodes from the second graph to construct the subgraph
				    E edgeG2 = edgeList2.get(y);
					V sourceG2 = g2.getEdgeSource(edgeG2);
					V targetG2 = g2.getEdgeTarget(edgeG2);
					
					subgraphG1.addVertex(sourceG1);
					subgraphG1.addVertex(targetG1);
					subgraphG1.addEdge(sourceG1, targetG1, edgeG1);

					subgraphG2.addVertex(sourceG2);
					subgraphG2.addVertex(targetG2);
					subgraphG2.addEdge(sourceG2, targetG2, edgeG2);
					
					// We have to check which nodes are compatible, in order to handle undirected graphs correctly, where source and target node can be swapped  
					boolean sourceTargetMatch = candidateChecker.checkNodes(sourceG1, sourceG2) && candidateChecker.checkNodes(targetG1, targetG2); 
					boolean sourceTargetInverseMatch = candidateChecker.checkNodes(sourceG1, targetG2) && candidateChecker.checkNodes(targetG1, sourceG2); 
					
					// Save the edge pair in the mapping
					edgeMapping.add(new Pair<E>(edgeG1, edgeG2));

					// Save the node pairs in the mapping
					if(sourceTargetMatch) {
						// The source nodes and target nodes of the mapped edges are compatible
						nodeMapping.add(new Pair<V>(sourceG1, sourceG2));
						nodeMapping.add(new Pair<V>(targetG1, targetG2));
					} else if(!directed && sourceTargetInverseMatch) {
						// In case of non-directed graphs, the source node of the first edge might be compatible to the target node of the second edge and vice versa
						nodeMapping.add(new Pair<V>(sourceG1, targetG2));
						nodeMapping.add(new Pair<V>(targetG1, sourceG2));
					} else {
						// Otherwise, this is an error that should not occur if everything is implemented correctly
						throw new IllegalStateException("An edge has been marked as mapped, even though its nodes do not match!");
					}
					//break;
				}
			}
		}
		
		if(connectedOnly){
			subgraphG1 = getLargestConnectedPart(subgraphG1);
			subgraphG2 = getLargestConnectedPart(subgraphG1);
		}
		return new McGregorResult<V, E>(new Pair<>(subgraphG1, subgraphG2), nodeMapping, edgeMapping);
	}
	
	private Graph<V,E> getLargestConnectedPart(Graph<V, E> subgraph) {
		//make sure this subgraph is connected, if it is not return the largest connected part
		List<Set<V>> connectedNodes = new ArrayList<Set<V>>();
		for(V v : subgraph.vertexSet()){
			if(!containsV(connectedNodes, v)){
				connectedNodes.add(getConnectedNodes(subgraph, v));
			}
		}
		//ConnectedNodes now contains Sets of connected nodes every node of the subgraph is contained exactly once in the list
		//if there is more then 1 set, then this method should return the largest connected part of the graph
		if(connectedNodes.size() > 1){
			Graph<V,E> largestResult = null;
			Graph<V,E> currentGraph;
			int largestSize = -1;
			Set<V> currentSet;
			for(int i=0;i<connectedNodes.size();i++){
				currentSet = connectedNodes.get(i);
				/*note that 'subgraph' is the result from the Mcgregor algorithm, 'currentGraph' is an
				 * induced subgraph of 'subgraph'. 'currentGraph' is connected, because the nodes in
				 * 'currentSet' are connected with edges in 'subgraph'
				 */
				currentGraph = new Subgraph<V,E,Graph<V,E>>(subgraph, currentSet);
				if(currentGraph.edgeSet().size() > largestSize){
					largestResult = currentGraph;
					largestSize = currentGraph.edgeSet().size();
				}
			}
			
			return largestResult;
		} else {
			return subgraph;
		}
	}
	
	public String toString(){
		String result = "";
		int width = 1 + 4*dimy;
		String hline = "\n";
		for(int i=0;i<width;i++){
			hline += "-";
		}
		for(int x=0;x<dimx;x++){
			result += hline + "\n";
			result += "|";
			for(int y=0;y<dimy;y++){
				if(matrix[x][y]) {
					result += " 1 |";
				} else {
					result += " 0 |";
				}
			}
		}
		result +=  hline;
		return result;
	}
	
	public Pair<Graph<V,E>> toEmptyGraphPair() {
		Graph<V, E> subgraphG1;
		Graph<V, E> subgraphG2;
		
		if(directed){
			subgraphG1 = new DirectedPseudograph<V,E>(g1.getEdgeFactory());
			subgraphG2 = new DirectedPseudograph<V,E>(g2.getEdgeFactory());
		} else {
			subgraphG1 = new Pseudograph<V,E>(g1.getEdgeFactory());
			subgraphG2 = new Pseudograph<V,E>(g2.getEdgeFactory());
		}
		
		return new Pair<>(subgraphG1, subgraphG2);
	}
	
	/**
	 * Searches for all nodes connected to 'node' in graph 'g'
	 * @param g the graph that should be searched
	 * @param node the node of which all connected nodes should be found
	 * @return A Set containing nodes: for every pair of nodes v1,v2
	 * where v1 != v2, there exists a path in Graph g (ignoring edge direction) from v1 to v2.
	 * This set will at least contain 'node'
	 * For every node vg in Graph g that is not in the resulting Set, there exists no path (ignoring edge direction)
	 * from vg to 'node'
	 */
	private static <V,E> Set<V> getConnectedNodes(Graph<V,E> g, V node){
		Set<V> result = new HashSet<V>();
		result.add(node);
		expandConnectedNodes(g, node, result);
		return result;
	}
	
	/**
	 * 
	 * @param g
	 * @param newNode
	 * @param connectedNodes
	 */
	private static <V,E> void expandConnectedNodes(Graph<V,E> g, V newNode, Set<V> connectedNodes){
		//for every edge (ignoring direction) connected newNode
		for(E edge : g.edgesOf(newNode)){
			V connectedNode; //find the other node connected to newVNode
			if(g.getEdgeSource(edge).equals(newNode)){
				connectedNode = g.getEdgeTarget(edge);
			} else {
				connectedNode = g.getEdgeSource(edge);
			}
			if(!connectedNodes.contains(connectedNode)){ //if this node is not in connectedNodes
				connectedNodes.add(connectedNode); //add it and search for more connected nodes
				expandConnectedNodes(g, connectedNode, connectedNodes);
			}
		}
	}

	private static <V> boolean containsV(List<Set<V>> list, V v){
		for(int i=0;i<list.size();i++){
			if(list.get(i).contains(v)) return true;
		}
		return false;
	}
}
