package de.hub.mse.mcs.algorithm;

import java.util.HashSet;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.Pseudograph;

import de.hub.mse.mcs.data.LabeledObject;
import de.hub.mse.mcs.data.McGregorResult;
import de.hub.mse.mcs.data.Pair;
import de.hub.mse.mcs.util.GraphViewer;

/**
 * MCSAdapter calculates the MCS and MCCS of two given graphs with the help of a given CandidateChecker. Here, the MCS with the maximal number of nodes is 
 * calculated, meaning that also isolated nodes are included. The MCSAdapter applies the MCS-Algorithm proposed by James J. McGregor (Backtrack Search 
 * Algorithms and the Maximal Common Subgraph Problem, 1982) implemented by Ruud Welling (A Performance Analysis on Maximal Common Subgraph Algorithms. 2011). 
 * 
 * <br><br>
 * After initialization of
 * an MCSAdapter, the MCS and MCCS can be requested by calling the corresponding getter methods that return JGraphT:Graph<V,E> objects. 
 * <br>In addition, it is possible to request the mappings of nodes and edges between the two graphs as HashSet<Pair<>> objects. Each mapping is represented by
 * a Pair<> object containing a node or edge of the first graph as first element, and the matched node or edge of the second graph as 
 * second element. 
 * <br><br>
 *
 * @param <V> The class representing nodes in the graphs for which the MCS is to be calculated. This class has to implement the LabeledObject interface.
 * @param <E> The class representing edges in the graphs for which the MCS is to be calculated.
 */
public class MCSAdapter<V extends LabeledObject, E> {
	// The two graphs for which the MCS is calculated
	private Graph<V, E> graph1;
	private Graph<V, E> graph2;
	// The CandidateChecker that is responsible for checking whether two nodes or edges are candidates that might be matched by the MCS.  
	private CandidateChecker<V, E> candidateChecker;
	
	private final long TIMEOUT;
	
	// The MCS of graph1 and graph2
	private Graph<V, E> mcs;
	// The MCCS of graph1 and graph2
	private Graph<V, E> mccs;
	
	// The HashSets of Pairs mapping nodes and edges of the two graphs
	private HashSet<Pair<V>> mcsNodeMapping;
	private HashSet<Pair<E>> mcsEdgeMapping;
	private HashSet<Pair<V>> mccsNodeMapping;
	private HashSet<Pair<E>> mccsEdgeMapping;

	/**
	 * Initialize a new MCSAdapter and calculate the MCS and MCCS for the two given graphs. The CandidateChecker
	 * is necessary in order to check whether two nodes or edges a potential candidates to be matched. 
	 * 
	 * @param g1 The first graph for which the MCS and MCCS is calculated
	 * @param g2 The second graph for which the MCS and MCCS is calculated
	 * @param candidateChecker The CandidateChecker that checks nodes and edges for compatibility. 
	 */
	public MCSAdapter(Graph<V, E> g1, Graph<V, E> g2, CandidateChecker<V, E> candidateChecker, long TIMEOUT) {
		this.graph1 = g1;
		this.graph2 = g2;
		this.candidateChecker = candidateChecker;
		this.TIMEOUT = TIMEOUT;

		this.mcsNodeMapping = new HashSet<>();
		this.mcsEdgeMapping = new HashSet<>();
		this.mccsNodeMapping = new HashSet<>();
		this.mccsEdgeMapping = new HashSet<>();
	}

	/**
	 * Show the first graph for which the MCS was calculated in a separate frame. 
	 */
	public void showGraph1() {
		GraphViewer.viewGraph(graph1, "Graph1");
	}

	/**
	 * Show the second graph for which the MCS was calculated in a separate frame. 
	 */
	public void showGraph2() {
		GraphViewer.viewGraph(graph2, "Graph2");
	}

	/**
	 * Show the MCS of the two graphs in a separate frame.
	 */
	public void showMCS() {
		GraphViewer.viewGraph(mcs, "MCS");
	}

	/**
	 * Show the MCCS of the two graphs in a separate frame.
	 */
	public void showMCCS() {
		GraphViewer.viewGraph(mccs, "MCCS");
	}

	/**
	 * 
	 * @return The graph that was given as first argument to the Adapter. 
	 */
	public Graph<V, E> getGraph1() {
		return graph1;
	}

	/**
	 * @return The graph that was given as second argument to the Adapter
	 */
	public Graph<V, E> getGraph2() {
		return graph2;
	}

	/**
	 * Return the MCS of the two graphs as new Graph< V, E > object. The nodes and edges of the graph are the
	 * same node and edge objects that are part of the first graph given to the MCSAdapter. 
	 * 
	 * @return The MCS of the two graphs as Graph< V, E > 
	 */
	public Graph<V, E> getMCS() {
		if (mcs == null) {
			createMaxCommonSubgraph();
		}
		return mcs;
	}

	/**
	 * Return the MCCS of the two graphs as new Graph< V, E > object. The nodes and edges of the graph are the
	 * same node and edge objects that are part of the first graph given to the MCSAdapter. 
	 * 
	 * @return The MCCS of the two graphs as Graph< V, E > 
	 */
	public Graph<V, E> getMCCS() {
		if (mccs == null) {
			createMaxCommonConnectedSubgraph();
		}
		return mccs;
	}

	/**
	 * Return the mappings of nodes between the two graphs. Each mapping is represented by a Pair< V > that contains a node of the first graph 
	 * as first element and a node of the second graph as second element. Two nodes are mapped to each other if they correspond to 
	 * the same node in the MCS of the two graphs. 
	 * 
	 * @return A HashSet with the mappings of nodes as Pair objects. The set contains one pair for each node in the MCS.
	 */
	public HashSet<Pair<V>> getMCSNodeMapping() {
		return mcsNodeMapping;
	}

	/**
	 * Return the mappings of edges between the two graphs. Each mapping is represented by a Pair< E > that contains a edge of the first graph 
	 * as first element and a edge of the second graph as second element. Two edges are mapped to each other if they correspond to 
	 * the same edge in the MCS of the two graphs. 
	 * 
	 * @return A HashSet with the mappings of edges as Pair objects. The set contains one pair for each edge in the MCS.
	 */
	public HashSet<Pair<E>> getMCSEdgeMapping() {
		return mcsEdgeMapping;
	}
	
	/**
	 * Return the mappings of nodes between the two graphs. Each mapping is represented by a Pair< V > that contains a node of the first graph 
	 * as first element and a node of the second graph as second element. Two nodes are mapped to each other if they correspond to 
	 * the same node in the MCCS of the two graphs. 
	 * 
	 * @return A HashSet with the mappings of nodes as Pair objects. The set contains one pair for each node in the MCCS.
	 */
	public HashSet<Pair<V>> getMCCSNodeMapping() {
		return mccsNodeMapping;
	}

	/**
	 * Return the mappings of edges between the two graphs. Each mapping is represented by a Pair< E > that contains a edge of the first graph 
	 * as first element and a edge of the second graph as second element. Two edges are mapped to each other if they correspond to 
	 * the same edge in the MCCS of the two graphs. 
	 * 
	 * @return A HashSet with the mappings of edges as Pair objects. The set contains one pair for each edge in the MCCS.
	 */
	public HashSet<Pair<E>> getMCCSEdgeMapping() {
		return mccsEdgeMapping;
	}

	/**
	 * Calculate the largest connected subgraph of the already calculated MCS
	 */
	public void createMaxCommonConnectedSubgraph() {
		boolean connected = true;
		// Get the largest connected part of the calculated MCS
		McGregorResult<V,E> mcgResult = McGregor.maxCommonSubgraph(graph1, graph2, connected, candidateChecker, this.TIMEOUT);
		
		mccs = mcgResult.getFirstMCS();
		// Get the node and edge mappings that belong to the MCCS by filtering the mappings 
		mcgResult.getNodeMap().stream()
								.filter(pair -> mccs.containsVertex(pair.getFirst()))
								.forEach(pair -> mccsNodeMapping.add(pair));
		mcgResult.getEdgeMap().stream()
							.filter(pair -> mccs.containsEdge(pair.getFirst()))
							.forEach(pair -> mccsEdgeMapping.add(pair));
	}

	/**
	 * Calculate the MCS of the two graphs by calling the McGregor implementation with connected set to "false". <br>
	 * The McGregor implementation returns the MCS of the two graphs without isolated nodes, these are handled 
	 * separately by calling the handleIsolatedNodes method. 
	 */
	public void createMaxCommonSubgraph() {
		// Get copies of the two graphs that can be altered during the search for the MCS
		Graph<V, E> graph1Copy = getGraphCopy(graph1);
		Graph<V, E> graph2Copy = getGraphCopy(graph2);

		// Initialize the instance for the MCS, depending on whether the input graphs are directed graphs or undirected graphs
		if (graph1 instanceof DirectedGraph && graph2 instanceof DirectedGraph) {
			mcs = new DirectedPseudograph<>(graph1.getEdgeFactory());
		} else {
			mcs = new Pseudograph<>(graph1.getEdgeFactory());
		}
		
		// If one of the graphs does not contain edges, we can immediately handle isolated nodes
		if (graph1Copy.edgeSet().isEmpty() || graph2Copy.edgeSet().isEmpty()) {
			handleIsolatedNodes(graph1Copy, graph2Copy);
		} else {
			boolean connected = false;
			// Apply the McGregor algorithm to get the MCS without isolated nodes
			McGregorResult<V, E> mcgResult = McGregor.maxCommonSubgraph(graph1Copy, graph2Copy, connected, candidateChecker, this.TIMEOUT);
			
			// Assert that the McGregor algorithm was able to find a result
			// Get the MCS of the two graphs, once with the node and edge references of the first graph, and once with the node and edge references of the second graph
			Graph<V, E> mcs1 = mcgResult.getFirstMCS();
			Graph<V, E> mcs2 = mcgResult.getSecondMCS();
			
			// If the McGregor implementation has found a solution, handle the result
			if (!mcs1.vertexSet().isEmpty()) {
				mcsNodeMapping.addAll(mcgResult.getNodeMap());
				mcsEdgeMapping.addAll(mcgResult.getEdgeMap());
				
				// Add the node and edge objects of the first MCS to the main MCS. Here, it does not really matter whether the first or second MCS is taken. 
				// The result is the same
				for (V node : mcs1.vertexSet()) {
					mcs.addVertex(node);
				}
				for (E edge : mcs1.edgeSet()) {
					mcs.addEdge(mcs1.getEdgeSource(edge), mcs1.getEdgeTarget(edge), edge);
				}
	
				// Delete all nodes that have been mapped and are already a part of the MCS
				graph1Copy.removeAllVertices(mcs1.vertexSet());
				graph2Copy.removeAllVertices(mcs2.vertexSet());
			} 
			// Try to match all nodes that are not part of the MCS constructed by the implementations of McGregors algorithm
			handleIsolatedNodes(graph1Copy, graph2Copy);
		}
	}

	private void handleIsolatedNodes(Graph<V, E> oldgraph1, Graph<V, E> oldgraph2) {
		// Get the node sets of both graphs in order to add all single nodes that are left
		Set<V> nodeSet1 = new HashSet<>(oldgraph1.vertexSet());
		Set<V> nodeSet2 = new HashSet<>(oldgraph2.vertexSet());

		// Search for compatible node pairs until all pairs have been tried
		for (V v1 : nodeSet1) {
			for (V v2 : nodeSet2) {
				if (candidateChecker.checkNodes(v1, v2)) {
					// A pair of compatible nodes has been found, add it to the mapping and MCS
					Pair<V> pair = new Pair<>(v1, v2);
					mcsNodeMapping.add(pair);
					mcs.addVertex(v1);

					// Delete the found candidate from the second set, to make sure it is not associated with a another node from the first set
					nodeSet2.remove(v2);
					// We have found a fitting candidate for the current v1, so we can search for a fitting candidate for the next v1 node
					break;
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Graph<V,E> getGraphCopy(Graph<V,E> graph) {
		try {
			return (Graph<V,E>) ((AbstractBaseGraph<V,E>) graph).clone();
		} catch (ClassCastException e) {
			ClassCastException realException = new ClassCastException("One of the given graphs is not a subclass of AbstractBaseGraph and can therefore not be copied!");
			realException.setStackTrace(e.getStackTrace());
			throw realException;
		}
	}
}
