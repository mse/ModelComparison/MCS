package de.hub.mse.mcs.algorithm;


/**
 * The CandidateChecker interface has to be implemented by any class whose instances are supposed to check whether two nodes or
 * edges are candidates for each other. Candidates in this case means, that the two nodes or edges might be matched by the 
 * MCS algorithm. <br> 
 * <br>
 * Whether two nodes or edges are candidates for each other mainly depends on the classes that represent them. For example, 
 * nodes represented by a simple LabeledObject implementation might be candidates for each other, if they carry the 
 * same label. 
 * 
 * @param <V> The class that represents the nodes of a graph 
 * @param <E> The class that represents the edges of a graph
 */
public interface CandidateChecker <V,E> {
	
	/**
	 * This method is supposed to check whether two objects of the node class are candidates for a potential match by the MCS algorithm. 
	 * Whether two nodes are candidates, depends on the information that the node class holds. 
	 * 
	 * @param node1 The first node for the candidate check
	 * @param node2 The second node for the candidate check
	 * @return true if both nodes are candidates for a MCS match, otherwise false 
	 */
    boolean checkNodes(V node1, V node2);

	/**
	 * This method is supposed to check whether two objects of the edge class are candidates for a potential match by the MCS algorithm. 
	 * Whether two edges are candidates, depends on the information that the edge class holds. 
	 * <br>!---!<br>
	 * Here, the compatibility of edged should NOT depend on their specific source or target nodes, as this is handled by the MCS algorithm itself.
	 * Instead, the compatibility of two edged should only depend on their inherent features, e.g. label, weight, type, ...  
	 * <br>!---!
	 * @param edge1
	 * @param edge2
	 * @return
	 */
    boolean checkEdges(E edge1, E edge2);
}
