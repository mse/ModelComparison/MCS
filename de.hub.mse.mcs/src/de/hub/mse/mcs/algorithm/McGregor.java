package de.hub.mse.mcs.algorithm;

import de.hub.mse.mcs.data.LabeledObject;
import de.hub.mse.mcs.data.McGregorResult;
import org.jgrapht.Graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the algorithm for the detection of the edge-maximal MCS without isolated nodes that
 * was proposed by James J. McGregor in "Backtrack Search Algorithms and the Maximal Common Subgraph Problem"
 * (1982). 
 * <br><br>
 * In addition to the standard algorithm, this implementation includes the handling of pre-labeled nodes
 * and priority subsets in order to improve performance, as proposed by McGregor. Therefore, the given node
 * class has to implement the LabeledObject interface. 
 * <br><br>
 * If it is not possible to group the nodes of the graphs with labels, the DefaultLabeledNode
 * interface can be implemented instead. Here, all nodes are assigned the same label and the standard 
 * algorithm is performed. 
 * 
 * @author Code: Ruud Welling. A Performance Analysis on Maximal Common Subgraph Algorithms. 2011 
 * <br> Edited: Alexander Schultheiss, HU-Berlin. 2019
 * 
 * @param <V> The class representing the nodes of the graphs
 * @param <E> The class representing the edges of the graphs
 */
class McGregor<V  extends LabeledObject ,E> {
	
	private List<V> nodes1;
	private List<V> nodes2;
	private long TIMEOUT;
	
	/**
	 * nodesTried[n1][n2] is true if node n2 has been tried for node n1
	 */
	private boolean[][] nodesTried;
	/**
	 * priority[i1][n2] is true if a node n2 is part of the priority subset for node i1
	 */
	private boolean[][] priority;
	private Marcs<V,E> marcs;
	private Map<Integer,Marcs<V,E>> marcsCopies;
	private int arcsLeft,bestArcsLeft;
	private boolean findConnectedSubgraphOnly;
	private List<String> labels;
	//remembers how often labels from List 'labels' occur in the graph where they least occur
	//for example, if g1 has 5 nodes with label X and g2 has 3 nodes with label X labelsize for label X will be 3
	private int[] labelsSize;
	//Array with one boolean value for each node of g1, if the value is true, then an untried node has been found which did not have the same label
	//this means that if no more untried nodes exist for that node, search must still continue for the next node (backtrack is not yet needed)
	private boolean[] noLabelMatch;
	
	private CandidateChecker<V, E> candidateChecker;
	
	/**
	 * @param g1 the first supergraph
	 * @param g2 the second supergraph
	 * @param connected must be true if the subgraph has to be a connected subgraph
	 * @return the maximum common subgraph of g1 and g2
	 */
	public static <V extends LabeledObject, E> McGregorResult<V,E> maxCommonSubgraph(Graph<V,E> g1, Graph<V,E> g2, boolean connected, CandidateChecker<V, E> candidateChecker, long TIMEOUT){
		McGregorResult<V,E> result;
		if(g1.vertexSet().size() > g2.vertexSet().size()){
			result = maxCommonSubgraph(g2, g1, connected, candidateChecker, TIMEOUT); //the algorithm asumes g1.vertexSet().size() <= g2.vertexSet().size()
			// The mappings of nodes and edges have to be inverted to handle the swapping of g2 and g1 correctly
			result.swapGraphResults();
		} else {
			McGregor<V,E> m = new McGregor<>(g1, g2, connected, candidateChecker, TIMEOUT);
			result = m.findMaximumCommonSubgraph();
		}
		return result;
	}
	
	private McGregor(Graph<V,E> g1, Graph<V,E> g2, boolean connected, CandidateChecker<V, E> candidateChecker, long TIMEOUT){
		this.candidateChecker = candidateChecker;
		this.TIMEOUT = TIMEOUT;
		
		nodes1 = new ArrayList<>(g1.vertexSet());
		nodes2 = new ArrayList<>(g2.vertexSet());
		//initialize the List that remembers which nodes have been tried
		nodesTried = new boolean[nodes1.size()][nodes2.size()];
		priority = new boolean[nodes1.size()][nodes2.size()];
		
		noLabelMatch = new boolean[nodes1.size()];
		
		//initialize the list with labels
		labels = new ArrayList<>();
		//Some temporary variables for reuse
		int i;
		String label;
		for(i=0;i<nodes1.size();i++){
			label = nodes1.get(i).getGroupLabel();
			if(!labels.contains(label)){
				labels.add(label);
			}
			//initialize noLabelMatch in the same loop
			noLabelMatch[i]=false;
		}
		for(i=0;i<nodes2.size();i++){
			label = nodes2.get(i).getGroupLabel();
			if(!labels.contains(label)){
				labels.add(label);
			}
		}
		//'labels' now contains all labels in g1 and g2
		//initialize the matrices with labelSize and labelCount
		//temporarily used to count labels from g1
		labelsSize = new int[labels.size()];
		//temporarily used to count labels from g2
		int[] labelsG2 = new int[labels.size()];
		for(i=0;i<nodes1.size();i++){
			label = nodes1.get(i).getGroupLabel();
			labelsSize[labels.indexOf(label)]++;
		}
		for(i=0;i<nodes2.size();i++){
			label = nodes2.get(i).getGroupLabel();
			labelsG2[labels.indexOf(label)]++; 
		}
		//make sure labelSize contains the smallest values
		for(i=0;i<labels.size();i++){
			if(labelsG2[i] < labelsSize[i]){
				labelsSize[i] = labelsG2[i];
				
			}
		}

		//initialize marcs and other variables
		marcs = new Marcs<>(g1, g2, connected, labels.size(), candidateChecker);
		marcsCopies = new HashMap<>();
		marcsCopies.put(0, new Marcs<>(marcs)); //put a copy of the default marcs at 0
		arcsLeft = g1.edgeSet().size();
		bestArcsLeft = 0;
		findConnectedSubgraphOnly = connected;
	}
	

	
	private McGregorResult<V,E> findMaximumCommonSubgraph(){
		//initalize marcs and other variables
		Marcs<V,E> marcsTemp;
		Marcs<V,E> bestMarcs = null;
		long startTime = System.currentTimeMillis();
		//current Node
		int iNode1 = 0;
		markAllAsUntried(iNode1); //Mark all nodes of G2 as untried for node i;
		//System.out.println("Starting at node " + iNode1 + "label:" + nodes1.get(iNode1).getLabel());
		
		while(iNode1>=0){ 	//repeat .... until i==-1    (i==0 changed to i==-1 because we start at 0 instead of 1)
			long currentTime = System.currentTimeMillis() - startTime;
			if(currentTime >= this.TIMEOUT) {
				//bestMarcs = null; //Stop and return null when interrupted
				System.err.println("The MCS calculation reached the timeout of " + this.TIMEOUT + "ms!");
				break;
			}
			//if there are any untried nodes in G2 to which node i of G1 may correspond
			//xi := one of these nodes
			int xiNode2 = getUntriedNode(iNode1);
			//System.out.println("Fetched untried node" + xiNode2);
			
			if(xiNode2 != -1){
				nodesTried[iNode1][xiNode2] = true;	//Mark node xi as tried for node i
				
				if(!candidateChecker.checkNodes(nodes1.get(iNode1), nodes2.get(xiNode2))) {//if the nodes are candidates
					noLabelMatch[iNode1] = true;
					// do not attempt to pair these nodes continue trying other nodes
				} else {
				
					//System.out.println("node pair:" + iNode1 + "," + xiNode2 + " before \n" + marcs);
					marcsTemp = new Marcs<>(marcs); //store a temporary copy of marcs, so it can be restored when arcsleft <= bestarcsleft for this node
					
					marcs.refine(nodes1.get(iNode1), nodes2.get(xiNode2));				//refine MARCS on the basis of this tentative correspondence for node i;
					//update the label
					marcs.incrementLabelCount(labels.indexOf(nodes1.get(iNode1).getGroupLabel()));
					arcsLeft = marcs.getArcsLeft(); //update arcsLeft;
					//System.out.println("after:" + marcs + "\n Arcsleft" + arcsLeft + "\n");
					if(arcsLeft > bestArcsLeft){
						//System.out.println(iNode1);
						if(iNode1 == nodes1.size()-1 || allPossibleNodesMapped()){ 	//if i = p1   (p1 is g1.vertexSet().size(). Because java arrays start at 0 we use size-1)
							if(!findConnectedSubgraphOnly || marcs.getArcsInBiggestConnectedGraph() > bestArcsLeft){ //this makes sure connected graphs are found only
								//System.out.println("partial solution found!");
								bestMarcs = new Marcs<>(marcs);					// take note  of  xl, x2,  ...,  xp1,  MARCS;  (x1 ..xp1 are stored in MARCS)
								bestArcsLeft = arcsLeft;
							}
						} else{
							iNode1++;
							marcsCopies.put(iNode1, new Marcs<>(marcs)); 		//store a copy of MARCS, arcsleft  in  the workspace associated with node i 
							markAllAsUntried(iNode1); //mark all nodes of G2 as untried for node i
						}
					} else {
						marcs = marcsTemp;
						//System.out.println("Restored Marcs");
					}
				}
			} else {
				//if there are still untried nodes, but with a different label
				//(exclude this test for the last node, because there is no next node to search for)
				/*if(noLabelMatch[iNode1] && iNode1 != nodes1.size()-1){ 
					noLabelMatch[iNode1] = false; //reset the boolean
					//System.out.println("all untried nodes have different labels");
					iNode1++;
					marcsCopies.put(iNode1, new Marcs<>(marcs)); 		//store a copy of MARCS, arcsleft  in  the workspace associated with node i 
					markAllAsUntried(iNode1); //mark all nodes of G2 as untried for node i
				} else {*/
					iNode1--;									// i := i-1
					if(iNode1 >= 0) marcs = new Marcs<>(marcsCopies.get(iNode1));				// restore marcs, arcsleft from the workspace associated with node i (arcsleft is ignored here, it is updated after refining)
				//}
			}
		}
		
		if(bestMarcs == null){
			// If there is no MCS by 
			return new McGregorResult<>(marcs.toEmptyGraphPair());
		} else {
			return bestMarcs.getResult();
		}
	}
	
	private boolean allPossibleNodesMapped(){
		boolean result = true;
		//System.out.println("Checking labelcount.....");
		for(int i=0;i<labels.size();i++){
			//System.out.println("testing (size==count): " + (labelsSize[i]) + "==" + marcs.getLabelCount(i));
			result &= (labelsSize[i]) == marcs.getLabelCount(i);
		}
		//System.out.println("Done checking labelcount result = " +result);
		//if(result) System.err.println(result + "---------------------------------");
		return result;
	}
	
	/**
	 * if there are any untried nodes in G2 to with node i (in this case v1) of G1 may correspond
	 * Does NOT check if the node labels match
	 * @param iNode1 a node number from g1
	 * @return a node number from g2 which is UNTRIED and has the same label as v1, or -1 when no untried node exists
	 */
	private int getUntriedNode(int iNode1){
		for(int v2=0;v2<nodes2.size();v2++){
			if(priority[iNode1][v2] && !nodesTried[iNode1][v2]){ //if this node combination is untried and in the priority subset
				return v2; 
			}
		}
		//when all nodes from the priorityList have been tried
		for(int v2=0;v2<nodes2.size();v2++){
			if(!nodesTried[iNode1][v2]){ //if this node combination has not been tried
				return v2; 
			}
		}
		return -1;
	}
	
	/**
	 * 
	 * @param iNode1 a node number from g1
	 */
	private void markAllAsUntried(int iNode1){
		for(int iv =0;iv<nodes2.size();iv++){ //Mark all nodes of G2 as untried for node i;
			nodesTried[iNode1][iv]= false;
			priority[iNode1][iv] = false;
		}
		for(V v2 : marcs.getPrioritySubset(nodes1.get(iNode1))){
			priority[iNode1][nodes2.indexOf(v2)] = true;
		}
	}
}

