/**
 */
package aatl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link aatl.ModuleElement#getModule <em>Module</em>}</li>
 * </ul>
 *
 * @see aatl.AatlPackage#getModuleElement()
 * @model abstract="true"
 * @generated
 */
public interface ModuleElement extends EObject {

	/**
	 * Returns the value of the '<em><b>Module</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link aatl.Module#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module</em>' container reference.
	 * @see #setModule(Module)
	 * @see aatl.AatlPackage#getModuleElement_Module()
	 * @see aatl.Module#getElements
	 * @model opposite="elements" transient="false"
	 * @generated
	 */
	Module getModule();

	/**
	 * Sets the value of the '{@link aatl.ModuleElement#getModule <em>Module</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Module</em>' container reference.
	 * @see #getModule()
	 * @generated
	 */
	void setModule(Module value);

	/**
	 * Accept a ModulePartVisitor.
	 * @param the visitor
	 * @generated NOT
	 */
    void accept(ModulePartVisitor visitor);
} // ModuleElement
