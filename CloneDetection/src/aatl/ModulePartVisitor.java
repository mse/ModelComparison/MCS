package aatl;

public interface ModulePartVisitor {
    void visit(Module module);
    void visit(MatchedRule matchedRule);
    void visit(InPattern inPattern);
    void visit(OutPattern outPattern);
    void visit(Filter filter);
    void visit(Variable outPattern);
    void visit(InPatternElement inPatternElement);
    void visit(OutPatternElement outPatternElement);
    void visit(Binding binding);
}