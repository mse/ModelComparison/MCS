/**
 */
package aatl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Pattern Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see aatl.AatlPackage#getInPatternElement()
 * @model
 * @generated
 */
public interface InPatternElement extends PatternElement {
} // InPatternElement
