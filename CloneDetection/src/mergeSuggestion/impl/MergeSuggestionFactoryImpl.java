/**
 */
package mergeSuggestion.impl;

import mergeSuggestion.MergeRule;
import mergeSuggestion.MergeRuleElement;
import mergeSuggestion.MergeSuggestion;
import mergeSuggestion.MergeSuggestionFactory;
import mergeSuggestion.MergeSuggestionPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MergeSuggestionFactoryImpl extends EFactoryImpl implements MergeSuggestionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MergeSuggestionFactory init() {
		try {
			MergeSuggestionFactory theMergeSuggestionFactory = (MergeSuggestionFactory)EPackage.Registry.INSTANCE.getEFactory(MergeSuggestionPackage.eNS_URI);
			if (theMergeSuggestionFactory != null) {
				return theMergeSuggestionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MergeSuggestionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeSuggestionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MergeSuggestionPackage.MERGE_SUGGESTION: return createMergeSuggestion();
			case MergeSuggestionPackage.MERGE_RULE: return createMergeRule();
			case MergeSuggestionPackage.MERGE_RULE_ELEMENT: return createMergeRuleElement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeSuggestion createMergeSuggestion() {
		MergeSuggestionImpl mergeSuggestion = new MergeSuggestionImpl();
		return mergeSuggestion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeRule createMergeRule() {
		MergeRuleImpl mergeRule = new MergeRuleImpl();
		return mergeRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeRuleElement createMergeRuleElement() {
		MergeRuleElementImpl mergeRuleElement = new MergeRuleElementImpl();
		return mergeRuleElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeSuggestionPackage getMergeSuggestionPackage() {
		return (MergeSuggestionPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MergeSuggestionPackage getPackage() {
		return MergeSuggestionPackage.eINSTANCE;
	}

} //MergeSuggestionFactoryImpl
