package icmt16rules.fmedit;

import java.io.IOException;


public class TestScanQat{

    public static void main(String[] args) throws IOException {
    
    	
        
        CdApplierCdEScan escan = new CdApplierCdEScan();
        escan.doCloneDetection();
        System.out.println("Escan done!");
        
        CdApplierCdConQat conqat = new CdApplierCdConQat();
        conqat.doCloneDetection();
        System.out.println("Conqat done!");
        
        CdApplierCdScanQat scanquat = new CdApplierCdScanQat();
    	scanquat.doCloneDetection();
        System.out.println("Scanqat done!");
        System.out.println("All done!");

    }
    
    
    
    public static void testCloneStuff() throws IOException {
    	CdApplierCdEScan escan = new CdApplierCdEScan();
        escan.doCloneDetection();
        System.out.println("Escan done!");
        
        CdApplierCdConQat conqat = new CdApplierCdConQat();
        conqat.doCloneDetection();
        System.out.println("Conqat done!");
        
        CdApplierCdScanQat scanquat = new CdApplierCdScanQat();
    	scanquat.doCloneDetection();
        System.out.println("Scanqat done!");
        System.out.println("All done!");
    }

}