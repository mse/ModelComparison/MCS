package icmt16rules.umledit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;
import org.eclipse.emf.henshin.variability.mergein.evaluation.RuleSetMetricsCalculator;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import icmt16rules.util.CloneComputationUtil;
import icmt16rules.util.WriteUtil;

public class CdApplierCdConQat {
	String example[] = { "umledit", "umleditoperations" };


	@Test
	public void doCloneDetection() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));
		StringBuilder resultTrace = new StringBuilder();
		String resultTracePath = path + "/" + locationPath + "/trace/conqat-results-" + fileName + ".txt";
		StringBuilder timeTrace = new StringBuilder();
		String timeTracePath = path + "/" + locationPath + "/trace/conqat-time-" + fileName + ".txt";
		StringBuilder metricsTrace = new StringBuilder();
//		String metricsTracePath = String.format("%s", System.getProperty("user.dir")) + "/" + example[0] + "/trace/metrics.txt";


		ModuleInfo moduleInfo = ModuleLoader.loadModule();
		for (String dir : moduleInfo.getDirectoriesToRules().keySet()) {
			doCloneDetection(false, dir, moduleInfo.getDirectoriesToRules().get(dir), resultTrace, timeTrace, metricsTrace);
		}

		doCloneDetection(true, "all", moduleInfo.getModule().getRules(), resultTrace, timeTrace, metricsTrace);

//		WriteUtil.write(metricsTrace, metricsTracePath);
		WriteUtil.write(resultTrace, resultTracePath);
		WriteUtil.write(timeTrace, timeTracePath);
	}

	private void doCloneDetection(boolean all, String name, List<Rule> relevant, StringBuilder resultTrace, StringBuilder timeTrace, StringBuilder metricsTrace) {
//		RuleSetMetricsCalculator calc = new RuleSetMetricsCalculator();
//		String pre = name + "\t" + calc.calculcate(relevant).createPresentationString();
//		WriteUtil.println(pre, metricsTrace);
//		if (1==1)return;
		
		long startTime = System.currentTimeMillis();

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(
				relevant, true);
		cq.detectCloneGroups();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		CloneGroupDetectionResult result = cq
				.getResultOrderedByNumberOfCommonElements();

		WriteUtil.println(name + "\t" + elapsedTime + "\t "
				+ result.getCloneGroups().size() + " entries in "
				+ relevant.size() + " rules.", timeTrace);

		CloneGroup l = CloneComputationUtil.getLargest(result.getCloneGroups(), true);
		CloneGroup b = CloneComputationUtil.getBroadest(result.getCloneGroups(), true);

		WriteUtil.printCloneGroup(l, name + "\tLargest", resultTrace);
		WriteUtil.printCloneGroup(b, "\tBroadest", resultTrace);
	}



}
