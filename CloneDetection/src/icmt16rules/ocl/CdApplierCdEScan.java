package icmt16rules.ocl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneGroupDetectionResultAsCloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.EScanDetectionArticleOriginal;
import icmt16rules.util.CloneComputationUtil;
import icmt16rules.util.CloneDetectorTask;
import icmt16rules.util.WriteUtil;

public class CdApplierCdEScan {
	String example[] = { "ocl", "OCL2NGC" };
	private static final String FILE_EXTENSION_HENSHIN = ".henshin";
	private static final int TIMEOUT_TIME = 60;
	private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MINUTES;


	@Test
	public void doCloneDetection() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));

		StringBuilder resultTrace = new StringBuilder();
		String resultTracePath = path + "/" + locationPath + "/trace/escan-results-" + fileName + ".txt";
		StringBuilder timeTrace = new StringBuilder();
		String timeTracePath = path + "/" + locationPath + "/trace/escan-time-" + fileName + ".txt";
		
		FileReader fr = new FileReader(locationPath+"/allRules");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();

		while (line != null) {
			String[] split = line.split(": ");
			String name = split[0];
			String[] relevant = split[1].split(" ");
			doCloneDetection(false, name, relevant, resultTrace, timeTrace);
			line = br.readLine();
		}

		doCloneDetection(true, "all", new String[0], resultTrace, timeTrace);
		
		WriteUtil.write(resultTrace, resultTracePath);
		WriteUtil.write(timeTrace, timeTracePath);
	}

	private void doCloneDetection(boolean all, String name, String[] relevant, StringBuilder resultTrace, StringBuilder timeTrace) {
		String locationPath = example[0];
		String fileName = example[1];
		HenshinResourceSet rs = new HenshinResourceSet(locationPath);	
		rs.registerDynamicEPackages("Pivot.ecore");

		initialize(locationPath, rs);
		Module module = (Module) rs.getEObject(fileName
				+ FILE_EXTENSION_HENSHIN);

		List<Rule> rules = getRelevantRules(module, all, relevant);

		long startTime = System.currentTimeMillis();


		EScanDetectionArticleOriginal detector = new EScanDetectionArticleOriginal(rules);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		boolean timeout = false;
	
		try {
			executor.submit(new CloneDetectorTask(detector)).get(TIMEOUT_TIME, TIMEOUT_UNIT);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			System.err.println("Timeout!");
			timeout = true;
		}

		if (!timeout) {
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			CloneGroupDetectionResultAsCloneMatrix result = detector
					.getResultAsCloneMatrixOrderedByNumberOfCommonElements();

			WriteUtil.println(name + "\t" + elapsedTime + "\t "
					+ result.getCloneGroups().size() + " entries in "
					+ rules.size() + " rules.", timeTrace);
			
			CloneMatrix l = CloneComputationUtil.getLargest(result.getCloneGroups());
			CloneMatrix b = CloneComputationUtil.getBroadest(result.getCloneGroups());
			WriteUtil.printCloneGroup(l, name + "\tLargest", resultTrace);
			WriteUtil.printCloneGroup(b, "\tBroadest", resultTrace);			
		} else {
			WriteUtil.println(name + "\t" + "--- (timeout after "+TIMEOUT_TIME+" "+TIMEOUT_UNIT+")",timeTrace);
			WriteUtil.println(name + "\t" + "--- ", resultTrace);
			WriteUtil.println("\t" + "--- ", resultTrace);
		}
	}

	
	private List<Rule> getRelevantRules(Module module, boolean all,
			String[] relevant) {
		ArrayList<Rule> result = new ArrayList<Rule>();
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(relevant));
	
		if (all) {
			for (Unit u : module.getUnits()) {
				if (u instanceof Rule) {
					Rule r = (Rule) u;
					// if (all || names.contains(r.getName()))
					result.add(r);
				}
			}
		} else {
			for (String n : names) {
				Unit u = module.getUnit(n);
				if (u == null) {
					System.err.println("Rule not found: " + n);
				} else
					result.add((Rule) u);
			}
		}
	
		return result;
	}

	private void initialize(String locationPath, HenshinResourceSet rs) {
		GraphConstraintPackage.eINSTANCE.eClass();
	}
}