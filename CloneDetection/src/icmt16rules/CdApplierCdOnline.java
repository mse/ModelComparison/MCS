package icmt16rules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;
import  pivot.PivotPackage;
import org.junit.Test;

import GraphConstraint.GraphConstraintPackage;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;

public class CdApplierCdOnline {
//	 String example[] = { "ocl", "OCL2NGC" };
	String example[] = { "fmedit", "featuremodeleditoperations" };
	private static final String FILE_PATH_EXPERIMENTAL_DATA = "exp/";
	private static final String FILE_EXTENSION_HENSHIN = ".henshin";

	//
	// private String name = "tr_E_15a";
	// private String[] relevant = { "tr_E_15a_body", "tr_E_15a_source",
	// "tr_E_15a_argument" };

	@Test
	public void testMerge() throws IOException {
		String locationPath = example[0];
		String fileName = example[1];
		String path = String.format("%s", System.getProperty("user.dir"));
		StringBuilder trace = new StringBuilder();
		String tracePath = path + "/" + locationPath + "/trace/online-time-" + fileName
				+ ".txt";
		
		
		FileReader fr = new FileReader(locationPath+"/allRules");
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();

		while (line != null) {
			String[] split = line.split(": ");
			String name = split[0];
			String[] relevant = split[1].split(" ");
			trace.append(testMerge(false, name, relevant));
			line = br.readLine();
		}

		trace.append(testMerge(true, "all", new String[0]));

		try {
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(tracePath));
			writer.write(trace.toString());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	private String testMerge(boolean all, String name, String[] relevant) {
		String locationPath = example[0];
		String fileName = example[1];
		HenshinResourceSet rs = new HenshinResourceSet(locationPath);
		initialize(locationPath, rs);
		Module module = (Module) rs.getEObject(fileName
				+ FILE_EXTENSION_HENSHIN);

		StringBuilder trace = new StringBuilder();
		List<Rule> rules = getRelevantRules(module, all, relevant);
//		StringBuilder ruleInfo = new StringBuilder();
//		ruleInfo.append("" + name + "\t" + rules.size() + "");
//		for (Rule r : rules)
//			ruleInfo.append(r.getName() + " ");
//		println(ruleInfo.toString(), trace);
//
//		RuleSetMetricsCalculator calc = new RuleSetMetricsCalculator();
//		String pre = name + "\t" + calc.calculcate(rules).createPresentationString();
//		println(pre, trace);

		long startTime = System.currentTimeMillis();

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(
				rules, true);
		cq.detectCloneGroups();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		CloneGroupDetectionResult result = cq
				.getResultOrderedByNumberOfCommonElements();
//		println("Took " + elapsedTime + " ms. Found "
//				+ result.getCloneGroups().size() + " entries in "
//				+ rules.size() + " rules.", trace);

		CloneGroup l = getLargest(result.getCloneGroups());
		CloneGroup b = getBroadest(result.getCloneGroups());

//		printCloneGroup(l, name + "\tLargest", trace);
//		printCloneGroup(b, "\tBroadest", trace);
		println(name+" \t "+elapsedTime, trace);
		return trace.toString();

	}

	private void printCloneGroup(CloneGroup r, String label, StringBuilder trace) {
		int ruleCount = r.getRules().size();
		String res = label + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 0;
		if (!r.getRules().isEmpty()) {
			 res = label + "\t" + ruleCount + "\t"
					+ r.getNodeMappings().size() / ruleCount + "\t"
					+ r.getEdgeMappings().size() / ruleCount + "\t"
					+ r.getAttributeMappings().size() / ruleCount;
		}
		println(res, trace);
	}

	private CloneGroup getBroadest(List<CloneGroup> cloneGroups) {
		if (cloneGroups.isEmpty())
			return new CloneGroup();
		CloneGroup broadest = cloneGroups.get(0);
		int maxBroadness = broadest.getRules().size();
		int maxSize = broadest.getSize();

		for (CloneGroup cg : cloneGroups) {
			if (cg.getRules().size() > maxBroadness) {
				broadest = cg;
				maxBroadness = broadest.getRules().size();
				maxSize = broadest.getSize();
			} else if (cg.getRules().size() > maxSize) { // size is secondary
															// criterion
				if (cg.getSize() > maxSize) {
					broadest = cg;
					maxBroadness = broadest.getRules().size();
					maxSize = broadest.getSize();
				}
			}
		}

		return broadest;
	}

	private CloneGroup getLargest(List<CloneGroup> cloneGroups) {
		if (cloneGroups.isEmpty())
			return new CloneGroup();

		CloneGroup largest = cloneGroups.get(0);
		int maxSize = largest.getSize();

		for (CloneGroup cg : cloneGroups) {
			if (cg.getSize() > maxSize) {
				largest = cg;
				maxSize = cg.getSize();
			}
		}

		return largest;
	}

	private Set<Rule> getUniqueRules(List<Rule> rules) {
		Set<Rule> result = new HashSet<Rule>();
		for (Rule r : rules) {
			result.add(r);
		}
		return result;
	}

	private List<Rule> getRelevantRules(Module module, boolean all,
			String[] relevant) {
		ArrayList<Rule> result = new ArrayList<Rule>();
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(relevant));

		if (all) {
			for (Unit u : module.getUnits()) {
				if (u instanceof Rule) {
					Rule r = (Rule) u;
					// if (all || names.contains(r.getName()))
					result.add(r);
				}
			}
		} else {
			for (String n : names) {
				Unit u = module.getUnit(n);
				if (u == null) {
					System.err.println("Rule not found: " + n);
				} else
					result.add((Rule) u);
			}
		}

		return result;
	}

	private void println(String string, StringBuilder trace) {
		System.out.println(string);
		trace.append(string);
		trace.append('\n');
	}

	private List<Rule> getRulesForIndependentExecution(Module module) {
		List<Rule> result = new ArrayList<Rule>();
		Unit indUnit = module.getUnit("closureIndependent");
		for (Unit u : indUnit.getSubUnits(false)) {
			if (u instanceof Rule) {
				result.add((Rule) u);
			}
		}
		return result;
	}

	private void initialize(String locationPath, HenshinResourceSet rs) {
		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();
		FeatureModelPackage.eINSTANCE.eClass();
	}
}
