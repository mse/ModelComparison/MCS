package de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel;

//http://stackoverflow.com/questions/11343848/
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import org.eclipse.emf.henshin.model.Node;

public class Permutation {

	public static List<List<Node>> permute(Set<Node> nodes) {

		if (nodes.size() == 1) {
			List<Node> arrayList = new ArrayList<Node>();
			arrayList.add(nodes.iterator().next());
			List<List<Node>> listOfList = new ArrayList<List<Node>>();
			listOfList.add(arrayList);
			return listOfList;
		}

		Set<Node> setOf = new HashSet<Node>(nodes);

		List<List<Node>> listOfLists = new ArrayList<List<Node>>();

		for (Node i : nodes) {
			ArrayList<Node> arrayList = new ArrayList<Node>();
			arrayList.add(i);

			Set<Node> setOfCopied = new HashSet<Node>();
			setOfCopied.addAll(setOf);
			setOfCopied.remove(i);

			Set<Node> isttt = new HashSet<Node>(setOfCopied);

			List<List<Node>> permute = permute(isttt);
			Iterator<List<Node>> iterator = permute.iterator();
			while (iterator.hasNext()) {
				List<Node> list = iterator.next();
				list.add(i);
				listOfLists.add(list);
			}
		}

		return listOfLists;
	}

}