package de.uni_marburg.fb12.swt.cloneDetection.atl.scanqat;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


import aatl.MatchedRule;
import aatl.Module;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneDetection;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.EScanDetectionOriginal;
import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.Fragment;

public abstract class HybridCloneDetector extends CloneDetection {
	public HybridCloneDetector(List<MatchedRule> rules) {
		super(rules);
	}

	public HybridCloneDetector(Set<Module> modules) {
		super(modules);
	}
	protected Set<Set<Fragment>> getGroupedAndFilteredCloneGroups(
			Map<Integer, Set<Fragment>> resConvert) {
		EScanDetectionOriginal eScanDetection = new EScanDetectionOriginal(
				ruleGraphMap, rules);

		int maxSizeFragments = CreateNewLatticeUtility
				.getMaxSizeFragments(resConvert);
		List<Set<Fragment>> lattice = CreateNewLatticeUtility.createLattice(
				resConvert, maxSizeFragments, maxSizeFragments);
		List<Set<Fragment>> cloneCandidates = new LinkedList<Set<Fragment>>();

		for (int i = 0; i < lattice.size(); i++) {
			Set<Fragment> set = new HashSet<Fragment>();
			set.addAll(lattice.get(i));
			cloneCandidates.add(set);
		}

		Set<Fragment> set = new HashSet<Fragment>();
		cloneCandidates.add(set);

		Set<Set<Fragment>> cloneGroups;
		cloneGroups = eScanDetection.eScanGroupAndFilterLattice(lattice);
		return cloneGroups;
	}

}
