package de.uni_marburg.fb12.swt.cloneDetection.atl.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import aatl.Module;

public class IOUtil {
	private static final String FILE_NAME_RULES_CLASSIC = ".atl";
	private static String CLUSTER_DESCRIPTION_FILE = "input\\clusters.txt";


	 static  List<Path> getLocations(String path) {
		List<Path> result = new ArrayList<Path>();
		try {
			Files.walk(Paths.get(path)).filter(Files::isRegularFile)
					.filter(s -> s.toString().endsWith(FILE_NAME_RULES_CLASSIC)).forEach(f -> result.add(f));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	static List<List<Module>> readClustersFromFile(List<Module> modules) {
		List<List<Module>> result = new ArrayList<List<Module>>();
		try (BufferedReader br = new BufferedReader(new FileReader(CLUSTER_DESCRIPTION_FILE))) {
			for (String line; (line = br.readLine()) != null;) {
				List<Module> entry = new ArrayList<Module>();
				String[] names = line.split("&")[0].split(" ");
				for (String name : names) {
					Module module = modules.stream().filter(m -> m.getName().equals(name)).findFirst().get();
					entry.add(module);
				}
				result.add(entry);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
