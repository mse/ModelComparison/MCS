package de.uni_marburg.fb12.swt.cloneDetection.atl.test;

import java.util.concurrent.Callable;

import de.uni_marburg.fb12.swt.cloneDetection.atl.escan.CloneDetection;



public class CloneDetectorTask implements Callable {
	CloneDetection cloneDetector;

	public CloneDetectorTask(CloneDetection cloneDetector) {
		this.cloneDetector = cloneDetector;
	}

	@Override
	public Object call() throws Exception {
		cloneDetector.detectCloneGroups();
		return cloneDetector;
	}

}
