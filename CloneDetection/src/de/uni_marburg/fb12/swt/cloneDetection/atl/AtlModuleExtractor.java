package de.uni_marburg.fb12.swt.cloneDetection.atl;

import java.awt.image.RescaleOp;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.common.ATL.Binding;
import org.eclipse.m2m.atl.common.ATL.InPatternElement;
import org.eclipse.m2m.atl.common.ATL.MatchedRule;
import org.eclipse.m2m.atl.common.ATL.Module;
import org.eclipse.m2m.atl.common.ATL.ModuleElement;
import org.eclipse.m2m.atl.common.ATL.OutPatternElement;
import org.eclipse.m2m.atl.common.ATL.RuleVariableDeclaration;
import org.eclipse.m2m.atl.common.OCL.OclExpression;
import org.eclipse.m2m.atl.common.OCL.OclModelElement;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.engine.parser.AtlParser;

import aatl.Filter;
import aatl.Variable;
import aatl.impl.AatlFactoryImpl;

public class AtlModuleExtractor {

	private List<String> currentFile;
	private Encoding currentEncoding;

	public ExtractionTrace extractAbstractedModule(Path path) {
		ExtractionTrace tr = null;
		List<Encoding> en = new ArrayList<Encoding>(Arrays.asList(Encoding.values()));
		while (tr == null && !en.isEmpty()) { // try out all known encodings
												// until one of them fits
			currentEncoding = en.get(0);
			en.remove(currentEncoding);
			try {
				EObject o = AtlParser.getDefault().parse(Files.newInputStream(path));
				currentFile = Files.readAllLines(path, Charset.forName(currentEncoding.getValue()));
				if (o instanceof Module) {
					tr = extractModule(o);
				} else { // Some ATL files do not contain a module.
					tr = new ExtractionTrace(null, null);
				}
			} catch (MalformedInputException e) {
				continue;
			} catch (ATLCoreException | IOException e) {
				e.printStackTrace();
			}
		}
		if (tr == null)
			throw new RuntimeException("Error while extracting module: " + path);
		return tr;
	}

	/**
	 * 
	 * @param paths
	 * @param keepClashes
	 *            How to deal with name clashes. If true, rename and keep
	 *            clashing modules. If false, discard clashing modules.
	 * @return
	 */
	public List<aatl.Module> extractAbstractedModules(List<Path> paths, boolean keepClashes) {
		List<aatl.Module> result = new ArrayList<aatl.Module>();
		List<String> existing = new ArrayList<String>();
		for (Path path : paths) {
			AtlModuleExtractor extractor = new AtlModuleExtractor();
			ExtractionTrace trace = extractor.extractAbstractedModule(path);
			aatl.Module module = trace.getOutModule();
			if (module == null)
				continue;

			if (keepClashes) {
				String originalName = module.getName();
				int index = 1;
				while (existing.contains(module.getName())) {
					index++;
					module.setName(originalName + index);
				}
				result.add(module);
				existing.add(module.getName());
			} else {
				if (!existing.contains(module.getName())) {
					result.add(module);
					existing.add(module.getName());
				}
			}
		}

		return result;
	}

	private ExtractionTrace extractModule(EObject o) {
		Module inModule = (Module) o;
		aatl.Module outModule = AatlFactoryImpl.eINSTANCE.createModule();
		outModule.setName(inModule.getName());
		ExtractionTrace tr = new ExtractionTrace(inModule, outModule);

		for (ModuleElement e : inModule.getElements()) {
			if (e instanceof MatchedRule) {
				MatchedRule in = (MatchedRule) e;
				aatl.MatchedRule out = AatlFactoryImpl.eINSTANCE.createMatchedRule();
				extractContents(in, out, tr);
				outModule.getElements().add(out);
			}
		}

		return tr;
	}

	private void extractContents(MatchedRule in, aatl.MatchedRule out, ExtractionTrace tr) {
		out.setName(in.getName());
		tr.trace(in, out);
		extractInElements(in, out, tr);
		extractVariables(in, out, tr);
		extractOutElements(in, out, tr);
	}

	private void extractVariables(MatchedRule in, aatl.MatchedRule out, ExtractionTrace tr) {
		EList<RuleVariableDeclaration> invars = in.getVariables();
		for (RuleVariableDeclaration invar : invars) {
			Variable outvar = AatlFactoryImpl.eINSTANCE.createVariable();
			outvar.setVarName(invar.getVarName());
			if (invar.getType() instanceof OclModelElement)
				outvar.setTypeModelName(((OclModelElement) invar.getType()).getModel().getName());
			outvar.setTypeName(invar.getType().getName());
			outvar.setInitExpression(restoreCS(invar.getInitExpression()));
			// getValue(invar.getVariableExp()); // "calls" of the variable.
			out.getVariables().add(outvar);
		}
	}

	private void extractInElements(MatchedRule inRule, aatl.MatchedRule outRule, ExtractionTrace tr) {
		if (inRule.getInPattern() == null)
			return;
			
		outRule.setInPattern(AatlFactoryImpl.eINSTANCE.createInPattern());
		tr.trace(inRule.getInPattern(), outRule.getInPattern());
		if (inRule.getInPattern().getFilter() != null) {
			Filter filter = AatlFactoryImpl.eINSTANCE.createFilter();
			filter.setValue(restoreCS(inRule.getInPattern().getFilter()));
			outRule.getInPattern().setFilter(filter);
		}
		
		for (InPatternElement inElem : inRule.getInPattern().getElements()) {
			aatl.InPatternElement outElem = AatlFactoryImpl.eINSTANCE.createInPatternElement();
			outElem.setVarName(inElem.getVarName());
			if (inElem.getType() instanceof OclModelElement)
				outElem.setTypeModelName(((OclModelElement) inElem.getType()).getModel().getName());
			outElem.setTypeName(inElem.getType().getName());
			outRule.getInPattern().getElements().add(outElem);
			tr.trace(inElem, outElem);
		}
	}

	private void extractOutElements(MatchedRule inRule, aatl.MatchedRule outRule, ExtractionTrace tr) {
		if (inRule.getOutPattern() == null)
			return;

		outRule.setOutPattern(AatlFactoryImpl.eINSTANCE.createOutPattern());
		tr.trace(inRule.getOutPattern(), outRule.getOutPattern());
		for (OutPatternElement inElem : inRule.getOutPattern().getElements()) {
			aatl.OutPatternElement outElem = AatlFactoryImpl.eINSTANCE.createOutPatternElement();
			outElem.setVarName(inElem.getVarName());
			if (inElem.getType() instanceof OclModelElement)
				outElem.setTypeModelName(((OclModelElement) inElem.getType()).getModel().getName());
			outElem.setTypeName(inElem.getType().getName());

			for (Binding b : inElem.getBindings()) {
				aatl.Binding outB = AatlFactoryImpl.eINSTANCE.createBinding();
				outB.setPropertyName(b.getPropertyName());
				outB.setValue(restoreCS(b.getValue()));
				outElem.getBindings().add(outB);
				tr.trace(b, outB);
			}
			outRule.getOutPattern().getElements().add(outElem);
			tr.trace(inElem, outElem);
		}
	}

	/**
	 * Restores an OCL expression in its concrete syntax.
	 * 
	 * @param value
	 * @return
	 */
	private String restoreCS(OclExpression value) {
		String locationString = value.getLocation();
		String[] locations = locationString.split("-");
		int lineNoStart = Integer.parseInt(locations[0].split(":")[0]) - 1;
		int start = Integer.parseInt(locations[0].split(":")[1]) - 1;
		int lineNoFin = Integer.parseInt(locations[1].split(":")[0]) - 1;
		int fin = Integer.parseInt(locations[1].split(":")[1]) - 1;
		if (lineNoStart == lineNoFin) {
			String line = currentFile.get(lineNoStart);
			return line.substring(start, fin);
		} else {
			StringBuilder result = new StringBuilder();
			result.append(currentFile.get(lineNoStart).substring(start));
			for (int i = lineNoStart + 1; i < lineNoFin; i++) {
				result.append(trim(currentFile.get(i)));
			}
			result.append(trim(currentFile.get(lineNoFin).substring(0, fin)));
			return result.toString();
		}
	}

	/**
	 * trim() for OCL expressions that are spread over multiple lines. Applies
	 * Java's String.trim() while trying to produce correct OCL expressions (for
	 * example, "if\nnot" becomes "if not" rather than "ifnot").
	 * 
	 * @param substring
	 * @return
	 */
	private String trim(String substring) {
		String result = substring.trim();
		if (result.length() > 0) {
			if ((result.charAt(0) != '.') && (result.charAt(0) != '-'))
				result = new String(" " + result);
		}
		return result;
	}

	public void saveModule(aatl.Module module, String uri) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();
		Resource resource = resSet.createResource(URI.createURI(uri));
		resource.getContents().add(module);
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	enum Encoding {
		UTF8("UTF-8"), ISO88591("ISO-8859-1"), ISO885915("ISO-8859-15"), UTF16("UTF-16");
		String value;

		public String getValue() {
			return value;
		}

		Encoding(String value) {
			this.value = value;
		}
	}
}
