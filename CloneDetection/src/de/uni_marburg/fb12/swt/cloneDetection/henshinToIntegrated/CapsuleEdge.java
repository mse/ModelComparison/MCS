package de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated;

import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.graph.DefaultEdge;
import org.eclipse.emf.ecore.EReference;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;

/**
 * 
 * (Attribute and (Original-)Edges (org.eclipse.emf.henshin.model.Edge) are
 * capsuled in CapsuleEdges
 *
 */
@SuppressWarnings("serial")
public class CapsuleEdge extends DefaultEdge {
	private Edge originalEdge;
	private boolean isAttributeEdge;
	private Attribute attribute;

	public CapsuleEdge() {
		super();
		isAttributeEdge = false;
	}

	@Override
	public int hashCode() {
		if (isAttributeEdge) {
			return attribute.hashCode();
		} else {
			return originalEdge.hashCode();
		}
	}

	@Override
	public boolean equals(Object o) {
		CapsuleEdge e = (CapsuleEdge) o;
		if (isAttributeEdge != e.isAttributeEdge()) {
			return false;
		} else {
			if (isAttributeEdge) {
				return attribute == e.getAttribute();
				// return attribute.equals(e.getAttribute());
			} else {
				return originalEdge == e.getOriginalEdge();
				// return edge.equals(e.getEdge());
			}

		}
	}

	@Override
	public CapsuleEdge clone() {
		if (isAttributeEdge) {
			return new CapsuleEdge(originalEdge, attribute);
		} else {
			return new CapsuleEdge(originalEdge);
		}
	}

	public boolean isAttributeEdge() {
		return isAttributeEdge;
	}

	public CapsuleEdge(Edge edge) {
		super();
		this.originalEdge = edge;
		isAttributeEdge = false;
	}

	public CapsuleEdge(Edge edge, Attribute attribute) {
		super();
		this.originalEdge = edge;
		isAttributeEdge = true;
		this.attribute = attribute;
	}

	public Action getAction() {
//		if (!isAttributeEdge) {
			return originalEdge.getAction();
//		} else {
//			System.out
//					.println("CapsuleEdge.getAction() - "
//							+ "CapsuleEdge contains an Attribute - no Action available");
//			return null;
//		}
	}

	public EReference getType() {
//		if (!isAttributeEdge) {
			return originalEdge.getType();
//		} else {
//			System.out
//					.println("CapsuleEdge.getType() - "
//							+ "CapsuleEdge contains an Attribute - no edgeType available");
//			return null;
//		}
	}

	public Rule getRule() {
		if (!isAttributeEdge) {
			return originalEdge.getGraph().getRule();
		} else {
			return attribute.getGraph().getRule();
		}
	}

	public Edge getOriginalEdge() {
//		if (!isAttributeEdge) {
			return originalEdge;
//		} else {
//			System.out
//					.println("CapsuleEdge.getOriginalEdge() - "
//							+ "CapsuleEdge contains an Attribute - no (original-)edge available");
//			return null;
//		}
	}

	public Attribute getAttribute() {
//		if (isAttributeEdge) {
			return attribute;
//		} else {
//			System.out
//					.println("CapsuleEdge.getAttribute() - "
//							+ "CapsuleEdge contains an (original-)Edge - no Attribute available");
//			return null;
//		}
	}

	@Override
	public String toString() {
		if (isAttributeEdge) {
			String res = attribute.getNode().toString() + " -> "
					+ LabelCreator.getCapsuleEdgeToString(this) + "\t";
			return res;
		} else {
			String res = originalEdge.getSource().toString() + " -> "
					+ LabelCreator.getCapsuleEdgeToString(this) + " -> "
					+ originalEdge.getTarget().toString() + "\t";
			return res;
		}
	}
}
