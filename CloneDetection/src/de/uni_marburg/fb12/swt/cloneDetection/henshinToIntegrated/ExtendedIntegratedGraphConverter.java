package de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.MappingList;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Action.Type;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.LabelCreator;

/**
 * 
 * extended: Attributes are transformed in additional Nodes and (Capsule-)Edges
 *
 */

public class ExtendedIntegratedGraphConverter {

	/**
	 * the extended Version
	 * 
	 * @param rules
	 * @return
	 */
	public Map<Rule, DirectedGraph<Node, CapsuleEdge>> getRuleToGraphWithAttributesMap(
			Collection<Rule> rules) {
		Map<Rule, DirectedGraph<Node, CapsuleEdge>> result = new HashMap<Rule, DirectedGraph<Node, CapsuleEdge>>();

		for (Rule rule : rules) {
			try {
				DirectedGraph<Node, CapsuleEdge> graph = createIntegratedGraph(
						rule, true);
				result.put(rule, graph);
			} catch (InputRuleNotSupportedException e) {
				System.out.println("Skipping rule " + rule.getName() + ": "
						+ e.getMessage());
			}

		}
		return result;
	}

	public DirectedGraph<Node, CapsuleEdge> createIntegratedGraph(Rule rule,
			boolean includeRhs) throws InputRuleNotSupportedException {
		checkPrecondition(rule);
		DirectedGraph<Node, CapsuleEdge> result = new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);

		// Ignoring kernel rule name: We look for clones between rules
		addGraphElementsToResult(rule, "", result, includeRhs);

		for (Rule multiRule : rule.getMultiRules()) {
			String ruleName = determineRuleName(rule, multiRule);
			addGraphElementsToResult(multiRule, ruleName, result, includeRhs);
		}

		return result;
	}

	private String determineRuleName(Rule rule, Rule multiRule) {
		if (multiRule.getName() != null && multiRule.getName().length() > 0)
			return multiRule.getName();
		if (rule.getMultiRules().size() == 1)
			return "mul";
		else {
			int i = 0;
			String result = "multi0";
			while (rule.getMultiRule(result) != null) {
				i++;
				result = "multi" + i;
			}
			return result;
		}
	}

	private void addGraphElementsToResult(Rule rule, String ruleName,
			DirectedGraph<Node, CapsuleEdge> result, boolean includeRhS) {
		Set<Type> types = getRelevantTypes(includeRhS);
		for (Type type : types) {
			for (Node n : getActionNodes(rule, new Action(type))) {
				// HenshinNode node = new HenshinNode(result, n.getType(), type,
				// ruleName);
				result.addVertex(n);
			}
		}

		for (Type type : types) {
			for (Edge edge : getActionEdges(rule, new Action(type))) {
				Node source = edge.getSource().getActionNode();
				Node target = edge.getTarget().getActionNode();
				CapsuleEdge capsuleEdge = new CapsuleEdge(edge);
				result.addEdge(source, target, capsuleEdge);
			}
		}

		for (Type type : types) {
			for (Attribute attribute : getActionAttributes(rule, new Action(
					type))) {
				Node node = HenshinFactory.eINSTANCE.createNode();
				node.setName(LabelCreator.getAttributeNodeName(attribute));
				result.addVertex(node);

				Edge originalEdge = HenshinFactory.eINSTANCE.createEdge();

				CapsuleEdge capsuleEdge = new CapsuleEdge(originalEdge,
						attribute);

				if(!result.addEdge(attribute.getNode().getActionNode(),
						node, capsuleEdge))
					result.removeVertex(node);
			}
		}
	}

	private Set<Type> getRelevantTypes(boolean includeRhs) {
		Set<Type> set = new HashSet<Type>();
		set.add(Type.DELETE);
		set.add(Type.FORBID);
		set.add(Type.PRESERVE);
		set.add(Type.REQUIRE);
		if (includeRhs) {
			set.add(Type.CREATE);
		}
		return set;
	}

	private void checkPrecondition(Rule rule)
			throws InputRuleNotSupportedException {
		// if (rule.getMultiRules().size() > 1)
		// throw new
		// InputRuleNotSupportedException("Currently support only 1 nested rule.");
//		if (rule.getLhs().getNACs().size() > 1)
//			throw new InputRuleNotSupportedException(
//					"Currently only supporting 1 NAC per rule.");
//		if (rule.getLhs().getPACs().size() > 1)
//			throw new InputRuleNotSupportedException(
//					"Currently only supporting 1 PAC per rule.");
//		if (rule.getLhs().getNACs().size() == 1)
//			if (!rule.getLhs().getNACs().get(0).getConclusion()
//					.getNestedConditions().isEmpty())
//				throw new InputRuleNotSupportedException(
//						"Currently not supporting nesting of application conditions.");
//		if (rule.getLhs().getPACs().size() == 1)
//			if (!rule.getLhs().getPACs().get(0).getConclusion()
//					.getNestedConditions().isEmpty())
//				throw new InputRuleNotSupportedException(
//						"Currently not supporting nesting of application conditions.");

	}

	private List<Node> getActionNodes(Rule rule, Action action) {
		ArrayList<Node> result = new ArrayList<Node>();
		TreeIterator<EObject> it = rule.eAllContents();
		while (it.hasNext()) {
			EObject current = it.next();
			if (current instanceof Node) {
				Node node = (Node) current;
				if (action.equals(node.getAction()))
					result.add(node);
			}
		}
		return result;
	}
	

	private List<Edge> getActionEdges(Rule rule, Action action) {
		ArrayList<Edge> result = new ArrayList<Edge>();
		TreeIterator<EObject> it = rule.eAllContents();
		while (it.hasNext()) {
			EObject current = it.next();
			if (current instanceof Edge) {
				Edge edge = (Edge) current;
				if (action.equals(edge.getAction()))
					result.add(edge);
			}
		}
		return result;
	}

	private List<Attribute> getActionAttributes(Rule rule, Action action) {
		ArrayList<Attribute> result = new ArrayList<Attribute>();
		TreeIterator<EObject> it = rule.eAllContents();
		while (it.hasNext()) {
			EObject current = it.next();
			if (current instanceof Node) {
				result.addAll(((Node) current).getActionAttributes(action));
			}
		}
		return result;
	}

	// public RuleToHenshinGraphMap createIntegratedGraphs(List<Rule> rules,
	// boolean includeRhs) {
	// RuleToHenshinGraphMap result = new RuleToHenshinGraphMap();
	// for (Rule rule : rules) {
	// try {
	// result.put(rule, createIntegratedGraph(rule, includeRhs));
	// } catch (InputRuleNotSupportedException e) {
	// System.out.println("Skipping rule " + rule.getName() + ": "
	// + e.getMessage());
	// }
	// }
	// return result;
	// }
}
