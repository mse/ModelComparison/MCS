package de.uni_marburg.fb12.swt.cloneDetection.variability;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Mapping;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import  pivot.PivotPackage;
import org.jgrapht.DirectedGraph;

import GraphConstraint.GraphConstraintPackage;

public class CloneGroupMappingToVariabilityHenshinRule {

	private static final String PRESERVE = (new Action(Action.Type.PRESERVE))
			.toString();
	private static final String DELETE = (new Action(Action.Type.DELETE)).toString();
	private static final String CREATE = (new Action(Action.Type.CREATE)).toString();
	private static final String FORBID = (new Action(Action.Type.FORBID)).toString();
	private static final String REQUIRE = (new Action(Action.Type.REQUIRE)).toString();

	public static Module getNewModule(Module oldModule, Set<Rule> rules,
			CloneGroupMapping cloneGroupMapping, String name) {
		Module module = createModul(oldModule);
		DirectedGraph<Node, CapsuleEdge> graph 
				= CloneGoupMappingToVariabilityActionNamedDirectedGraph
				.convert(cloneGroupMapping, rules);
		Rule rule = getRuleGraph(graph, name);
		Set<String> vPoints = new HashSet<String>();
		for (Rule r : rules) {
			vPoints.add(r.getName());
		}

		setForbidden(rule, vPoints);

		module.getUnits().add(rule);
		return module;
	}

	private static Module createModul(Module oldModule) {

		PivotPackage.eINSTANCE.eClass();
		GraphConstraintPackage.eINSTANCE.eClass();

		Module module = HenshinFactory.eINSTANCE.createModule();
		for (EPackage p : oldModule.getImports()) {
			module.getImports().add(p);
		}
		return module;
	}

	/**
	 * 
	 * @param rule
	 * @param vPoints
	 */
	private static void setForbidden(Rule rule, Set<String> vPoints) {
		String value = "xor (";
		Iterator<String> stringIterator = vPoints.iterator();
		while (stringIterator.hasNext()) {
			value = value + stringIterator.next();
			if (stringIterator.hasNext()) {
				value = value + ", ";
			}
		}
		value = value + ")";
		// rule.setFeatureModell(value);
		rule.setForbiddenVariants(value);
	}

	private static Rule getRuleGraph(DirectedGraph<Node, CapsuleEdge> graph,
			String name) {
		Rule r = HenshinFactory.eINSTANCE.createRule(name);

		Graph lhs = HenshinFactory.eINSTANCE.createGraph("LHS");
		Graph rhs = HenshinFactory.eINSTANCE.createGraph("RHS");

		r.setLhs(lhs);
		r.setRhs(rhs);

		// for forbid
		NestedCondition ncN = lhs.createNAC("");
		Graph nac = ncN.getConclusion();
		// for require
		NestedCondition ncP = lhs.createPAC("");
		Graph pac = ncP.getConclusion();

		Map<Node, Node> formerToNewNodeLhs = new HashMap<Node, Node>();
		Map<Node, Node> formerToNewNodeRhs = new HashMap<Node, Node>();
		Map<Node, Node> formerToNewNodeNac = new HashMap<Node, Node>();
		Map<Node, Node> formerToNewNodePac = new HashMap<Node, Node>();

		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(PRESERVE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(lhs,
						n.getType(), null);
				Node node2 = HenshinFactory.eINSTANCE.createNode(rhs,
						n.getType(), null);

				node1.setPresenceCondition(n.getPresenceCondition());
				node2.setPresenceCondition(n.getPresenceCondition());

				List<Attribute> attributes1 = new LinkedList<Attribute>();
				List<Attribute> attributes2 = new LinkedList<Attribute>();

				for (Attribute a : n.getAttributes()) {
					Attribute a1 = HenshinFactory.eINSTANCE.createAttribute();
					Attribute a2 = HenshinFactory.eINSTANCE.createAttribute();
					
					a1.setType(a.getType());
					a2.setType(a.getType());
					a1.setValue(a.getValue());
					a2.setValue(a.getValue());
					a1.setPresenceCondition(a.getPresenceCondition());
					a2.setPresenceCondition(a.getPresenceCondition());
					attributes1.add(a1);
					attributes2.add(a2);
				}

				node1.getAttributes().addAll(attributes1);
				node2.getAttributes().addAll(attributes2);

				lhs.getNodes().add(node1);
				rhs.getNodes().add(node2);

				Mapping mapping = HenshinFactory.eINSTANCE.createMapping(node1,
						node2); // ode1, node2);
				r.getMappings().add(mapping);

				formerToNewNodeLhs.put(n, node1);
				formerToNewNodeRhs.put(n, node2);

			} else if (n.getName().equals(CREATE)) {

				Node node2 = HenshinFactory.eINSTANCE.createNode(rhs,
						n.getType(), null);
				node2.setPresenceCondition(n.getPresenceCondition());

				List<Attribute> attributes2 = new LinkedList<Attribute>();

				for (Attribute a : n.getAttributes()) {
					Attribute a2 = HenshinFactory.eINSTANCE.createAttribute();
					a2.setType(a.getType());
					a2.setValue(a.getValue());
					a2.setPresenceCondition(a.getPresenceCondition());
					attributes2.add(a2);
				}

				node2.getAttributes().addAll(attributes2);

				rhs.getNodes().add(node2);

				formerToNewNodeRhs.put(n, node2);

			} else if (n.getName().equals(DELETE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(lhs,
						n.getType(), null);
				node1.setPresenceCondition(n.getPresenceCondition());

				List<Attribute> attributes1 = new LinkedList<Attribute>();

				for (Attribute a : n.getAttributes()) {
					Attribute a1 = HenshinFactory.eINSTANCE.createAttribute();
					a1.setType(a.getType());
					a1.setValue(a.getValue());
					a1.setPresenceCondition(a.getPresenceCondition());
					attributes1.add(a1);
				}

				node1.getAttributes().addAll(attributes1);

				rhs.getNodes().add(node1);

				formerToNewNodeLhs.put(n, node1);
			}
		}

		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(FORBID)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(nac,
						n.getType(), null);
				node1.setPresenceCondition(n.getPresenceCondition());

				List<Attribute> attributes1 = new LinkedList<Attribute>();

				for (Attribute a : n.getAttributes()) {
					Attribute a1 = HenshinFactory.eINSTANCE.createAttribute();
					a1.setType(a.getType());
					a1.setValue(a.getValue());
					a1.setPresenceCondition(a.getPresenceCondition());
					attributes1.add(a1);
				}

				node1.getAttributes().addAll(attributes1);

				nac.getNodes().add(node1);

				formerToNewNodeNac.put(n, node1);

				// forbid Edges
				for (CapsuleEdge ce : (graph.incomingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node sourceNode = e.getSource();
					if (sourceNode == null) {
						sourceNode = graph.getEdgeSource(ce);
					}
					Node node1b = HenshinFactory.eINSTANCE.createNode(nac,
							sourceNode.getType(), null);
					node1b.setPresenceCondition(sourceNode
							.getPresenceCondition());

					List<Attribute> attributes1b = new LinkedList<Attribute>();
					for (Attribute a : sourceNode.getAttributes()) {
						Attribute a1b = HenshinFactory.eINSTANCE
								.createAttribute();
						a1b.setType(a.getType());
						a1b.setValue(a.getValue());
						a1b.setPresenceCondition(a.getPresenceCondition());
						attributes1b.add(a1b);
					}
					node1b.getAttributes().addAll(attributes1b);
					nac.getNodes().add(node1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(sourceNode)), node1b);
					ncN.getMappings().add(mapping);

					formerToNewNodeNac.put(sourceNode, node1b);

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1b, formerToNewNodeNac.get(n), e.getType());
					createEdge1.setPresenceCondition(e.getPresenceCondition());
					nac.getEdges().add(createEdge1);

				}

				for (CapsuleEdge ce : (graph.outgoingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node targetNode = e.getTarget();
					if (targetNode == null) {
						targetNode = graph.getEdgeTarget(ce);
					}
					Node node1b = HenshinFactory.eINSTANCE.createNode(nac,
							targetNode.getType(), null);
					node1b.setPresenceCondition(targetNode
							.getPresenceCondition());

					List<Attribute> attributes1b = new LinkedList<Attribute>();
					for (Attribute a : targetNode.getAttributes()) {
						Attribute a1b = HenshinFactory.eINSTANCE
								.createAttribute();
						a1b.setType(a.getType());
						a1b.setPresenceCondition(a.getPresenceCondition());
						a1b.setValue(a.getValue());
						attributes1b.add(a1b);
					}
					node1b.getAttributes().addAll(attributes1b);
					nac.getNodes().add(node1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(targetNode)), node1b);
					ncN.getMappings().add(mapping);

					formerToNewNodeNac.put(targetNode, node1b);

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeNac.get(n), node1b, e.getType());
					createEdge1.setPresenceCondition(e.getPresenceCondition());
					nac.getEdges().add(createEdge1);

				}
			}
		}

		for (Node n : graph.vertexSet()) {
			if (n.getName().equals(REQUIRE)) {
				Node node1 = HenshinFactory.eINSTANCE.createNode(pac,
						n.getType(), null);
				node1.setPresenceCondition(n.getPresenceCondition());

				List<Attribute> attributes1 = new LinkedList<Attribute>();

				for (Attribute a : n.getAttributes()) {
					Attribute a1 = HenshinFactory.eINSTANCE.createAttribute();
					a1.setType(a.getType());
					a1.setValue(a.getValue());
					a1.setPresenceCondition(a.getPresenceCondition());
					attributes1.add(a1);
				}

				node1.getAttributes().addAll(attributes1);

				pac.getNodes().add(node1);

				formerToNewNodePac.put(n, node1);

				// require Edges
				for (CapsuleEdge ce : (graph.incomingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node sourceNode = e.getSource();
					if (sourceNode == null) {
						sourceNode = graph.getEdgeSource(ce);
					}
					Node node1b = HenshinFactory.eINSTANCE.createNode(pac,
							sourceNode.getType(), null);
					node1b.setPresenceCondition(sourceNode
							.getPresenceCondition());

					List<Attribute> attributes1b = new LinkedList<Attribute>();
					for (Attribute a : sourceNode.getAttributes()) {
						Attribute a1b = HenshinFactory.eINSTANCE
								.createAttribute();
						a1b.setType(a.getType());
						a1b.setValue(a.getValue());
						a1b.setPresenceCondition(a.getPresenceCondition());
						attributes1b.add(a1b);
					}
					node1b.getAttributes().addAll(attributes1b);
					pac.getNodes().add(node1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(sourceNode)), node1b);
					ncP.getMappings().add(mapping);

					formerToNewNodeNac.put(sourceNode, node1b);

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							node1b, formerToNewNodeNac.get(n), e.getType());
					createEdge1.setPresenceCondition(e.getPresenceCondition());
					pac.getEdges().add(createEdge1);

				}

				for (CapsuleEdge ce : (graph.outgoingEdgesOf(n))) {
					Edge e = ce.getOriginalEdge();
					Node targetNode = e.getTarget();
					if (targetNode == null) {
						targetNode = graph.getEdgeTarget(ce);
					}
					Node node1b = HenshinFactory.eINSTANCE.createNode(pac,
							targetNode.getType(), null);
					node1b.setPresenceCondition(targetNode
							.getPresenceCondition());

					List<Attribute> attributes1b = new LinkedList<Attribute>();
					for (Attribute a : targetNode.getAttributes()) {
						Attribute a1b = HenshinFactory.eINSTANCE
								.createAttribute();
						a1b.setType(a.getType());
						a1b.setPresenceCondition(a.getPresenceCondition());
						a1b.setValue(a.getValue());
						attributes1b.add(a1b);
					}
					node1b.getAttributes().addAll(attributes1b);
					pac.getNodes().add(node1b);

					Mapping mapping = HenshinFactory.eINSTANCE.createMapping(
							(formerToNewNodeLhs.get(targetNode)), node1b);
					ncP.getMappings().add(mapping);

					formerToNewNodeNac.put(targetNode, node1b);

					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeNac.get(n), node1b, e.getType());
					createEdge1.setPresenceCondition(e.getPresenceCondition());
					pac.getEdges().add(createEdge1);

				}
			}
		}

		// remaining edges

		for (Object tempEdge : graph.edgeSet()) {
			CapsuleEdge e = (CapsuleEdge) tempEdge;
			Node source = graph.getEdgeSource(e);
			Node target = graph.getEdgeTarget(e);

			if (source.getName().equals(PRESERVE)) {
				if (target.getName().equals(PRESERVE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					createEdge1.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					createEdge2.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getLhs().getEdges().add(createEdge1);
					r.getRhs().getEdges().add(createEdge2);
				}
				if (target.getName().equals(CREATE)) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					createEdge2.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getRhs().getEdges().add(createEdge2);
				}
				if (target.getName().equals(DELETE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					createEdge1.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getLhs().getEdges().add(createEdge1);
				}

			}

			if (source.getName().equals(CREATE)) {
				if ((target.getName().equals(PRESERVE))) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					createEdge2.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getRhs().getEdges().add(createEdge2);
				}

				if ((target.getName().equals(CREATE))) {
					Edge createEdge2 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeRhs.get(source),
							formerToNewNodeRhs.get(target), e.getType());
					createEdge2.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getRhs().getEdges().add(createEdge2);
				}

			}

			if (source.getName().equals(DELETE)) {
				if (target.getName().equals(PRESERVE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					createEdge1.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getLhs().getEdges().add(createEdge1);
				}

				if (target.getName().equals(DELETE)) {
					Edge createEdge1 = HenshinFactory.eINSTANCE.createEdge(
							formerToNewNodeLhs.get(source),
							formerToNewNodeLhs.get(target), e.getType());
					createEdge1.setPresenceCondition(e.getOriginalEdge()
							.getPresenceCondition());
					r.getLhs().getEdges().add(createEdge1);
				}
			}

		}

		return r;
	}

}
