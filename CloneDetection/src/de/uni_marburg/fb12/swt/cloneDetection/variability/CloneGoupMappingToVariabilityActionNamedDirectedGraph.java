package de.uni_marburg.fb12.swt.cloneDetection.variability;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map;

import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * 
 * the Actions of the ActionNodes of the Nodes could not be set, since the
 * name-field of the Nodes is not used so far the actions of the Nodes will be
 * stored as a String in the name field of the Nodes
 */
public class CloneGoupMappingToVariabilityActionNamedDirectedGraph {
	private static final Action PRESERVE = new Action(Action.Type.PRESERVE);
	private static final Action DELETE = new Action(Action.Type.DELETE);
	private static final Action CREATE = new Action(Action.Type.CREATE);
	private static final Action FORBID = new Action(Action.Type.FORBID);
	private static final Action REQUIRE = new Action(Action.Type.REQUIRE);

	public static DirectedGraph<Node, CapsuleEdge> convert(
			CloneGroupMapping cloneGroupMapping, Set<Rule> rules) {
		if (cloneGroupMapping == null) {
			return null;
		}
		List<Rule> ruleList = new LinkedList<Rule>();
		for (Rule rule : rules) {
			ruleList.add(rule);
		}
		ResultObject ro = convertWithoutAttributes(
				cloneGroupMapping.getEdgeMappings(), ruleList);
		DirectedGraph<Node, CapsuleEdge> variabilityGraph = ro
				.getVariabilityGraph();

		if (cloneGroupMapping.getAttributeMappings().size() > 0) {
			Map<Node, Node> originalActionNodesToNewNodes = ro
					.getOriginalActionNodesToNewNodes();
			Map<Node, Node> orginalActionNodeFirstRuleInCommonPartToNewNode = ro
					.getOrginalActionNodeFirstRuleInCommonPartToNewNode();
			variabilityGraph = addAttributes(variabilityGraph,
					cloneGroupMapping.getAttributeMappings(),
					originalActionNodesToNewNodes,
					orginalActionNodeFirstRuleInCommonPartToNewNode, ruleList);
		}
		return variabilityGraph;
	}

	private static DirectedGraph<Node, CapsuleEdge> addAttributes(
			DirectedGraph<Node, CapsuleEdge> variabilityGraph,
			Map<Attribute, Map<Rule, Attribute>> attributeMappings,
			Map<Node, Node> originalActionNodesToNewNodes,
			Map<Node, Node> orginalActionNodeFirstRuleInCommonPartToNewNode,
			List<Rule> ruleList) {

		Set<Attribute> originalAttributesIncludedInCommonParts = new HashSet<Attribute>();
		Set<Rule> ruleSet = new HashSet<Rule>();
		ruleSet.addAll(ruleList);
		Map<Rule, Set<Attribute>> attributeMap = getAttributeMap(
				attributeMappings, ruleSet);
		// set commonAttributes based on firstRule
		Rule firstRule = ruleList.get(0);
		for (Attribute attributeFirstRule : attributeMap.get(firstRule)) {
			Node originalActionNode = attributeFirstRule.getNode()
					.getActionNode();
			Map<Rule, Attribute> tempMap = attributeMappings
					.get(attributeFirstRule);
			if (orginalActionNodeFirstRuleInCommonPartToNewNode.keySet()
					.contains(originalActionNode)) {
				Node node = orginalActionNodeFirstRuleInCommonPartToNewNode
						.get(originalActionNode);
				for (Node n : variabilityGraph.vertexSet()) {
					if (node == n) {
						Attribute attribute = HenshinFactory.eINSTANCE
								.createAttribute(n,
										attributeFirstRule.getType(),
										attributeFirstRule.getValue());
						n.getAttributes().add(attribute);
						for (Rule r : ruleList) {
							originalAttributesIncludedInCommonParts.add(tempMap
									.get(r));
						}
					}
				}
			}
		}
		// extend to variabilityParts
		for (Rule rule : ruleList) {
			Set<Node> actionNodeSet = new HashSet<Node>();
			actionNodeSet.addAll(rule.getActionNodes(PRESERVE));
			actionNodeSet.addAll(rule.getActionNodes(CREATE));
			actionNodeSet.addAll(rule.getActionNodes(DELETE));
			actionNodeSet.addAll(rule.getActionNodes(FORBID));
			actionNodeSet.addAll(rule.getActionNodes(REQUIRE));

			Set<Attribute> allAttributes = new HashSet<Attribute>();
			for (Node n : actionNodeSet) {
				for (Attribute a : n.getAttributes()) {
					allAttributes.add(a);
				}
			}

			for (Attribute attribute : allAttributes) {
				if (!originalAttributesIncludedInCommonParts
						.contains(attribute)) {
					Node originalActionNode = attribute.getNode()
							.getActionNode();

					Node node = originalActionNodesToNewNodes
							.get(originalActionNode);
					for (Node n : variabilityGraph.vertexSet()) {
						if (node == n) {
							Attribute newAttribute = HenshinFactory.eINSTANCE
									.createAttribute(n, attribute.getType(),
											attribute.getValue());
							String condition = "def(" + rule.getName() + ")";
							newAttribute.setPresenceCondition(condition);
							n.getAttributes().add(newAttribute);
						}
					}
				}
			}

		}

		return variabilityGraph;
	}

	private static ResultObject convertWithoutAttributes(
			Map<Edge, Map<Rule, Edge>> edgeMappings, List<Rule> ruleList) {
		// create cloneGraphCommonParts based on firstRule
		DirectedGraph<Node, CapsuleEdge> cloneGraphCommonParts 
							= new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);
		Set<Rule> ruleSet = new HashSet<Rule>();
		ruleSet.addAll(ruleList);
		Map<Rule, Set<Edge>> edgeMap = getEdgeMap(edgeMappings, ruleSet);
		Map<Node, Node> originalActionNodesToNewNodes = new HashMap<Node, Node>();
		Map<Edge, List<Edge>> edgeToOriginalEdge = new HashMap<Edge, List<Edge>>();

		Set<Node> originalNodesIncludedInCommonParts = new HashSet<Node>();
		Set<Edge> originalEdgesIncludedInCommonParts = new HashSet<Edge>();

		Rule firstRule = ruleList.get(0);
		Map<Node, Node> orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode 
														= new HashMap<Node, Node>();

		for (Edge edgeFirstRule : edgeMap.get(firstRule)) {
			Node nodeSourceFirstRule = edgeFirstRule.getSource()
					.getActionNode();
			Node nodeTargetFirstRule = edgeFirstRule.getTarget()
					.getActionNode();
			Node nodeS;
			Node nodeT;

			Map<Rule, Edge> tempMap = edgeMappings.get(edgeFirstRule);
			List<Edge> edgeList = new LinkedList<Edge>();

			for (Rule r : ruleList) {
				edgeList.add(tempMap.get(r));
				originalEdgesIncludedInCommonParts.add(tempMap.get(r));
			}
			edgeToOriginalEdge.put(edgeFirstRule, edgeList);

			if (!orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode
					.containsKey(nodeSourceFirstRule)) {
				nodeS = HenshinFactory.eINSTANCE.createNode();
				nodeS.setType(nodeSourceFirstRule.getType());
				Action action = nodeSourceFirstRule.getActionNode().getAction();
				nodeS.setName(action.toString());
				for (Rule r : ruleList) {
					Node n = tempMap.get(r).getSource();
					originalActionNodesToNewNodes.put(n.getActionNode(), nodeS);
					originalNodesIncludedInCommonParts.add(n);
				}
				orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode.put(
						nodeSourceFirstRule, nodeS);
				cloneGraphCommonParts.addVertex(nodeS);
			} else {
				nodeS = orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode
						.get(nodeSourceFirstRule);
			}

			if (!orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode
					.containsKey(nodeTargetFirstRule)) {
				nodeT = HenshinFactory.eINSTANCE.createNode();
				nodeT.setType(nodeTargetFirstRule.getType());
				Action action = nodeTargetFirstRule.getActionNode().getAction();

				nodeT.setName(action.toString());
				for (Rule r : ruleList) {
					Node n = tempMap.get(r).getTarget();
					originalActionNodesToNewNodes.put(n.getActionNode(), nodeT);
					originalNodesIncludedInCommonParts.add(n);
				}
				orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode.put(
						nodeTargetFirstRule, nodeT);
				cloneGraphCommonParts.addVertex(nodeT);
			} else {
				nodeT = orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode
						.get(nodeTargetFirstRule);
			}

			CapsuleEdge capsuleEdge = new CapsuleEdge(edgeFirstRule);
			cloneGraphCommonParts.addEdge(nodeS, nodeT, capsuleEdge);
		}

		// extend to variabilityGraph
		DirectedGraph<Node, CapsuleEdge> variabilityGraph 
				= new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);
		for (Node node : cloneGraphCommonParts.vertexSet()) {
			variabilityGraph.addVertex(node);
		}

		for (CapsuleEdge edge : cloneGraphCommonParts.edgeSet()) {
			variabilityGraph.addEdge(cloneGraphCommonParts.getEdgeSource(edge),
					cloneGraphCommonParts.getEdgeTarget(edge), edge);
		}

		for (Rule rule : ruleList) {
			Set<Node> actionNodeSet = new HashSet<Node>();
			actionNodeSet.addAll(rule.getActionNodes(PRESERVE));
			actionNodeSet.addAll(rule.getActionNodes(CREATE));
			actionNodeSet.addAll(rule.getActionNodes(DELETE));
			actionNodeSet.addAll(rule.getActionNodes(FORBID));
			actionNodeSet.addAll(rule.getActionNodes(REQUIRE));

			for (Node actionNode : actionNodeSet) {

				if (!originalActionNodesToNewNodes.containsKey(actionNode)) {
					Node newNode = HenshinFactory.eINSTANCE.createNode();
					newNode.setType(actionNode.getType());
					Action action = actionNode.getActionNode().getAction();
					newNode.setName(action.toString());
					String condition = "def(" + rule.getName() + ")";
					newNode.setPresenceCondition(condition);
					originalActionNodesToNewNodes.put(actionNode, newNode);

					variabilityGraph.addVertex(newNode);
				}
			}
		}

		for (Rule rule : ruleList) {
			Set<Edge> edgeSet = new HashSet<Edge>();
			edgeSet.addAll(rule.getActionEdges(PRESERVE));
			edgeSet.addAll(rule.getActionEdges(CREATE));
			edgeSet.addAll(rule.getActionEdges(DELETE));
			edgeSet.addAll(rule.getActionEdges(FORBID));
			edgeSet.addAll(rule.getActionEdges(REQUIRE));

			for (Edge edge : edgeSet) {
				if (!originalEdgesIncludedInCommonParts.contains(edge)) {

					Node originalActionNodeS = edge.getSource().getActionNode();
					Node newNodeS = originalActionNodesToNewNodes
							.get(originalActionNodeS);

					Node originalActionNodeT = edge.getTarget().getActionNode();
					Node newNodeT = originalActionNodesToNewNodes
							.get(originalActionNodeT);

					
					Edge newEdge = HenshinFactory.eINSTANCE.createEdge();
					newEdge.setType(edge.getType());
					String condition = "def(" + rule.getName() + ")";
					newEdge.setPresenceCondition(condition);
					CapsuleEdge capsuleEdge = new CapsuleEdge(newEdge);
					variabilityGraph.addEdge(newNodeS, newNodeT, capsuleEdge);
				}
			}
		}
		ResultObject resultObject = new ResultObject(variabilityGraph,
				originalActionNodesToNewNodes,
				orginalActionNodeFirstRuleAlreadyAddToCommonPartToNewNode);
		return resultObject;
	}

	private static Map<Rule, Set<Edge>> getEdgeMap(
			Map<Edge, Map<Rule, Edge>> edgeMapping, Set<Rule> rules) {
		Map<Rule, Set<Edge>> res = new HashMap<Rule, Set<Edge>>();

		Collection<Map<Rule, Edge>> tempMaps = edgeMapping.values();
		for (Map<Rule, Edge> tempMap : tempMaps) {
			for (Rule rule : tempMap.keySet()) {
				if (rules.contains(rule)) {
					if (res.containsKey(rule)) {
						res.get(rule).add(tempMap.get(rule));
					} else {
						Set<Edge> set = new HashSet<Edge>();
						set.add(tempMap.get(rule));
						res.put(rule, set);
					}
				}
			}

		}
		return res;
	}

	private static Map<Rule, Set<Attribute>> getAttributeMap(
			Map<Attribute, Map<Rule, Attribute>> attributeMappings,
			Set<Rule> rules) {
		Map<Rule, Set<Attribute>> res = new HashMap<Rule, Set<Attribute>>();

		Collection<Map<Rule, Attribute>> tempMaps = attributeMappings.values();
		for (Map<Rule, Attribute> tempMap : tempMaps) {
			for (Rule rule : tempMap.keySet()) {
				if (rules.contains(rule)) {
					if (res.containsKey(rule)) {
						res.get(rule).add(tempMap.get(rule));
					} else {
						Set<Attribute> set = new HashSet<Attribute>();
						set.add(tempMap.get(rule));
						res.put(rule, set);
					}
				}
			}

		}
		return res;
	}

}
