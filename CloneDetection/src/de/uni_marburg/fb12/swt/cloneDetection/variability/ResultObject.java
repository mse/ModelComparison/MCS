package de.uni_marburg.fb12.swt.cloneDetection.variability;

import java.util.Map;

import org.eclipse.emf.henshin.model.Node;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

public class ResultObject {
	private DirectedGraph<Node, CapsuleEdge> variabilityGraph;
	private Map<Node, Node> originalActionNodesToNewNodes;
	private Map<Node, Node> orginalActionNodeFirstRuleInCommonPartToNewNode;

	public ResultObject(DirectedGraph<Node, CapsuleEdge> variabilityGraph,
			Map<Node, Node> originalActionNodesToNewNodes,
			Map<Node, Node> orginalActionNodeFirstRuleInCommonPartToNewNode) {
		this.variabilityGraph = variabilityGraph;
		this.originalActionNodesToNewNodes = originalActionNodesToNewNodes;
		this.orginalActionNodeFirstRuleInCommonPartToNewNode 
				= orginalActionNodeFirstRuleInCommonPartToNewNode;
	}

	public DirectedGraph<Node, CapsuleEdge> getVariabilityGraph() {
		return variabilityGraph;
	}

	public Map<Node, Node> getOriginalActionNodesToNewNodes() {
		return originalActionNodesToNewNodes;
	}

	public Map<Node, Node> getOrginalActionNodeFirstRuleInCommonPartToNewNode() {
		return orginalActionNodeFirstRuleInCommonPartToNewNode;
	}

}
