package de.uni_marburg.fb12.swt.cloneDetection.cloneDetection;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

/**
 * 
 * Since there are normal nodes and attribute nodes, some utility methods are
 * needed to tell them apart.
 * 
 *
 */
public class NodeUtility {

	public static DirectedGraph<Node, CapsuleEdge> getGraph(Node node,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Rule rule = getRule(node, ruleGraphMap);
		return ruleGraphMap.get(rule);
	}

	/**
	 * 
	 * @param node
	 * @param ruleGraphMap
	 * @return the Rule this node belongs to
	 */
	public static Rule getRule(Node node,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		if (node.getActionNode() != null) {
			Rule rule = node.getActionNode().getGraph().getRule();
			if (rule != null) {
				return rule;
			}
		}
		// else node is AttributeNode
		String ruleNames = "";
		Set<Rule> rules = new HashSet<Rule>();
		for (Rule rule : ruleGraphMap.keySet()) {
			DirectedGraph<Node, CapsuleEdge> graph = ruleGraphMap.get(rule);
			if (graph.containsVertex(node)) {
				rules.add(rule);
			} else {
				ruleNames = ruleNames + " " + rule.getName();
			}
		}
		if (rules.size() != 1) {
			//System.out.println("rules.size()!=1 " + node.toString());
			//System.out.println("NodeUtility-getRule: nicht in: " + ruleNames);
		}
		// if(!("Node " +
		// rules.iterator().next().getName()).equals(node.toString()))
		// System.out.println(rules.iterator().next().getName() + " " +
		// node.toString());
		return rules.iterator().next();
	}

	/**
	 * 
	 * @param
	 * @param graph
	 *            has to be the computation- or the fragment-graph, not the
	 *            original-/henshin Graph
	 * @return the attribute that is capsuled inside the node and it�s incoming
	 *         edge null if node is not a attributeNode
	 * 
	 */
	public static Attribute getAttribute(Node node,
			DirectedGraph<Node, CapsuleEdge> graph) {
		if (!NodeUtility.isAttributeNode(node, graph)) {
			return null;
		} else {
			CapsuleEdge capsuleEdge = graph.incomingEdgesOf(node).iterator()
					.next();
			return capsuleEdge.getAttribute();
		}
	}

	/**
	 * Checks if node is an attributeNode, by checking if the Node has an
	 * incomming CapsuleEdge that is an attributeEdge
	 * 
	 * @param node
	 * @param graph
	 *            the graph the node belongs to (has to be the computation- or
	 *            the fragment-graph, not the original-/henshin Graph)
	 * @return true if node is an attributeNode, false else, undefined if node
	 *         is not a Vertex of graph
	 */
	public static boolean isAttributeNode(Node node,
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<CapsuleEdge> capsuleEdges = graph.incomingEdgesOf(node);
		if (capsuleEdges.size() == 0) {
			return false;
		}
		CapsuleEdge capsuleEdge = capsuleEdges.iterator().next();
		if (capsuleEdge.isAttributeEdge()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param node
	 *            the Node of which the adjacent nodes are returned
	 * @param graph
	 *            he graph the node belongs to (has to be the computation- or
	 *            the fragment-graph, not the original-/henshin Graph)
	 * @param considerAttributeNodes
	 *            true - all adjacentNodes will be returned false - only the
	 *            nonAttributeNodes will be returned
	 * @return the adjacent Nodes of node in the graph, undefined if node is not
	 *         a Vertex of graph
	 */
	public static Set<Node> getAdjacentNodes(Node node,
			DirectedGraph<Node, CapsuleEdge> graph,
			boolean considerAttributeNodes) {
		Set<Node> adjacentNodes = getAdjacentNodes(node, graph);

		if (considerAttributeNodes) {
			return adjacentNodes;
		} else {
			return removeAttributeNodes(adjacentNodes, graph);
		}
	}

	/**
	 * Returns all nodes that have identical labels, uses LabelCreator.
	 * 
	 * @param graph
	 *            has to be the computation- or the fragment-graph, not the
	 *            original-/henshin Graph
	 * @return all Nodes of the graph that have the same Label
	 */
	public static Set<Node> getDuplikateNodes(
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<Node> res = new HashSet<Node>();
		for (Node node : graph.vertexSet()) {
			for (Node n : graph.vertexSet()) {
				if (node != n) {
					if (LabelCreator.getModelCdNodeLabel(node, graph).equals(
							LabelCreator.getModelCdNodeLabel(n, graph))) {
						res.add(node);
						res.add(n);
					}
				}
			}
		}
		return res;
	}

	/**
	 * Returns all CapsuleEdges that have identical labels and whose source- and
	 * target-nodes have identical labels, uses LabelCreator.
	 * 
	 * @param graph
	 *            has to be the computation- or the fragment-graph, not the
	 *            original-/henshin Graph
	 * @return all Nodes of the graph that have identical labels and whose
	 *         source- and target-nodes have identical labels.
	 */
	public static Set<CapsuleEdge> getDuplikateCapsuleEdges(
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<CapsuleEdge> res = new HashSet<CapsuleEdge>();
		for (CapsuleEdge capsuleEdge : graph.edgeSet()) {
			for (CapsuleEdge ce : graph.edgeSet()) {
				if (capsuleEdge != ce) {
					if (LabelCreator.getSimpleModelCdEdgeLabel(capsuleEdge,
							graph).equals(
							LabelCreator.getSimpleModelCdEdgeLabel(ce, graph))) {
						res.add(capsuleEdge);
						res.add(ce);
					}
				}
			}
		}
		return res;
	}

	/**
	 * removes all attributeNodes from the given node set
	 * 
	 * @param adjacentNodes
	 * @param graph
	 * @return
	 */
	private static Set<Node> removeAttributeNodes(Set<Node> adjacentNodes,
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<Node> adjacentNodesTemp = new HashSet<Node>();
		for (Node node : adjacentNodes) {
			if (!isAttributeNode(node, graph)) {
				adjacentNodesTemp.add(node);
			}
		}
		adjacentNodes = adjacentNodesTemp;
		return adjacentNodes;
	}

	/**
	 * 
	 * @param node
	 *            the Node of which the adjacent nodes are returned
	 * @param graph
	 *            he graph the node belongs to (has to be the computation- or
	 *            the fragment-graph, not the original-/henshin Graph)
	 * @return the adjacent Nodes of node in the graph, undefined if node is not
	 *         a Vertex of graph
	 */
	public static Set<Node> getAdjacentNodes(Node node,
			DirectedGraph<Node, CapsuleEdge> graph) {
		Set<Node> adjacentNodes = new HashSet<Node>();
		Set<CapsuleEdge> edges = new HashSet<CapsuleEdge>();
		edges.addAll(graph.incomingEdgesOf(node));
		edges.addAll(graph.outgoingEdgesOf(node));
		for (CapsuleEdge e : edges) {
			Node source = graph.getEdgeSource(e);
			Node target = graph.getEdgeTarget(e);
			if (node != source) {
				adjacentNodes.add(source);
			}
			if (node != target) {
				adjacentNodes.add(target);
			}
			if ((node != source) && (node != target)) {
				System.out.println("Fehler in NodeUtility.getAdjacentNodes");
			}
		}
		return adjacentNodes;
	}
}
