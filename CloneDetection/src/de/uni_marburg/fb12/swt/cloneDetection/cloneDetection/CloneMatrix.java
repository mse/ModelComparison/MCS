package de.uni_marburg.fb12.swt.cloneDetection.cloneDetection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Edge;

import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

public class CloneMatrix {
	private List<List<Edge>> edgeMatrix = new LinkedList<List<Edge>>();
	private List<List<Attribute>> attributeMatrix = new LinkedList<List<Attribute>>();

	public CloneMatrix(){
	}

	/**
	 * The order of the rows of edgeMatrix and attributeMatrix have to be
	 * according each other, means the k�ths row of both matrices have to belong
	 * to the same clone
	 * 
	 * @param rules
	 * @param edgeMatrix
	 * @param attributeMatrix
	 */
	public CloneMatrix(List<List<Edge>> edgeMatrix,
			List<List<Attribute>> attributeMatrix) {
		this.edgeMatrix = edgeMatrix;
		this.attributeMatrix = attributeMatrix;
	}

	private int getNumberOfEdgeColumns() {
		if (edgeMatrix.size() != 0) {
			return edgeMatrix.get(0).size();
		} else {
			return 0;
		}
	}

	private int getNumberOfAttributeColumns() {
		if (attributeMatrix.size() != 0) {
			return attributeMatrix.get(0).size();
		} else {
			return 0;
		}

	}

	/**
	 * !losing information, in case there are rule-intern clones, since in this
	 * case there would be more than one edge / attribute per rule
	 * 
	 * @return the CloneMatrix as a CloneGroupMapping
	 */
	public CloneGroupMapping getAsCloneGroupMapping() {
		Map<Edge, Map<Rule, Edge>> edgeMappings = new HashMap<Edge, Map<Rule, Edge>>();
		Map<Attribute, Map<Rule, Attribute>> attributeMappings 
									= new HashMap<Attribute, Map<Rule, Attribute>>();
		for (int column = 0; column < getNumberOfEdgeColumns(); column++) {
			Map<Rule, Edge> columnEdgeMapping = new HashMap<Rule, Edge>();
			// !losing information, in case there are rule-intern clones,
			// since in this case there would be more
			// than one edge per rule
			for (List<Edge> row : edgeMatrix) {
				Edge edge = row.get(column);
				columnEdgeMapping.put(edge.getGraph().getRule(), edge);
			}
			for (List<Edge> row : edgeMatrix) {
				Edge edge = row.get(column);
				edgeMappings.put(edge, columnEdgeMapping);
			}
		}
		for (int column = 0; column < getNumberOfAttributeColumns(); column++) {
			Map<Rule, Attribute> columnAttributeMapping = new HashMap<Rule, Attribute>();
			// !losing information, in case there are rule-intern clones,
			// since in this case there would be more
			// than one attribute per rule
			for (List<Attribute> row : attributeMatrix) {
				Attribute attribute = row.get(column);
				columnAttributeMapping.put(attribute.getGraph().getRule(),
						attribute);
			}
			for (List<Attribute> row : attributeMatrix) {
				Attribute attribute = row.get(column);
				attributeMappings.put(attribute, columnAttributeMapping);
			}

		}

		CloneGroupMapping cloneGroupMapping = new CloneGroupMapping(
				getRuleSet(), edgeMappings, attributeMappings);

		return cloneGroupMapping;
	}

	/**
	 * 
	 * @return the rules of the clonematrices as a List
	 */
	public List<Rule> getRuleList() {
		List<Rule> allRules = new LinkedList<Rule>();
		if (edgeMatrix.size() == 0)
			return allRules;
		if (edgeMatrix.get(0).size() != 0) {
			for (List<Edge> row : edgeMatrix) {
				allRules.add(row.get(0).getGraph().getRule());
			}
			return allRules;
		} else {
			for (List<Attribute> row : attributeMatrix) {
				if (!row.isEmpty())
					allRules.add(row.get(0).getGraph().getRule());
			}
		}
		return allRules;
	}

	/**
	 * 
	 * @return the rules of the clonematrices as a Set
	 */
	public Set<Rule> getRuleSet() {
		List<Rule> allRules = getRuleList();
		Set<Rule> res = new HashSet<Rule>();
		res.addAll(allRules);
		return res;
	}

	/**
	 * 
	 * @return all Edges of this CloneMatrix
	 */
	public Set<Edge> getAllEdges() {
		Set<Edge> allEdges = new HashSet<Edge>();
		for (List<Edge> row : edgeMatrix) {
			allEdges.addAll(row);
		}
		return allEdges;
	}
	/**
	 * 
	 * @return all Edges of this CloneMatrix
	 */
	public Set<Node> getAllNodes() {
		Set<Node> allNodes = new HashSet<Node>();
		
		for (Edge e : getAllEdges()) {
			allNodes.add(e.getSource().getActionNode());
			allNodes.add(e.getTarget().getActionNode());
		}
		
		return allNodes;
	}
	

	/**
	 * 
	 * @return all Attributes of this CloneMatrix
	 */
	public Set<Attribute> getAllAttributes() {
		Set<Attribute> allAttributes = new HashSet<Attribute>();
		for (List<Attribute> row : attributeMatrix) {
			allAttributes.addAll(row);
		}
		return allAttributes;
	}

	public List<List<Edge>> getEdgeMatrix() {
		return edgeMatrix;
	}

	public List<List<Attribute>> getAttributeMatrix() {
		return attributeMatrix;
	}

	public int getNumberOfCommonNodes() {
		return getAllNodes().size() / getRuleList().size();
	}

	/**
	 * 
	 * @return the number of cloned Edges (since this is Edges and not
	 *         CapsuleEdges, this means the former CapsuleEdges, which capsuled
	 *         Attributes are not inculded)
	 */
	public int getNumberOfCommonEdges() {
		if (edgeMatrix.size() != 0) {
			return edgeMatrix.get(0).size();
		}
		return 0;
	}

	/**
	 * 
	 * @return the number of cloned Attributes
	 */
	public int getNumberOfCommonAttributes() {
		if (attributeMatrix.size() != 0) {
			return attributeMatrix.get(0).size();
		}
		return 0;
	}

	@Override
	public int hashCode() {
		int res = 0;
		if (edgeMatrix != null) {
			res = res + edgeMatrix.hashCode();
		}
		if (attributeMatrix != null) {
			res = res + attributeMatrix.hashCode();
		}
		return res;
	}

	@Override
	public boolean equals(Object o) {
		CloneMatrix c = (CloneMatrix) o;
		if (!this.edgeMatrix.equals(c.edgeMatrix)) {
			return false;
		}
			
		if (!this.attributeMatrix.equals(c.attributeMatrix)) {
			return false;
		}
			
		return true;
	}

	@Override
	public String toString() {
		String res = "";
		res = res + "\n" + "CloneMatrix - edgeMatrix: " + "\n"
				+ toStringEdgeMatrix(edgeMatrix);
		res = res + "\n" + "CloneMatrix - attributeMatrix: " + "\n"
				+ toStringAttributeMatrix(attributeMatrix) + "\n" + "\n";
		return res;
	}

	private String toStringEdgeMatrix(List<List<Edge>> edgeMatrix) {
		String res = "";
		for (List<Edge> row : edgeMatrix) {
			for (Edge edge : row) {
				res = res + edge.toString() + "\t" + "*" + "\t";

			}
			res = res + "\n";
		}
		return res;
	}

	private String toStringAttributeMatrix(List<List<Attribute>> attributeMatrix) {
		String res = "";
		for (List<Attribute> row : attributeMatrix) {
			for (Attribute attribute : row) {
				res = res + attribute.toString() + "\t" + "*" + "\t";

			}
			res = res + "\n";
		}
		return res;
	}
	
	public int getSize() {
		return getNumberOfCommonNodes()+getNumberOfCommonAttributes()+getNumberOfCommonEdges();
	}

}
