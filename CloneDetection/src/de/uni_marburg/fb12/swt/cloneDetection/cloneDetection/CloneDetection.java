package de.uni_marburg.fb12.swt.cloneDetection.cloneDetection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.ui.clonedetector.AbstractCloneGroupDetector;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.ExtendedIntegratedGraphConverter;


/**
 * The top-level CloneDetection class.
 * 
 */
public abstract class CloneDetection extends AbstractCloneGroupDetector {
	// AbstractCloneGroupDetector:
	// protected Collection<Rule> rules;
	// protected Set<CloneGroupMapping> result;

	/**
	 * The result of the CloneDetection.
	 */
	protected Set<CloneMatrix> resultAsCloneMatrix;

	/**
	 * Due to the way the rules are selected in ui, there are no duplicated
	 * rules.
	 */
	protected final Set<Rule> ruleSet;

	/**
	 * Rule to computation-Graph
	 */
	protected final Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap;

	/**
	 * Ordered rules
	 */
	protected final List<Rule> ruleList;

	/**
	 * Mostly true, for optimization Step1 mostly false
	 */
	protected final boolean considerAttributeNodes;

	public CloneDetection(Collection<Rule> rules) {
		super(rules);
		ruleSet = getRuleSet(rules);
		ruleGraphMap = getRuleGraphMap(ruleSet);
		ruleList = getRuleList(ruleSet);
		considerAttributeNodes = true;
		resultAsCloneMatrix = new HashSet<CloneMatrix>();
		this.result = null;
	}

	public abstract void detectCloneGroups();

	public CloneDetection(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap.keySet());
		this.ruleSet = ruleGraphMap.keySet();
		this.ruleGraphMap = ruleGraphMap;
		this.ruleList = ruleList;
		this.considerAttributeNodes = considerAttributeNodes;
		resultAsCloneMatrix = new HashSet<CloneMatrix>();
	}

	public Set<CloneMatrix> getResultAsCloneMatrix() {
		return resultAsCloneMatrix;
	}

	/**
	 * In CloneDetective and CloneDetectiveTupel: getSimilarityFunctionValue
	 * specifies how many terms are used
	 * 
	 * @return
	 */
	protected int getNumberOfTermsForCloneDetective() {
		// no special reason
		return 3;
	}

	/**
	 * Due to the way the rules are selected in ui, there are no duplicated
	 * rules.
	 * 
	 * @param rules
	 *            --> Constructor
	 * @return rules as Set
	 */
	protected Set<Rule> getRuleSet(Collection<Rule> rules) {
		Set<Rule> ruleSet = new HashSet<Rule>();
		ruleSet.addAll(rules);
		return ruleSet;
	}

	/**
	 * Building up the computation graphs to each rule.
	 * 
	 * (It�s very important, especially for the combine algorithm, that the
	 * RuleGraphMap isn�t created a second time. (Due to the fact that the
	 * Attributes are converted into Nodes, and this Nodes are new created, the
	 * nodes of a new generated graph are equal to the Nodes of the former
	 * graph, but not identical, and JGraph tests for identity, means a new
	 * computation of ruleGraphMap could lead to a lot of errors.)
	 *
	 * @return rule to rule-computation-graph
	 */
	private Map<Rule, DirectedGraph<Node, CapsuleEdge>> getRuleGraphMap(
			Set<Rule> ruleSet) {
		ExtendedIntegratedGraphConverter graphConverter = new ExtendedIntegratedGraphConverter();
		return graphConverter.getRuleToGraphWithAttributesMap(ruleSet);
		// return graphConverter.getRuleToGraphWithOutAttributesMap(ruleSet);
	}

	/**
	 * Put rules in order.
	 * 
	 * @return
	 */
	protected List<Rule> getRuleList(Set<Rule> ruleSet) {
		List<Rule> ruleList = new LinkedList<Rule>();
		ruleList.addAll(ruleSet);
		//System.out.println("Rules: ");
		for (Rule rule : ruleList) {
			//System.out.println(rule.getName());
		}
		return ruleList;
	}

	/**
	 * from org.eclipse.emf.henshin.variability.ui.clonedetector.
	 * AbstractCloneGroupDetector
	 * 
	 * @return
	 */
	public CloneGroupDetectionResultAsCloneMatrix 
				getResultAsCloneMatrixOrderedByNumberOfCommonElements() {
		List<CloneMatrix> orderedResult = new ArrayList<CloneMatrix>();
		orderedResult.addAll(resultAsCloneMatrix);
		Comparator<CloneMatrix> comp = new Comparator<CloneMatrix>() {
			@Override
			public int compare(CloneMatrix arg0, CloneMatrix arg1) {
				return arg1.getNumberOfCommonEdges()
						- arg0.getNumberOfCommonEdges();
			}
		};
		Collections.sort(orderedResult, comp);
		return new CloneGroupDetectionResultAsCloneMatrix(orderedResult);
	}

	/**
	 * to ensure an uniform output
	 * 
	 * @param name
	 *            the name of the CloneDetection
	 * @return an output String
	 */
	public String startDetectCloneGroups(String name) {
		Date dateStartDetect = new Date();
		return ("\n" + name + " - start " + dateStartDetect.toString() + "\n");
	}

	/**
	 * for combine algorithm to ensure an uniform output
	 * 
	 * @param nameFirst
	 *            the name of the first CloneDetection
	 * @return an output String
	 */
	public String startConversionTempResult(String nameFirst) {
		Date date = new Date();
		return ("\n" + nameFirst + " end - start Conversion tempResult"
				+ date.toString() + "\n");
	}

	/**
	 * for combine algorithm to ensure an uniform output
	 * 
	 * @param nameSecond
	 *            the name of the second CloneDetection
	 * @return an output String
	 */
	public String moveOnToSecondAlgortihm(String nameSecond) {
		Date date = new Date();
		return ("\n" + "Conversion tempResult done - start " + nameSecond + " "
				+ date.toString() + "\n");
	}

	/**
	 * for combine algorithm CloneDetective with eScans group- and filter-steps
	 * to ensure an uniform output
	 * 
	 * @return an output String
	 */
	public String startGroupingResults() {
		Date date = new Date();
		return ("\n" + "start ModelCd-eScan group and filter steps "
				+ date.toString() + "\n");
	}

	/**
	 * to ensure an uniform output
	 * 
	 * @return an output String
	 */
	public String startConversion() {
		Date dateStartConversion = new Date();
		return ("\n"
				+ "CloneDetection itself done - start Conversion to CloneGroupMapping "
				+ dateStartConversion.toString() + "\n");
	}

	/**
	 * to ensure an uniform output
	 * 
	 * @param name
	 *            the name of the CloneDetection
	 * @param startTime
	 * @return an output String
	 */
	public String endDetectCloneGroups(String name, long startTime) {
		long end = System.currentTimeMillis();
		String res;
		Date dateEndDetect = new Date();
		res = (name + " - end " + dateEndDetect.toString() + "\n");
		res = res + getTime(startTime, end);
		res = res
				+ "------------------------------------------------------------"
				+ "\n";
		return res;
	}

	/**
	 * to ensure an uniform output convert the time difference to minutes and
	 * seconds
	 * 
	 * @param start
	 *            start time
	 * @param end
	 *            end time
	 * @return
	 */
	private String getTime(long start, long end) {
		long sec = (end - start) / 1000;
		if (sec < 60) {
			return ("Running time: " + sec + " sec." + "\n");
		}
		int min = (int) (sec / 60);
		sec = sec % 60;
		String res = ("Running time: " + min + " min. " + sec + " sec." + "\n");

		return res;
	}

}
