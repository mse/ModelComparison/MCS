package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

public class CreateNewLatticeUtility {

	public static int getNumberOfLayersToAdd(
			Map<Integer, Set<Fragment>> resConvert, int maxSizeFragments) {

		Set<Rule> allRules = new HashSet<Rule>();
		for (Set<Fragment> fragments : resConvert.values()) {
			for (Fragment fragment : fragments) {
				allRules.add(fragment.getRule());
			}
		}

		int numberOfLayersToAdd = 0;

		Set<Rule> foundRules = new HashSet<Rule>();
		int position = maxSizeFragments;
		while (position >= 0) {
			if (resConvert.containsKey(position)) {
				for (Fragment fragment : resConvert.get(position)) {
					foundRules.add(fragment.getRule());
				}
			}
			numberOfLayersToAdd++;
			if (foundRules.containsAll(allRules)) {
				return numberOfLayersToAdd;
			} else {
				position--;
			}

		}
		return -1;
	}

	/**
	 * Orders the Fragments accordingly to the order of ruleList.
	 * 
	 * @param fragments
	 *            there should be exactly one fragment per rule of ruleList
	 * @param ruleList
	 *            the FragmentSets should be ordered accordingly to the position
	 *            of the rule in ruleList, to which the Fragments belong
	 * @return
	 */
	private static List<Fragment> orderFragmentSet(Set<Fragment> fragments,
			List<Rule> ruleList) {
		List<Fragment> res = new LinkedList<Fragment>();
		Map<Rule, Fragment> map = new HashMap<Rule, Fragment>();
		for (Fragment fragment : fragments) {
			map.put(fragment.getRule(), fragment);
		}
		for (Rule rule : ruleList) {
			res.add(map.get(rule));
		}

		return res;
	}

	public static List<Set<CapsuleEdge>> getFragmentsEdgeSetListWithAttributes(
			Set<Fragment> maxFragments, List<Rule> ruleList) {
		List<Set<CapsuleEdge>> fragmentsEdgeSetList = new LinkedList<Set<CapsuleEdge>>();
		List<Fragment> fragmentList = orderFragmentSet(maxFragments, ruleList);
		for (Fragment fragment : fragmentList) {
			Set<CapsuleEdge> set = new HashSet<CapsuleEdge>();
			set.addAll(fragment.getCapsuleEdges());
			fragmentsEdgeSetList.add(set);
		}
		return fragmentsEdgeSetList;
	}

	/**
	 * one Fragment per rule
	 * 
	 * @param resConvert
	 * @return
	 */
	public static Set<Fragment> getMaxFragments(
			Map<Integer, Set<Fragment>> resConvert) {
		int maxSizeFragments = getMaxSizeFragments(resConvert);

		Set<Rule> allRules = new HashSet<Rule>();
		for (Set<Fragment> fragments : resConvert.values()) {
			for (Fragment fragment : fragments) {
				allRules.add(fragment.getRule());
			}
		}

		Set<Fragment> fragments = new HashSet<Fragment>();
		Set<Rule> foundRules = new HashSet<Rule>();
		int position = maxSizeFragments;
		while (position >= 0) {
			if (resConvert.containsKey(position)) {
				for (Fragment fragment : resConvert.get(position)) {
					if (!foundRules.contains(fragment.getRule())) {
						foundRules.add(fragment.getRule());
						fragments.add(fragment);
					}
				}
			}
			if (foundRules.containsAll(allRules)) {
				break;
			} else {
				position--;
			}

		}

		if (foundRules.size() == allRules.size()) {
			return fragments;
		} else {
			return null;
		}
	}

	private static int getMaxSizeFragments2(
			Map<Integer, Set<List<Fragment>>> resConvert) {
		Set<Integer> integerSet = resConvert.keySet();
		List<Integer> integerList = new LinkedList<Integer>();
		integerList.addAll(integerSet);
		Collections.sort(integerList);
		int maxSizeFragments = integerList.get(integerList.size() - 1)
				.intValue();
		return maxSizeFragments;
	}

	public static int getMaxSizeFragments(Map<Integer, Set<Fragment>> resConvert) {
		Set<Integer> integerSet = resConvert.keySet();
		List<Integer> integerList = new LinkedList<Integer>();
		integerList.addAll(integerSet);
		Collections.sort(integerList);
		int maxSizeFragments = integerList.get(integerList.size() - 1)
				.intValue();
		return maxSizeFragments;
	}

	public static Map<Fragment, List<Set<Fragment>>> createTopLayerLattice3D(
			Map<Integer, Set<List<Fragment>>> resConvert) {
		Map<Fragment, List<Set<Fragment>>> topLayer = new HashMap<Fragment, List<Set<Fragment>>>();
		int maxSizeFragments = getMaxSizeFragments2(resConvert);
		Set<List<Fragment>> maxFragments = resConvert.get(maxSizeFragments);

		for (List<Fragment> formerCloneTupelAsFragmentList : maxFragments) {
			Fragment key = formerCloneTupelAsFragmentList.get(0);
			if (topLayer.containsKey(key)) {
				for (int i = 1; i < formerCloneTupelAsFragmentList.size(); i++) {
					(topLayer.get(key)).get(i - 1).add(
							formerCloneTupelAsFragmentList.get(i));
				}
			} else {
				List<Set<Fragment>> value = new LinkedList<Set<Fragment>>();
				for (int i = 1; i < formerCloneTupelAsFragmentList.size(); i++) {
					Set<Fragment> set = new HashSet<Fragment>();
					set.add(formerCloneTupelAsFragmentList.get(i));
					value.add(set);
				}
				topLayer.put(key, value);
			}

		}
		return topLayer;
	}

	public static List<Set<Fragment>> createLattice(Set<Fragment> topLayer) {
		int sizeFragments = topLayer.iterator().next().size();
		List<Set<Fragment>> lattice = new LinkedList<Set<Fragment>>();

		for (int i = 0; i < sizeFragments - 1; i++) {
			Set<Fragment> set = new HashSet<Fragment>();
			lattice.add(set);
		}

		lattice.add(topLayer);
		return lattice;
	}

	public static List<Set<Fragment>> createLattice(
			Map<Integer, Set<Fragment>> resConvert, int maxSizeFragments,
			int numberOfLayersToAdd) {
		List<Set<Fragment>> lattice = new LinkedList<Set<Fragment>>();

		if (maxSizeFragments < numberOfLayersToAdd) {
			numberOfLayersToAdd = maxSizeFragments;
		}
		for (int i = 0; i <= maxSizeFragments; i++) {
			Set<Fragment> set = new HashSet<Fragment>();
			if (i == maxSizeFragments - numberOfLayersToAdd) {
				if (resConvert.containsKey(maxSizeFragments
						- numberOfLayersToAdd)) {
					set.addAll(resConvert.get(maxSizeFragments
							- numberOfLayersToAdd));
				}
				numberOfLayersToAdd--;
			}
			lattice.add(set);
		}
		return lattice;
	}

}
