package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.CloneDetective;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.ClonePair;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

import org.eclipse.emf.henshin.model.Rule;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 
 * Contains the CloneDetection-Algorithm CloneDetective with ModelCd-eScans
 * group- and filter-steps
 *
 */
public class CloneDetectiveWithEScanGroupAndFilterAdded extends
		CombineDetection {

	public CloneDetectiveWithEScanGroupAndFilterAdded(Collection<Rule> rules) {
		super(rules);
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		//System.out
		//		.println(startDetectCloneGroups("CloneDetectiveWithEScanGroupAndFilterAdded"));
		CloneDetective cloneDetective = new CloneDetective(ruleGraphMap,
				ruleList, considerAttributeNodes);
		Collection<ClonePair> resCloneDetective = cloneDetective
				.runCloneDetective();

		//System.out.println(startGroupingResults());
		Map<Integer, Set<Fragment>> resConvert = ConvertToFragments
				.convertClonePairsToFragments(resCloneDetective, ruleGraphMap);
		Set<Set<Fragment>> tempRes = getGroupedAndFilteredCloneGroups(resConvert);

		//System.out.println(startConversion());
		resultAsCloneMatrix = CloneMatrixCreator.convertEScanResult(tempRes);

		//System.out.println(endDetectCloneGroups(
		//		"CloneDetectiveWithEScanGroupAndFilterAdded", startZeit));
	}

}
