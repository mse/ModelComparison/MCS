package de.uni_marburg.fb12.swt.cloneDetection.combineDetection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetective.ClonePair;
//import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveQuartet.CloneQuartet;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetectiveTupel.CloneTupel;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

public class ConvertToFragments {

	public static Map<Integer, Set<Fragment>> convertClonePairsToFragments(
			Collection<ClonePair> clonePairs,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		// sort ClonePairs by size
		Map<Integer, Set<Fragment>> res = new HashMap<Integer, Set<Fragment>>();
		for (ClonePair clonePair : clonePairs) {
			Set<Fragment> fragments = clonePair.getAsFragments(ruleGraphMap);
			if (fragments != null) {
				int size = fragments.iterator().next().getCapsuleEdges().size();
				if (res.containsKey(size)) {
					res.get(size).addAll(fragments);
				} else {
					res.put(size, fragments);
				}
			}
		}

		return res;
	}

	public static Map<Integer, Set<List<Fragment>>> convertCloneTupelsToFragmentLists(
			Collection<CloneTupel> cloneTupels,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		// sort CloneTupels by size
		Map<Integer, Set<List<Fragment>>> res = new HashMap<Integer, Set<List<Fragment>>>();
		for (CloneTupel cloneTupel : cloneTupels) {
			List<Fragment> fragments = cloneTupel
					.getAsFragmentList(ruleGraphMap);
			if (fragments != null) {
				int size = fragments.get(0).getCapsuleEdges().size();
				if (res.containsKey(size)) {
					res.get(size).add(fragments);
				} else {
					Set<List<Fragment>> set = new HashSet<List<Fragment>>();
					set.add(fragments);
					res.put(size, set);
				}
			}
		}

		return res;
	}

	public static Set<Set<Fragment>> convertCloneGroupsToFragments(
			CloneGroupDetectionResult inputResult,
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
		Set<Set<Fragment>>  result = new HashSet<Set<Fragment>>();
		for (CloneGroup entry : inputResult.getCloneGroups()) {
			HashMap<Rule,Fragment> rule2fragment = new HashMap<Rule, Fragment>();
			for (Rule r : entry.getRules()) {
				Fragment f = new Fragment(new HashSet<CapsuleEdge>(), r, ruleGraphMap.get(r));
				rule2fragment.put(r,f);
			}

			for (Edge e : entry.getEdgeMappings().keySet()) {
				Fragment f = rule2fragment.get(e.getGraph().getRule());
				DirectedGraph<Node, CapsuleEdge> x = f.getGraph();
				Node n1 = e.getSource().getActionNode();
				Node n2 = e.getTarget().getActionNode();
				CapsuleEdge c = null;
				for (CapsuleEdge ex : x.getAllEdges(n1, n2)) {
					if (ex.getOriginalEdge() == e) {
						c = ex;
					} break;
				}
				if (c != null) 
					f.getCapsuleEdges().add(c);
				else
					System.err.println("Found edge with no corresponding CapsuleEdge!");
			}
			
			for (Attribute a : entry.getAttributeMappings().keySet()) {
				Fragment f = rule2fragment.get(a.getNode().getGraph().getRule());
				DirectedGraph<Node, CapsuleEdge> x = f.getGraph();
				boolean found = false;
				for (CapsuleEdge e : x.outgoingEdgesOf(a.getNode().getActionNode())) {
					if (e.getAttribute() == a) {
						f.getCapsuleEdges().add(e);
						found = true;
						break;
					} 
				}
				if (!found)
					System.err.println("Found attribute edge with no corresponding CapsuleEdge!");
				
			}
			
			

			Set<Fragment> fragments = new HashSet<Fragment>();
			for (Rule r : rule2fragment.keySet()) {
				fragments.add(rule2fragment.get(r));
			}
			result.add(fragments);
			
		}
		
		return result;
	}

	

//	public Set<Fragment> getAsFragments(
//			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap) {
//		if (capsuleEdges1.size() == 0) {
//			return null;
//		}
//		Set<Fragment> res = new HashSet<Fragment>();
//
//		Rule rule1 = capsuleEdges1.get(0).getRule();
//		Rule rule2 = capsuleEdges2.get(0).getRule();
//		Set<CapsuleEdge> capsuleEdgeSet1 = new HashSet<CapsuleEdge>();
//		Set<CapsuleEdge> capsuleEdgeSet2 = new HashSet<CapsuleEdge>();
//		capsuleEdgeSet1.addAll(capsuleEdges1);
//		capsuleEdgeSet2.addAll(capsuleEdges2);
//		DirectedGraph<Node, CapsuleEdge> graph1neu = ruleGraphMap.get(rule1);
//		DirectedGraph<Node, CapsuleEdge> graph2neu = ruleGraphMap.get(rule2);
//		Fragment f1 = new Fragment(capsuleEdgeSet1, rule1, graph1neu);
//		Fragment f2 = new Fragment(capsuleEdgeSet2, rule2, graph2neu);
//		res.add(f1);
//		res.add(f2);
//		return res;
//	}


}
