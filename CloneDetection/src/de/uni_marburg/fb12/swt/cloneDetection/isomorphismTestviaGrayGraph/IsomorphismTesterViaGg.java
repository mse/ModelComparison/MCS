package de.uni_marburg.fb12.swt.cloneDetection.isomorphismTestviaGrayGraph;

import java.util.Date;
import java.util.Set;

//import org.eclipse.emf.henshin.interpreter.EGraph;
//import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
//import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.jgrapht.DirectedGraph;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCd.Fragment;

/**
 * 
 * A GrayGraph is like an integrated Graph Version without actions
 *
 */
public class IsomorphismTesterViaGg {

	public static boolean testResult(Set<CloneMatrix> result) {
		boolean res = true;
		for (CloneMatrix cloneMatrix : result) {
			if (!testCloneMatrix(cloneMatrix)) {
				res = false;
			}
		}

		return res;
	}

	public static boolean testCloneMatrix(CloneMatrix cloneMatrix) {
		long start = System.currentTimeMillis();
		Date dateStart = new Date();
		//System.out.println(getStartString("isomorphism-test", dateStart));

		Set<Graph> graphs = CloneMatrixToGrayGraphConverter
				.getCloneGraphs(cloneMatrix);

		boolean allAreIsomorph = true;

		Graph firstGraph = graphs.iterator().next();
		for (Graph graph : graphs) {
			if (!areIsomorph(firstGraph, graph)) {
				allAreIsomorph = false;
				System.err.println(" **** Non-isomorphy detected!");
			}
		}

		//System.out.println(getEndString(allAreIsomorph, start));
		return allAreIsomorph;
	}

	private static boolean areIsomorph(Graph graph1, Graph graph2) {
		DirectedGraph<Node, CapsuleEdge> dGraph1 = GrayGraphToDirectedGraph
				.getIntegratedGraph(graph1);
		DirectedGraph<Node, CapsuleEdge> dGraph2 = GrayGraphToDirectedGraph
				.getIntegratedGraph(graph2);

		Fragment fragment1 = new Fragment(dGraph1.edgeSet(), null, dGraph1);
		Fragment fragment2 = new Fragment(dGraph2.edgeSet(), null, dGraph2);

		String canonicalLabel1 = fragment1.getLabel();
		String canonicalLabel2 = fragment2.getLabel();
		return (canonicalLabel1.equals(canonicalLabel2));
	}

	public static boolean testCloneMatrix(CloneMatrix cloneMatrix, Rule rule) {

		Graph comparisonGraph = RuleToGrayGraphConverter
				.getIntegeratedHenshinGraph(rule);
		long start = System.currentTimeMillis();
		Date dateStart = new Date();
		System.out.println(getStartString("isomorphism-test", dateStart));

		Set<Graph> graphs = CloneMatrixToGrayGraphConverter
				.getCloneGraphs(cloneMatrix);

		boolean allAreIsomorph = true;

		for (Graph graph : graphs) {
			if (!areIsomorph(comparisonGraph, graph)) {
				allAreIsomorph = false;
			}

		}

		System.out.println(getEndString(allAreIsomorph, start));
		return allAreIsomorph;
	}

	private static String getStartString(String s, Date dateStart) {
		return ("start " + s + ": " + dateStart.toString() + "\n");
	}

	private static String getEndString(boolean allAreIsomorph, long start) {
		Date dateDone = new Date();
		long end = System.currentTimeMillis();
		String testRes = "";

		if (allAreIsomorph) {
			testRes = testRes + "all Clones are isomorph" + "\n";
		} else {
			testRes = testRes + "fail \n";
		}
		long sec = (end - start) / 1000;
		testRes = testRes + "IsomorphismTest - done, in " + sec + " sec "
				+ dateDone.toString() + "\n";
		return testRes;
	}

}
