package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import org.jgrapht.DirectedGraph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class EScanDetectionArticleIncOpt2 extends EScanDetectionArticleIncOpt1 {

	private final boolean optimized = true;

	public EScanDetectionArticleIncOpt2(Collection<Rule> rules) {
		super(rules);
	}

	public EScanDetectionArticleIncOpt2(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
	}

	@Override
	public void detectCloneGroups() {
		int itemizedUpTo = getMaxSizeOfClones() + 1; // +1 just to be on the
														// safe side
		detectCloneGroups(itemizedUpTo);
	}

	/**
	 * 
	 * @param itemizedUpTo
	 *            the Layer up to with lattice is build up, step by step,
	 *            respectively layer by layer
	 * 
	 */
	public void detectCloneGroups(int itemizedUpTo) {
		List<Set<Fragment>> lattice = new LinkedList<Set<Fragment>>();
		detectCloneGroups(itemizedUpTo, lattice);

	}

	/**
	 * 
	 * @param itemizedUpTo
	 *            the Layer up to with lattice is build up, step by step,
	 *            respectively layer by layer
	 * @param lattice
	 *            the data structure eScan works with
	 */
	public void detectCloneGroups(int itemizedUpTo, List<Set<Fragment>> lattice) {
		long startZeit = System.currentTimeMillis();
		final int MAX_LAYER = -1;
		boolean latticeComplete;

		System.out
				.println(startDetectCloneGroups("EScanDetectionArticleInc2StepsOpt - Step 1"));
		latticeComplete = false;
		Set<Fragment> all1Fragments = getL1FragmentWithoutAttributes(); // getL1Fragment();
		for (int i = START_LAYER; i < itemizedUpTo; i++) {
			eScanBuildLatticeAndPrintToConsole(lattice, all1Fragments, i,
					(i + 1));

			if (lattice.size() < (i)) {
				latticeComplete = true;
				break;
			}
		}

		Set<Set<Fragment>> cloneGroups;
		if ((latticeComplete) || (itemizedUpTo == lattice.size())) {

			cloneGroups = eScanGroupAndFilterLattice(lattice);
		} else {
			if (itemizedUpTo < 0) {
				itemizedUpTo = START_LAYER;
			}
			eScanBuildLatticeAndPrintToConsole(lattice, all1Fragments,
					itemizedUpTo, MAX_LAYER);
			cloneGroups = eScanGroupAndFilterLattice(lattice);
		}

		detectCloneGroupStep2(cloneGroups);
		//System.out.println(endDetectCloneGroups(
		//		"EScanDetectionArticleInc2StepsOpt", startZeit));
	}

	public void detectCloneGroupStep2(Set<Set<Fragment>> cloneGroups) {

		System.out
				.println(startDetectCloneGroups("EScanDetectionArticleInc2StepsOpt - Step 2"));
		boolean ignoreIsGeneratingParent = true;
		boolean latticeComplete = false;
		Set<CapsuleEdge> attributeEdges = getAttributeEdges();

		List<Set<Fragment>> newLattice = rebuildLattice(cloneGroups);

		int i = START_LAYER;
		while (true) {
			eScanBuildLatticeAndPrintToConsoleStep2(newLattice, attributeEdges,
					i, (i + 1), ignoreIsGeneratingParent, optimized);

			if (newLattice.size() < (i + 1)) {
				latticeComplete = true;
				break;
			}
			i++;
		}

		if ((latticeComplete)) {
			cloneGroups = eScanGroupAndFilterLattice(newLattice);
		} else {
			System.out
					.println("EScanDetectionArticlaIncOpt2.- detectCloneGroupStep2 -  ????");
		}

		//System.out.println(startConversion());

		resultAsCloneMatrix = CloneMatrixCreator
				.convertEScanResult(cloneGroups);
	}

	private List<Set<Fragment>> rebuildLattice(Set<Set<Fragment>> cloneGroups) {
		Map<Integer, Set<Fragment>> orderedCloneGroupBySizeOfFragments 
				= new HashMap<Integer, Set<Fragment>>();
		int maxSize = 0;
		for (Set<Fragment> fragmentSet : cloneGroups) {
			int size = fragmentSet.iterator().next().size();
			if (maxSize < size) {
				maxSize = size;
			}
			if (orderedCloneGroupBySizeOfFragments.containsKey(size)) {
				orderedCloneGroupBySizeOfFragments.get(size)
						.addAll(fragmentSet);
			} else {
				orderedCloneGroupBySizeOfFragments.put(size, fragmentSet);
			}
		}

		List<Set<Fragment>> newLattice = new LinkedList<Set<Fragment>>();
		for (int i = 0; i < maxSize + 1; i++) {
			int k = i + 1;
			if (orderedCloneGroupBySizeOfFragments.containsKey(k)) {
				newLattice.add(orderedCloneGroupBySizeOfFragments.get(k));
			} else {
				newLattice.add(new HashSet<Fragment>());
			}

		}

		return newLattice;
	}

}
