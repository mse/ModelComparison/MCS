package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;
import de.uni_marburg.fb12.swt.cloneDetection.modelCdCanonicalLabel.CanonicalLabelForFragmentCreator;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/**
 * fragment from the ModelCd-eScan article
 *
 */
public class Fragment {
	private final List<CapsuleEdge> orderedCapsuleEdges;
	private final Rule rule;
	private final DirectedGraph<Node, CapsuleEdge> graph;
	private String label;

	private CapsuleEdge lastNotDisconnectingCapsuleEdge;

	@Override
	public int hashCode() {
		int res = rule.getName().hashCode();
		res = res + label.hashCode();
		return res;
	}

	@Override
	public boolean equals(Object o) {
		Fragment fragment = (Fragment) o;

		if (this.rule != fragment.rule) {
			return false;
		}
		if (!fragment.orderedCapsuleEdges.equals(orderedCapsuleEdges)) {
			return false;
		}

		return true;
	}

	/**
	 * Checks if this Fragment and the given one are isomorphic to each other,
	 * respectively if the canonical labels are equal
	 * 
	 * @param f
	 *            the Fragment this Fragment should be compared to
	 * @return n <code>true</code> if the Fragments are isomorphic to each other
	 *         <code>false</code> else
	 */
	public boolean isIsomorph(Fragment f) {
		return label.equals(f.getLabel());
	}

	@Override
	public String toString() {
		String res = rule.getName() + "\n Label: " + label;
		return res;
	}

	/**
	 * 
	 * @return the source- and target-Nodes of the Edges of this Fragment
	 */
	public Set<Node> getNodes() {
		Set<Node> nodes = new HashSet<Node>();
		for (CapsuleEdge capsuleEdge : orderedCapsuleEdges) {
			nodes.add(graph.getEdgeSource(capsuleEdge));
			nodes.add(graph.getEdgeTarget(capsuleEdge));
		}
		return nodes;
	}

	/**
	 * 
	 * @return the source- and target-Nodes of the Edges of this Fragment, that
	 *         are not AttributeNodes
	 */
	public Set<Node> getOriginalNodes() {
		Set<Node> nodes = new HashSet<Node>();
		for (CapsuleEdge capsuleEdge : orderedCapsuleEdges) {
			if (!capsuleEdge.isAttributeEdge()) {
				nodes.add(graph.getEdgeSource(capsuleEdge));
				nodes.add(graph.getEdgeTarget(capsuleEdge));
			} else {
				nodes.add(graph.getEdgeSource(capsuleEdge));
			}

		}
		return nodes;
	}

	/**
	 * 
	 * @return all the CapsuleEdges this Fragment exist of
	 */
	public List<CapsuleEdge> getCapsuleEdges() {
		return orderedCapsuleEdges;
	}

	/**
	 * The size of a Fragment is the number of the edges of this Fragment.
	 * 
	 * @return the size of this Fragment
	 * 
	 */
	public int size() {
		return orderedCapsuleEdges.size();
	}

	/**
	 * The edges of the computation graph are CapsuleEdges, CapsuleEdges capsule
	 * either an original edge or an attribute.
	 * 
	 * @return all original edges (that is Henhsin-edges) of this Fragment
	 */
	public Set<Edge> getOriginalEdges() {
		Set<Edge> res = new HashSet<Edge>();
		for (CapsuleEdge e : orderedCapsuleEdges) {
			if (!e.isAttributeEdge()) {
				res.add(e.getOriginalEdge());
			}
		}
		return res;
	}

	/**
	 * The edges of the computation graph are CapsuleEdges, CapsuleEdges capsule
	 * either an original edge or an attribute.
	 * 
	 * @return all original attributes (that is Henhsin-attributes) of this
	 *         Fragment
	 */
	public Set<Attribute> getAttributes() {
		Set<Attribute> res = new HashSet<Attribute>();
		for (CapsuleEdge e : orderedCapsuleEdges) {
			if (e.isAttributeEdge()) {
				res.add(e.getAttribute());
			}
		}
		return res;
	}

	/**
	 * 
	 * @return the rule this Fragment belongs to
	 */
	public Rule getRule() {
		return rule;
	}

	/**
	 * 
	 * @return the whole computation graph this Fragment belongs to
	 */
	public DirectedGraph<Node, CapsuleEdge> getGraph() {
		return graph;
	}

	public Fragment(Set<CapsuleEdge> capsuleEdges, Rule rule,
			DirectedGraph<Node, CapsuleEdge> graph) {
		this.rule = rule;
		this.graph = graph;
		DirectedGraph<Node, CapsuleEdge> fragmentGraph = getFragmentAsGraph(
				capsuleEdges, graph);
		Map<String, List<CapsuleEdge>> labelToOrderedCapsuleEdges = CanonicalLabelForFragmentCreator
				.getCanonicalLabel(fragmentGraph);
		this.label = labelToOrderedCapsuleEdges.keySet().iterator().next();
		this.orderedCapsuleEdges = labelToOrderedCapsuleEdges.get(label);

	}

	/**
	 * 
	 * @param biggerFragment
	 *            the Fragment that maybe contains this Fragment.
	 * @return <code>true</code> if this Fragments is a part of the
	 *         biggerFragment <code>false</code> else
	 */
	public boolean isSubFragment(Fragment biggerFragment) {
		if (biggerFragment.orderedCapsuleEdges.size() < orderedCapsuleEdges
				.size()) {
			//System.out.println("Fragment.java: biggerFragment.size(): "
			//		+ biggerFragment.orderedCapsuleEdges.size()
			//		+ " this.size() " + orderedCapsuleEdges.size());
		}
		for (CapsuleEdge e : orderedCapsuleEdges) {
			if (!biggerFragment.orderedCapsuleEdges.contains(e)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Two Fragments are NodeOverlapping if they contains a least one identical
	 * Node.
	 * 
	 * @param f
	 *            the other Node
	 * @return <code>true</code> if the Fragments have at least one identical
	 *         Node <code>false</code> else
	 */
	public boolean isNodeOverlapping(Fragment f) {
		if (this.rule != f.getRule()) {
			return false;
		}

		Set<Node> nodesThis = new HashSet<Node>();
		Set<Node> nodesF = new HashSet<Node>();
		DirectedGraph<Node, CapsuleEdge> graphF = f.getGraph();

		for (CapsuleEdge e : orderedCapsuleEdges) {
			nodesThis.add(graph.getEdgeSource(e));
			nodesThis.add(graph.getEdgeTarget(e));
		}

		for (CapsuleEdge e : f.orderedCapsuleEdges) {
			nodesF.add(graphF.getEdgeSource(e));
			nodesF.add(graphF.getEdgeTarget(e));
		}

		for (Node n : nodesThis) {
			if (nodesF.contains(n)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param ckp1
	 * @return
	 */
	public boolean isGeneratingParent(Fragment ckp1) {
		if (ckp1.getRule() != rule) {
			return false;
		}
		// determine the edge the fragments differ from each other
		CapsuleEdge additionalEdge = null;
		boolean foundAdditionalEdge = false;
		for (CapsuleEdge e : ckp1.orderedCapsuleEdges) {
			if (!orderedCapsuleEdges.contains(e)) {
				if (foundAdditionalEdge) {
					return false;
				}
				additionalEdge = e;
				foundAdditionalEdge = true;
			}
		}

		if (additionalEdge == ckp1.lastNotDisconnectingCapsuleEdge()) {
			return true;
		}
		return false;
	}

	private CapsuleEdge lastNotDisconnectingCapsuleEdge() {
		if (lastNotDisconnectingCapsuleEdge != null) {
			return lastNotDisconnectingCapsuleEdge;
		}

		for (CapsuleEdge testEdge : orderedCapsuleEdges) {
			if (!(isTheOnlyConnectingCapsuleEdge(testEdge))) {
				lastNotDisconnectingCapsuleEdge = testEdge;
				return lastNotDisconnectingCapsuleEdge;
			}
		}

		return null;
	}

	/**
	 * if e is the only connecting edge, the remaining fragments is not
	 * completely connected
	 * 
	 * @param e
	 * @return
	 */
	private boolean isTheOnlyConnectingCapsuleEdge(CapsuleEdge e) {
		Set<CapsuleEdge> capsuleEdgesWithoutE = new HashSet<CapsuleEdge>();
		for (CapsuleEdge edge : orderedCapsuleEdges) {
			if (edge != e) {
				capsuleEdgesWithoutE.add(edge);
			}
		}
		return !(isConnected(capsuleEdgesWithoutE, graph));
	}

	/**
	 * 
	 * starts with an arbitrary CapsuleEdge, put this CapsuleEdge to the
	 * successfulTestedCapsuleEdges and its start and destination node to nodes
	 * 
	 * searches in all remaining CapsuleEdge for a CapsuleEdge which start or
	 * destination Nodes are already in nodes found no such edge -->
	 * notConnected --> retrun false found one -- > start search again
	 * 
	 * no remaining CapsuleEdges --> isConnected --> retrun true
	 * 
	 * @param testcapsuleEdgeset
	 * @param graph
	 * @return
	 */

	public static boolean isConnected(Set<CapsuleEdge> testCapsuleEdgeSet,
			DirectedGraph<Node, CapsuleEdge> graph) {
		if (testCapsuleEdgeSet.size() == 1) {
			return true;
		}

		Set<Node> nodes = new HashSet<Node>();
		Set<CapsuleEdge> successfulTestedCapsuleEdges = new HashSet<CapsuleEdge>();

		Iterator<CapsuleEdge> testcapsuleEdgesetIterator = testCapsuleEdgeSet
				.iterator();
		if (testcapsuleEdgesetIterator.hasNext()) {
			CapsuleEdge e = testcapsuleEdgesetIterator.next();
			successfulTestedCapsuleEdges.add(e);
			nodes.add(graph.getEdgeSource(e));
			nodes.add(graph.getEdgeTarget(e));
		}

		boolean foundSuccessfulTestedCapsuleEdges = false;

		while (testCapsuleEdgeSet.size() != successfulTestedCapsuleEdges.size()) {
			foundSuccessfulTestedCapsuleEdges = false;

			for (CapsuleEdge e : testCapsuleEdgeSet) {
				if (!successfulTestedCapsuleEdges.contains(e)) {
					Node source = graph.getEdgeSource(e);
					Node target = graph.getEdgeTarget(e);
					if (nodes.contains(source) || nodes.contains(target)) {
						nodes.add(source);
						nodes.add(target);
						successfulTestedCapsuleEdges.add(e);
						foundSuccessfulTestedCapsuleEdges = true;
					}
				}
			}
			if (!foundSuccessfulTestedCapsuleEdges) {
				return false;
			}
		}
		return true;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * 
	 * @param onlyThisCapsuleEdges
	 *            the CapsuelEdges this fragment should be extended with (a
	 *            Fragment could only be extended with a CapsuleEdge, to which
	 *            its corresponding graph is connected to)
	 * @return the set of all fragments, which are created when this fragments
	 *         is extended with one of the CapsuleEdges from
	 *         onlyThisCapsuleEdges
	 */
	public Set<Fragment> extensOp(Set<CapsuleEdge> onlyThisCapsuleEdges) {
		Set<Fragment> res = new HashSet<Fragment>();

		for (CapsuleEdge capsuleEdge : onlyThisCapsuleEdges) {
			if (!orderedCapsuleEdges.contains(capsuleEdge)) {
				if ((graph.containsEdge(capsuleEdge))
						&& (isConnectedTo(capsuleEdge, orderedCapsuleEdges,
								graph))) {
					Set<CapsuleEdge> tempCapsuleEdges = new HashSet<CapsuleEdge>(); 
					tempCapsuleEdges.addAll(orderedCapsuleEdges);
					tempCapsuleEdges.add(capsuleEdge);
					Fragment temp = new Fragment(tempCapsuleEdges, rule, graph);
					res.add(temp);

				}
			}
		}
		return res;
	}

	/**
	 * 
	 * @param capsuleEdge
	 * @return a fragment that is created, when this fragment would be extended
	 *         with capsuleEdge
	 */
	public Fragment extensOp(CapsuleEdge capsuleEdge) {

		if (!orderedCapsuleEdges.contains(capsuleEdge)) {
			if ((graph.containsEdge(capsuleEdge))
					&& (isConnectedTo(capsuleEdge, orderedCapsuleEdges, graph))) {
				Set<CapsuleEdge> tempCapsuleEdges = new HashSet<CapsuleEdge>();
				tempCapsuleEdges.addAll(orderedCapsuleEdges);
				tempCapsuleEdges.add(capsuleEdge);

				Fragment res = new Fragment(tempCapsuleEdges, rule, graph);
				return res;

			}

		}
		return null;
	}

	private boolean isConnectedTo(CapsuleEdge e,
			List<CapsuleEdge> listCapsuleEdges,
			DirectedGraph<Node, CapsuleEdge> graph) {
		if (!e.isAttributeEdge()) {
			if (e.getOriginalEdge().getActionEdge().getGraph().getRule() != rule) {
				return false;
			}
		} else {
			if (e.getAttribute().getActionAttribute().getGraph().getRule() != rule) {
				return false;
			}
		}

		Set<Node> nodes = new HashSet<Node>();
		for (CapsuleEdge edge : listCapsuleEdges) {
			nodes.add(graph.getEdgeSource(edge));
			nodes.add(graph.getEdgeTarget(edge));
		}
		if (nodes.contains(graph.getEdgeSource(e))) {
			return true;
		}
		if (nodes.contains(graph.getEdgeTarget(e))) {
			return true;
		}
		return false;
	}

	private DirectedGraph<Node, CapsuleEdge> getFragmentAsGraph(
			Set<CapsuleEdge> capsuleEdges,
			DirectedGraph<Node, CapsuleEdge> graph) {
		DirectedGraph<Node, CapsuleEdge> fragmentGraph 
				= new DefaultDirectedGraph<Node, CapsuleEdge>(
				CapsuleEdge.class);

		for (CapsuleEdge capsuleEdge : capsuleEdges) {
			Node source = graph.getEdgeSource(capsuleEdge);
			Node target = graph.getEdgeTarget(capsuleEdge);
			fragmentGraph.addVertex(source);
			fragmentGraph.addVertex(target);
			fragmentGraph.addEdge(source, target, capsuleEdge);
		}

		return fragmentGraph;
	}

}
