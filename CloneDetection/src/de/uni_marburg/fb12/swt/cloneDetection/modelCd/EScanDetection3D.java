package de.uni_marburg.fb12.swt.cloneDetection.modelCd;

import org.jgrapht.DirectedGraph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
//import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrixCreator;
import de.uni_marburg.fb12.swt.cloneDetection.henshinToIntegrated.CapsuleEdge;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EScanDetection3D extends EScanDetection3DAbstract {
	public EScanDetection3D(Collection<Rule> rules) {
		super(rules);
	}

	public EScanDetection3D(
			Map<Rule, DirectedGraph<Node, CapsuleEdge>> ruleGraphMap,
			List<Rule> ruleList, boolean considerAttributeNodes) {
		super(ruleGraphMap, ruleList, considerAttributeNodes);
	}

	@Override
	public void detectCloneGroups() {
		long startZeit = System.currentTimeMillis();
		//System.out
		//		.println(startDetectCloneGroups("EScanDetection3DIn1Step (CloneDetection"
		//				+ " directly with Attributes)"));
		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(ruleList);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		if (edgesLayer1.size() == 0) {
			resultAsCloneMatrix = null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			//System.out.println(startConversion());
			resultAsCloneMatrix = CloneMatrixCreator
					.convertEScanResult(cloneGroups);
			//System.out.println(endDetectCloneGroups("EScanDetection3DIn1Step",
			//		startZeit));
		}

	}

	@Override
	public void detectCloneGroups(Map<Fragment, List<Set<Fragment>>> topLayer) {

		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(ruleList);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
				topLayer, edgesLayer1);

		Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
		//System.out.println(startConversion());
		resultAsCloneMatrix = CloneMatrixCreator
				.convertEScanResult(cloneGroups);

	}

	public Set<Set<Fragment>> getCloneGroups() {
		List<Set<CapsuleEdge>> graphsEdgeSetList = getGraphsEdgeSetListWithAttributes(ruleList);
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsEdgeSetList);

		if (edgesLayer1.size() == 0) {
			return null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			return cloneGroups;
		}
	}

	public Set<Set<Fragment>> getCloneGroups(
			List<Set<CapsuleEdge>> graphsFragmentsSetList) {
		Map<CapsuleEdge, List<Set<CapsuleEdge>>> edgesLayer1 
				= getCapsuleEdgeMappingLayer1(graphsFragmentsSetList);

		if (edgesLayer1.size() == 0) {
			return null;
		} else {
			Map<Fragment, List<Set<Fragment>>> layer1 = buildLayer1(edgesLayer1);
			List<Map<Fragment, List<Set<Fragment>>>> lattice3D = buildLattice3D(
					layer1, edgesLayer1);

			Set<Set<Fragment>> cloneGroups = groupAndFilterLattice3D(lattice3D);
			return cloneGroups;
		}
	}

	private List<Set<CapsuleEdge>> getGraphsEdgeSetListWithAttributes(
			List<Rule> ruleList) {
		List<Set<CapsuleEdge>> graphsEdgeSetList = new LinkedList<Set<CapsuleEdge>>();
		for (Rule rule : ruleList) {
			graphsEdgeSetList.add(ruleGraphMap.get(rule).edgeSet());
		}
		return graphsEdgeSetList;
	}

}
