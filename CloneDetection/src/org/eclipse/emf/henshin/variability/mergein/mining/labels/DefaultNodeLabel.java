package org.eclipse.emf.henshin.variability.mergein.mining.labels;

import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Action.Type;
import org.eclipse.emf.henshin.variability.mergein.normalize.HenshinNode;

/**
 * This class represents the label of the graph nodes under mining. It
 * stores the type of a given node, and supports hashCode() and equals()
 * methods for the comparison of different instances. 
 * 
 * @author str�ber
 *
 */ 
public class DefaultNodeLabel implements INodeLabel {

	protected EObject type;
	protected Action action;
	
	public DefaultNodeLabel(HenshinNode node) {
		this.type = node.getType();
		this.action = node.getAction();
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(action);
		if (type instanceof ENamedElement) {
			result.append("");
			result.append(((ENamedElement)type).getName());
		}
		return result.toString();
	}


	public EObject getType() {
		return type;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DefaultNodeLabel)) {
			return false;
		}

		DefaultNodeLabel other = (DefaultNodeLabel) o;

		if (!other.getType().equals(this.getType())) {
			return false;
		}
		if (!other.getAction().equals(this.getAction())) {
			return false;
		}
		return true;
	}

	private Action getAction() {
		return action;
	}

	@Override
	public int hashCode() {
		return type.hashCode() + action.hashCode();
	}

	@Override
	public String getLabelName() {
		return toString();
	}
}

