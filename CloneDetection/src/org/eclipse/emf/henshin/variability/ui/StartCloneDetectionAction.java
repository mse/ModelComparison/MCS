package org.eclipse.emf.henshin.variability.ui;
 
import org.eclipse.emf.henshin.diagram.edit.parts.ModuleEditPart;
import org.eclipse.emf.henshin.diagram.providers.HenshinElementTypes;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;
import org.eclipse.emf.henshin.variability.ui.clonedetector.DummyCloneGroupDetector;
import org.eclipse.emf.henshin.variability.ui.views.CloneGroupView;
import org.eclipse.ui.IActionDelegate;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.DeferredCreateConnectionViewAndElementCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequestFactory;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class StartCloneDetectionAction implements IActionDelegate {

	public final static String ID = "org.eclipse.gmf.examples.mindmap.popup.MindmapCreateSubtopicActionID";
	private ModuleEditPart selectedElement;

	public void run(IAction action) {
		if (selectedElement.getModel() instanceof DiagramImpl) {
			CloneGroupView cloneGroupView = openAndGetCloneGroupView();
			cloneGroupView.showBusy(true);
			
			Module module = (Module) ((DiagramImpl) selectedElement.getModel())
					.getElement();
			DummyCloneGroupDetector cd = new DummyCloneGroupDetector(
					module.getRules());
			cd.detectCloneGroups();
			cloneGroupView.setContents(cd.getResultOrderedByNumberOfCommonElements());
			cloneGroupView.setContextDiagram(selectedElement);
			cloneGroupView.showBusy(false);
 
		} 
}

	private CloneGroupView openAndGetCloneGroupView() {
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().showView(CloneGroupView.ID);
			return 
					(CloneGroupView) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
			.getActivePage().findView(CloneGroupView.ID);
		} catch (PartInitException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		selectedElement = null;
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.getFirstElement() instanceof ModuleEditPart) {
				selectedElement = (ModuleEditPart) structuredSelection
						.getFirstElement();
			}
		}
	}

	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

}
