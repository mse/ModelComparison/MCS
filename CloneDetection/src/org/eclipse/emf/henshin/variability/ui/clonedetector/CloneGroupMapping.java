package org.eclipse.emf.henshin.variability.ui.clonedetector;
 
import java.util.Set; 
import java.util.Map;

import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Edge;
   
public class CloneGroupMapping {
	private Set<Rule> rules;
	private Map<Edge, Map<Rule, Edge>> edgeMappings;
	private Map<Attribute, Map<Rule, Attribute>> attributeMappings;
	
	public CloneGroupMapping(Set<Rule> rules,
			Map<Edge, Map<Rule, Edge>> edgeMappings,
			Map<Attribute, Map<Rule, Attribute>> attributeMappings) {
		this.rules = rules;
		this.edgeMappings = edgeMappings;
		this.attributeMappings = attributeMappings;
	}
   
	public Set<Rule> getRules() {
		return rules;
	}

	public Map<Edge, Map<Rule, Edge>> getEdgeMappings() {
		return edgeMappings;
	}
	
	
	public Map<Attribute, Map<Rule, Attribute>> getAttributeMappings() {
		return attributeMappings;
	}
	
	public int getNumberOfCommonEdges() {
		return edgeMappings.keySet().size() / rules.size();
	}
	
	public int getNumberOfCommonAttributes() {
		return attributeMappings.keySet().size() / rules.size();
	}
	
	
	@Override
	/**
	 * TO Do
	 * 
	 */
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public boolean equals(Object o) {
		CloneGroupMapping c = (CloneGroupMapping) o;
		if(! this.rules.equals(c.rules)) return false;
		if(! this.edgeMappings.equals(c.edgeMappings)) return false;
		if(! this.attributeMappings.equals(c.attributeMappings)) return false;
		return true;
	}
	/*
	@Override
	public String toString() {
		String res = "CloneGroupMapping - rules: " + rules.toString();
		return res;
	}
	*/
	
	@Override
	public String toString() {
		String res = "CloneGroupMapping - rules: " + rules.toString();
		res = res + "\n" + "CloneGroupMapping - edges: " + edgeMappings.toString();
		res = res + "\n" + "CloneGroupMapping - attributes: " + attributeMappings.toString() + "\n" + "\n";
		return res;
	}
	
	
}






