package de.hub.mse.mcs.experiments.wrappers.emf;

import java.util.*;
import java.util.concurrent.*;

import de.hub.mse.mcs.data.Pair;
import de.hub.mse.mcs.experiments.wrappers.CompareWrapper;
import de.hub.mse.mcs.experiments.wrappers.emf.EMFCompareTask;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphMappings;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphParser;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Mapping;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.compare.Comparison;

import static de.hub.mse.mcs.experiments.main.ExperimentRunner.VERBOSE;

/**
 * Wrapper class that offers methods in order to compare two HenshinRules with EMFCompare.
 */
public class EMFCompareWrapper extends CompareWrapper {
    private boolean useUUIDS;


    public EMFCompareWrapper(String useUIDS) {
        super("EMF", "EMFCompare", false);
        useUUIDS = useUIDS.contentEquals("UIDS");
        if (useUUIDS) {
            setNames("EMFUUID", "EMFCompareWithUUIDs");
        }
    }

    /**
     * Compare the two rules that are contained in the given Henshin resources with or without making use of the UUIDs of the resources
     */
    public void compare() {
        Set<Pair<EObject>> emfcMatches = new HashSet<>();

        ExecutorService executor = Executors.newSingleThreadExecutor();
        timeout = false;

        // Get matching engine registry
        IMatchEngine.Factory.Registry registry = MatchEngineFactoryRegistryImpl.createStandaloneInstance();

        // Set options, e.g. how to use (UU)IDs for model elements
        MatchEngineFactoryImpl matchEngineFactory;
        if (useUUIDS) {
            matchEngineFactory = new MatchEngineFactoryImpl(UseIdentifiers.ONLY);
        } else {
            matchEngineFactory = new MatchEngineFactoryImpl(UseIdentifiers.NEVER);
        }
        matchEngineFactory.setRanking(10); // default engine ranking is 10, must be higher to override.
        registry.add(matchEngineFactory);

        EMFCompare comparer = EMFCompare.builder().setMatchEngineFactoryRegistry(registry).build();
        IComparisonScope emfScope = new DefaultComparisonScope(resourceA, resourceB, null);

        // Compare and get all matches
        long start = System.currentTimeMillis();
        DaemonMaster daemonMaster = new DaemonMaster(comparer, emfScope);
        Comparison comparisonRes = null;

        try {
            Object tempResult = executor.submit(daemonMaster).get();
            if (tempResult instanceof Comparison) {
                comparisonRes = (Comparison) tempResult;
            }
            // Stop the master
            daemonMaster.stop();
        } catch (Exception e) {
            if (VERBOSE) {
                System.err.println("Daemon Master finished!");
            }
        }
        long stop = System.currentTimeMillis();
        runtime = stop - start;

        if (!timeout && comparisonRes != null) {
            EList<Match> tempMatches = comparisonRes.getMatches();

            // Collect the relevant matches in our own map
            emfcMatches = collectRelevantMatches(tempMatches);
        } else {
            try {
                Thread.sleep(RESET_TIME);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        }
        // Do the classification into TP FP and FN
        classifyTpAndFp(emfcMatches, runtime, result);
    }

    private Set<Pair<EObject>> collectRelevantMatches(EList<Match> tempMatches) {
        Set<Pair<EObject>> matches = new HashSet<>();
        for (Match tempMatch : tempMatches) {
            collectRelevantMatches(tempMatch, matches);
        }
        return matches;
    }

    private void collectRelevantMatches(Match tempMatch, Set<Pair<EObject>> matches) {
        if (tempMatch.getLeft() != null && tempMatch.getRight() != null) {
            EObject objA = tempMatch.getLeft();
            EObject objB = tempMatch.getRight();

            if (objA instanceof Rule) {
                Rule ruleA = (Rule) objA;
                Rule ruleB = (Rule) objB;

                if (ruleA.getName() != null) {
                    assert (ruleA.getName().equals(ruleB.getName()));
                } else {
                    assert (ruleB.getName() == null);
                }

            }
            if (objA instanceof Graph) {
                Graph graphA = (Graph) objA;
                Graph graphB = (Graph) objB;

                if (graphA.getName() != null) {
                    assert !graphA.getName().equals("Lhs") || (graphB.getName().equals("Lhs"));
                    assert !graphA.getName().equals("Rhs") || (graphB.getName().equals("Rhs"));
                } else {
                    assert (graphB.getName() == null);
                }
            }

            if (isRelevant(objA) && isRelevant(objB)) {
                matches.add(new Pair<>(objA, objB));
            }
        }

        for (Match subMatch : tempMatch.getSubmatches()) {
            collectRelevantMatches(subMatch, matches);
        }
    }




    private boolean isRelevant(EObject object) {
        if (object instanceof Node) {
            Node node = (Node) object;

            return !isMapped(node) && isInBothRules(object);
        }

        if (object instanceof Edge) {
            Edge edge = (Edge) object;

            boolean isMapped = isMapped(edge.getSource()) && isMapped(edge.getTarget());

            return !isMapped && isInBothRules(object);
        }

        return false;
    }

    private boolean isInBothRules(EObject object) {
        String idA = resourceA.getID(object);
        if (idA != null && idA != "") {
            for (Iterator<EObject> iterator = resourceB.getAllContents(); iterator.hasNext(); ) {
                EObject objB = iterator.next();
                if (resourceB.getID(objB).equals(idA))
                    return true;
            }
        }
        String idB = resourceB.getID(object);
        if (idB != null && idB != "") {
            for (Iterator<EObject> iterator = resourceA.getAllContents(); iterator.hasNext(); ) {
                EObject objA = iterator.next();
                if (resourceA.getID(objA).equals(idB))
                    return true;
            }
        }
        return false;
    }

    private boolean isMapped(Node node) {
        boolean isMapped = false;

        // Within a condition
        if (node.getGraph().eContainer() instanceof NestedCondition) {
            NestedCondition cond = (NestedCondition) node.getGraph().eContainer();

            for (Mapping m : cond.getMappings()) {
                if (m.getImage() == node) {
                    isMapped = true;
                }
            }
        }

        // Within a multi rule
        if (node.getGraph().eContainer() instanceof Rule) {
            Rule rule = (Rule) node.getGraph().eContainer();

            for (Mapping m : rule.getMultiMappings()) {
                if (m.getImage() == node) {
                    isMapped = true;
                }
            }
        }

        return isMapped;
    }

    private class DaemonMaster implements Callable {
        private EMFCompare cloneDetector;
        private IComparisonScope emfScope;
        private Comparison result;

        public DaemonMaster(EMFCompare cloneDetector, IComparisonScope emfScope) {
            this.cloneDetector = cloneDetector;
            this.emfScope = emfScope;
        }

        public Object call() {
            DaemonThreadFactory daemonThreadFactory = new DaemonThreadFactory("EMFDaemon");
            ExecutorService executor = Executors.newSingleThreadExecutor(daemonThreadFactory);

            try {
                EMFCompareTask task = new EMFCompareTask(cloneDetector, emfScope);
                Object tempResult = executor.submit(task).get(TIMEOUT, TIMEOUT_UNIT);
                if (tempResult instanceof Comparison) {
                    result = (Comparison) tempResult;
                }
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                System.err.println("EMFCompare reached the timeout of " + TIMEOUT + " " + TIMEOUT_UNIT.toString() + "!");
                timeout = true;
                daemonThreadFactory.getLastDaemon().stop();
            }
            return result;
        }

        public void stop() {
            throw new RuntimeException();
        }

    }
}
