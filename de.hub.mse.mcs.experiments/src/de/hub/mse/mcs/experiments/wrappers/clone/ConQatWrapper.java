package de.hub.mse.mcs.experiments.wrappers.clone;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroup;
import org.eclipse.emf.henshin.variability.mergein.clone.CloneGroupDetectionResult;
import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;

import icmt16rules.util.CloneComputationUtil;


public class ConQatWrapper extends CloneDetectionWrapper {

	public ConQatWrapper() {
		super("ConQat", "ConQat", true);
	}

	/**
	 * Compare the two rules that are contained in the given Henshin resources with or without making use of the UUIDs of the resources
	 */
	@Override
	public void compare() {
		CloneGroup cloneGroup = doCloneDetection(ruleA, ruleB);

		Map<Edge, Map<Rule, Edge>> edgeMapping = cloneGroup.getEdgeMappings();
		Map<Node, Map<Rule, Node>> nodeMapping = cloneGroup.getNodeMappings();

		combineMatches(edgeMapping, nodeMapping);
	}


	public CloneGroup doCloneDetection(Rule ruleA, Rule ruleB) {
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(ruleA);
		rules.add(ruleB);

		ConqatBasedCloneGroupDetector cq = new ConqatBasedCloneGroupDetector(rules, true);

		boolean hasResult = startCloneDetection(new DaemonMaster(cq));

		if (hasResult) {
			CloneGroupDetectionResult result = cq.getResultOrderedByNumberOfCommonElements();
			CloneGroup l = CloneComputationUtil.getLargest(result.getCloneGroups(), true);
			return l;
		} else {
			return new CloneGroup();
		}
	}

	private class DaemonMaster implements Callable {
		ConqatBasedCloneGroupDetector cloneDetector;

		public DaemonMaster(ConqatBasedCloneGroupDetector cloneDetector) {
			this.cloneDetector = cloneDetector;
		}

		public Object call() {
			DaemonThreadFactory daemonThreadFactory = new DaemonThreadFactory("ConQatDaemon");
			ExecutorService executor = Executors.newSingleThreadExecutor(daemonThreadFactory);

			try {
				executor.submit(new ConqatDetectorTask(cloneDetector)).get(TIMEOUT, TIMEOUT_UNIT);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				System.err.println("ConQat reached the timeout of " + TIMEOUT + " " + TIMEOUT_UNIT.toString() +"!");
				timeout = true;
				daemonThreadFactory.getLastDaemon().stop();
			}
			throw new RuntimeException();
		}
	}

}
