package de.hub.mse.mcs.experiments.wrappers.clone;

import java.util.*;
import java.util.concurrent.*;

import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.variability.ui.clonedetector.CloneGroupMapping;

import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneGroupDetectionResultAsCloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.cloneDetection.CloneMatrix;
import de.uni_marburg.fb12.swt.cloneDetection.combineDetectionWeb.CombineDetectionCDWebwithEScanInc;
import icmt16rules.util.CloneComputationUtil;
import icmt16rules.util.CloneDetectorTask;

public class ScanQatWrapper extends CloneDetectionWrapper {

	public ScanQatWrapper() {
		super("ScanQat", "ScanQat", true);
	}

	/**
	 * Compare the two rules that are contained in the given Henshin resources with or without making use of the UUIDs of the resources
	 */
	@Override
	public void compare() {
		CloneMatrix cloneMatrix = doCloneDetection(ruleA, ruleB);
		CloneGroupMapping cgMapping = cloneMatrix.getAsCloneGroupMapping();

		Map<Edge, Map<Rule, Edge>> edgeMapping = cgMapping.getEdgeMappings();
		Map<Node, Map<Rule, Node>> nodeMapping = deriveNodeMappings(edgeMapping);

		combineMatches(edgeMapping, nodeMapping);
	}
	
	public CloneMatrix doCloneDetection(Rule ruleA, Rule ruleB) {
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(ruleA);
		rules.add(ruleB);		
		
		CombineDetectionCDWebwithEScanInc detector = new CombineDetectionCDWebwithEScanInc(rules);
		boolean hasResult = startCloneDetection(new DaemonMaster(detector));
		
		if (hasResult) {
			CloneGroupDetectionResultAsCloneMatrix result = detector.getResultAsCloneMatrixOrderedByNumberOfCommonElements();
			CloneMatrix l = CloneComputationUtil.getLargest(result.getCloneGroups());
			return l;
		} else {
			return new CloneMatrix();
		}
	}

	private class DaemonMaster implements Callable {
		CombineDetectionCDWebwithEScanInc cloneDetector;

		public DaemonMaster(CombineDetectionCDWebwithEScanInc cloneDetector) {
			this.cloneDetector = cloneDetector;
		}

		public Object call() {
			DaemonThreadFactory daemonThreadFactory = new DaemonThreadFactory("ScanQatDaemon");
			ExecutorService executor = Executors.newSingleThreadExecutor(daemonThreadFactory);

			try {
				executor.submit(new CloneDetectorTask(cloneDetector)).get(TIMEOUT, TIMEOUT_UNIT);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				System.err.println("ScanQat reached the timeout of " + TIMEOUT + " " + TIMEOUT_UNIT.toString() +"!");
				timeout = true;
				daemonThreadFactory.getLastDaemon().stop();
			}
			throw new RuntimeException();
		}
	}
}
