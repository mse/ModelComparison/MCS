package de.hub.mse.mcs.experiments.wrappers.mcs;

import java.util.ArrayList;
import java.util.List;

import de.hub.mse.mcs.experiments.wrappers.CompareWrapper;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphMappings;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphParser;
import de.hub.mse.modelcomp.mcsimpl.CandidateCheckerIntegrated;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker.CandidateCheckerFeatures;

/**
 * Wrapper that applies the MCS algorithm to two graphs and evaluates result of the matching. 
 *
 */
public class MCSCompareWrapperInt extends MCSCompareWrapper {

	private boolean connected;

	public MCSCompareWrapperInt(String connected) {
		super("INTC", "MCSIntegrated", true);
		this.connected = connected.equals("connected");
		if (this.connected)
			setNames("INTCC", "MCCSIntegrated");
	}


	/**
	 * Compare the two given rules with each other based on their MCS or MCCS
	 */
	public void compare() {
			int numberOfIntegratedNodes = (parserA.getIntegratedGraph().vertexSet().size() + parserB.getIntegratedGraph().vertexSet().size()) / 2;
			int numberOfIntegratedEdges = (parserA.getIntegratedGraph().edgeSet().size() + parserB.getIntegratedGraph().edgeSet().size()) / 2;
			result.setNumberOfNodes(numberOfIntegratedNodes);
			result.setNumberOfEdges(numberOfIntegratedEdges);
			
			// -----------------------------------------------------------
			// Start the comparison of the integrated graphs
			// ----------------------------------------------------------
			HenshinToJGraphMappings mapA = new HenshinToJGraphMappings(parserA.getINTnodesMap(), parserA.getINTedgesMap());
			HenshinToJGraphMappings mapB = new HenshinToJGraphMappings(parserB.getINTnodesMap(), parserB.getINTedgesMap());
			//System.out.println("Comparing Integrated: " + numberOfIntegratedNodes + "Vs + " + numberOfIntegratedEdges + "Es");
			
			
			FeatureBasedCandidateChecker candidateChecker = new FeatureBasedCandidateChecker(features, ruleA.getModule().getImports());

			startRuleComparison(parserA.getIntegratedGraph(), parserB.getIntegratedGraph(),
					mapA, mapB,
					result,
					candidateChecker,
					connected, TIMEOUT,
					elementsOfA,
					elementsOfB);
			
	}
}
