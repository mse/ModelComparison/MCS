package de.hub.mse.mcs.experiments.wrappers.mcs;

import de.hub.mse.mcs.experiments.main.ExperimentResult;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphMappings;
import de.hub.mse.modelcomp.mcsimpl.CandidateCheckerSeparated;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.resource.HenshinResource;

import java.util.HashSet;
import java.util.Set;


/**
 * Wrapper that applies the MCS algorithm to two graphs and evaluates result of the matching.
 */
public class MCSCompareWrapperSep extends MCSCompareWrapper {

    private ExperimentResult resultLHS;
    private ExperimentResult resultRHS;

    private long HALF_TIMEOUT;

    private boolean connected;
    private Set<EObject> elementsOfALHS;
	private Set<EObject> elementsOfARHS;
	private Set<EObject> elementsOfBLHS;
	private Set<EObject> elementsOfBRHS;

    public MCSCompareWrapperSep(String connected) {
        super("SEPC", "MCSSeperate", false);
        HALF_TIMEOUT = this.TIMEOUT / 2;
        this.connected = connected.equals("connected");
        if (this.connected)
            setNames("SEPCC", "MCCSSeparate");
    }

    @Override
    public void nextRule(String name) {
        super.nextRule(name);
        resultLHS = new ExperimentResult("");
        resultRHS = new ExperimentResult("");
    }

    @Override
    public void nextComparison(HenshinResource resourceA, HenshinResource resourceB, Module module, Module modulecopy) {
        super.nextComparison(resourceA, resourceB, module, modulecopy);
        elementsOfALHS = new HashSet<>();
        elementsOfARHS = new HashSet<>();
        elementsOfBLHS = new HashSet<>();
        elementsOfBRHS = new HashSet<>();

        this.elementsOfALHS.addAll(parserA.getLHSnodesMap().keySet());
		this.elementsOfALHS.addAll(parserA.getLHSedgesMap().keySet());

		this.elementsOfARHS.addAll(parserA.getRHSnodesMap().keySet());
        this.elementsOfARHS.addAll(parserA.getRHSedgesMap().keySet());

        this.elementsOfBLHS.addAll(parserB.getLHSnodesMap().keySet());
		this.elementsOfBLHS.addAll(parserB.getLHSedgesMap().keySet());

		this.elementsOfBRHS.addAll(parserB.getRHSnodesMap().keySet());
		this.elementsOfBRHS.addAll(parserB.getRHSedgesMap().keySet());
    }

    @Override
    public void compare() {
        int numberOfLHSNodes = (parserA.getLHS().vertexSet().size() + parserB.getLHS().vertexSet().size()) / 2;
        int numberOfLHSEdges = (parserA.getLHS().edgeSet().size() + parserB.getLHS().edgeSet().size()) / 2;
        int numberOfRHSNodes = (parserA.getRHS().vertexSet().size() + parserB.getRHS().vertexSet().size()) / 2;
        int numberOfRHSEdges = (parserA.getRHS().edgeSet().size() + parserB.getRHS().edgeSet().size()) / 2;

        resultLHS.setNumberOfNodes(numberOfLHSNodes);
        resultRHS.setNumberOfNodes(numberOfRHSNodes);
        result.setNumberOfNodes(numberOfLHSNodes + numberOfRHSNodes);

        resultLHS.setNumberOfEdges(numberOfLHSEdges);
        resultRHS.setNumberOfEdges(numberOfRHSEdges);
        result.setNumberOfEdges(numberOfLHSEdges + numberOfRHSEdges);

        // ----------------------------------------------------------
        // Start the comparison of the LHS graphs
        // ----------------------------------------------------------
        HenshinToJGraphMappings mapA = new HenshinToJGraphMappings(parserA.getLHSnodesMap(), parserA.getLHSedgesMap());
        HenshinToJGraphMappings mapB = new HenshinToJGraphMappings(parserB.getLHSnodesMap(), parserB.getLHSedgesMap());
        //System.out.println("Comparing LHS: " + numberOfLHSNodes + "Vs + " + numberOfLHSEdges + "Es");

        startRuleComparison(parserA.getLHS(), parserB.getLHS(),
                mapA, mapB,
                resultLHS,
                new CandidateCheckerSeparated(),
                connected, HALF_TIMEOUT,
				elementsOfALHS, elementsOfBLHS);

        // ----------------------------------------------------------
        // Start the comparison of the RHS graphs
        // ----------------------------------------------------------
        mapA = new HenshinToJGraphMappings(parserA.getRHSnodesMap(), parserA.getRHSedgesMap());
        mapB = new HenshinToJGraphMappings(parserB.getRHSnodesMap(), parserB.getRHSedgesMap());

        //System.out.println("Comparing RHS: " + numberOfRHSNodes + "Vs + " + numberOfRHSEdges + "Es");
        startRuleComparison(parserA.getRHS(), parserB.getRHS(),
                mapA, mapB,
                resultRHS,
                new CandidateCheckerSeparated(),
                connected, HALF_TIMEOUT,
				elementsOfARHS, elementsOfBRHS);

        // -----------------------------------------------------------
        // Merge the results of LHS and RHS for the separated results
        result.combineResults(resultLHS, resultRHS);
        // -----------------------------------------------------------

    }

}
