package de.hub.mse.mcs.experiments.wrappers.clone;

import org.eclipse.emf.henshin.variability.mergein.clone.ConqatBasedCloneGroupDetector;

import java.util.concurrent.Callable;

public class ConqatDetectorTask implements Callable {

    private ConqatBasedCloneGroupDetector cloneDetector;

    public ConqatDetectorTask(ConqatBasedCloneGroupDetector cloneDetector) {
        this.cloneDetector = cloneDetector;
    }

    public Object call() throws Exception {
        this.cloneDetector.detectCloneGroups();
        return this.cloneDetector;
    }

}
