package de.hub.mse.mcs.experiments.wrappers.mcs;

import de.hub.mse.mcs.algorithm.CandidateChecker;
import de.hub.mse.mcs.algorithm.MCSAdapter;
import de.hub.mse.mcs.data.Pair;
import de.hub.mse.mcs.experiments.main.ExperimentResult;
import de.hub.mse.mcs.experiments.wrappers.CompareWrapper;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphMappings;
import de.hub.mse.modelcomp.mcsimpl.HenshinElement;
import org.eclipse.emf.ecore.EObject;
import org.jgrapht.Graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

abstract class MCSCompareWrapper extends CompareWrapper {

    MCSCompareWrapper(String shortName, String longName, boolean comparesIntegrated) {
        super(shortName, longName, comparesIntegrated);
    }

    void startRuleComparison(Graph<HenshinElement, HenshinElement> graphA, Graph<HenshinElement, HenshinElement> graphB,
                             HenshinToJGraphMappings groundMapA,
                             HenshinToJGraphMappings groundMapB,
                             ExperimentResult result,
                             CandidateChecker<HenshinElement, HenshinElement> candidateChecker,
                             boolean connected, long TIMEOUT,
                             Set<EObject> elementsOfA,
                             Set<EObject> elementsOfB) {

        String label;
        ArrayList<String> labels = new ArrayList<>();
        Set<HenshinElement> nodesOfGraphA = graphA.vertexSet();
        for (HenshinElement v : nodesOfGraphA) {
            label = v.getGroupLabel();
            if (!labels.contains(label)) {
                labels.add(label);
            }
        }
        result.setNumberOfLabel(labels.size());

        MCSAdapter<HenshinElement, HenshinElement> mcsadapter = new MCSAdapter<>(
                graphA,
                graphB,
                candidateChecker, TIMEOUT);

        HashSet<Pair<HenshinElement>> nodeMapAtoB;
        HashSet<Pair<HenshinElement>> edgeMapAtoB;

        long start = System.currentTimeMillis();

        if (connected) {
            mcsadapter.createMaxCommonConnectedSubgraph();
            nodeMapAtoB = mcsadapter.getMCCSNodeMapping();
            edgeMapAtoB = mcsadapter.getMCCSEdgeMapping();
        } else {
            mcsadapter.createMaxCommonSubgraph();
            nodeMapAtoB = mcsadapter.getMCSNodeMapping();
            edgeMapAtoB = mcsadapter.getMCSEdgeMapping();
        }

        long runtime = System.currentTimeMillis() - start;
        Set<Pair<EObject>> matches = collectMatches(nodeMapAtoB, edgeMapAtoB, groundMapA, groundMapB);
        classifyTpAndFp(matches, runtime, result, elementsOfA, elementsOfB);
    }

    private Set<Pair<EObject>> collectMatches(HashSet<Pair<HenshinElement>> nodeMapAtoB,
                                              HashSet<Pair<HenshinElement>> edgeMapAtoB,
                                              HenshinToJGraphMappings groundMapA,
                                              HenshinToJGraphMappings groundMapB) {
        Set<Pair<EObject>> matches = new HashSet<>();

        for (Pair<HenshinElement> nodePair : nodeMapAtoB) {
            HenshinElement jgNodeA = nodePair.getFirst();
            HenshinElement jgNodeB = nodePair.getSecond();

            matches.add(new Pair<>(groundMapA.getNode(jgNodeA), groundMapB.getNode(jgNodeB)));
        }
        for (Pair<HenshinElement> edgePair : edgeMapAtoB) {
            HenshinElement jgEdgeA = edgePair.getFirst();
            HenshinElement jgEdgeB = edgePair.getSecond();

            matches.add(new Pair<>(groundMapA.getEdge(jgEdgeA), groundMapB.getEdge(jgEdgeB)));
        }
        return matches;
    }

}
