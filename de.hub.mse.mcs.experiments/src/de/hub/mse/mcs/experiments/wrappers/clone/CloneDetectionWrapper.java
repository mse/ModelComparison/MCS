package de.hub.mse.mcs.experiments.wrappers.clone;

import de.hub.mse.mcs.data.Pair;
import de.hub.mse.mcs.experiments.wrappers.CompareWrapper;
import de.hub.mse.modelcomp.mcsimpl.HenshinElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.*;
import org.eclipse.emf.henshin.model.resource.HenshinResource;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static de.hub.mse.mcs.experiments.main.ExperimentRunner.VERBOSE;

abstract class CloneDetectionWrapper extends CompareWrapper {

    CloneDetectionWrapper(String shortName, String longName, boolean comparesIntegrated) {
        super(shortName, longName, comparesIntegrated);
    }

    void combineMatches(Map<Edge, Map<Rule, Edge>> edgeMapping, Map<Node, Map<Rule, Node>> nodeMapping) {
        Set<Pair<EObject>> matches = new HashSet<>();

        matches.addAll(translateToPairs(edgeMapping));
        matches.addAll(translateToPairs(nodeMapping));

        classifyTpAndFp(matches, runtime, result);
    }

    boolean startCloneDetection(Callable daemonMaster) {
        timeout = false;
        ExecutorService executor = Executors.newSingleThreadExecutor();

        long start = System.currentTimeMillis();
        try {
            executor.submit(daemonMaster).get();
        } catch (Exception e) {
            if (VERBOSE) {
                System.out.println("Daemon Master finished!");
            }
            try {
                Thread.sleep(RESET_TIME);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        }

        long stop = System.currentTimeMillis();
        runtime = stop - start;

        return !timeout;
    }

    Map<Node, Map<Rule, Node>> deriveNodeMappings(Map<Edge, Map<Rule, Edge>> edgeMapping) {
        Map<Node, Map<Rule, Node>> nodeMapping = new HashMap<Node, Map<Rule, Node>>();
        for (Edge e : edgeMapping.keySet()) {
            Node source = e.getSource();
            Node target = e.getTarget();
            HashMap<Rule, Node> sources = new HashMap<Rule, Node>();
            HashMap<Rule, Node> targets = new HashMap<Rule, Node>();
            if (edgeMapping.get(e).keySet().size() > 2)
                System.err.println("Are there more than two rules, you are comparing at once?");
            for (Rule rule : edgeMapping.get(e).keySet()) {
                Edge innerEdge = edgeMapping.get(e).get(rule);
                sources.put(rule, innerEdge.getSource());
                targets.put(rule, innerEdge.getTarget());
            }
            nodeMapping.put(source, sources);
            nodeMapping.put(target, targets);
        }
        return nodeMapping;
    }

    private boolean isParsedElement(EObject element, Set<EObject> parsedElements, HenshinResource resource) {
        for (EObject parsedElement : parsedElements) {
            String parsedID = resource.getID(parsedElement);
            String elementID = resource.getID(element);
            if (elementID == null) {
                return false;
            }
            if (parsedID.equals(elementID)) {
                return true;
            }
        }
        return false;
    }

    private <T extends GraphElement> Set<Pair<EObject>> translateToPairs(Map<T, Map<Rule, T>> map) {
        Set<Pair<EObject>> matches = new HashSet<>();
        for (T e1 : map.keySet()) {
            if (map.get(e1).keySet().size() > 2)
                System.err.println("The element should only be mapped onto ONE other element, as we compare only two rules!");
            if (resourceA.getID(e1) == null) {
                continue;
            }
            for (Rule r : map.get(e1).keySet()) {
                T e2 = map.get(e1).get(r);
                if (resourceB.getID(e2) == null) {
                    continue;
                }
                T parsedE1 = getParsedObjectIfAvailable(e1, elementsOfA, ruleA);
                T parsedE2 = getParsedObjectIfAvailable(e2, elementsOfB, ruleB);
                matches.add(new Pair<>(parsedE1, parsedE2));
            }
        }
        return matches;
    }

    private <T extends GraphElement> T getParsedObjectIfAvailable(T element, Set<EObject> parsedElements, Rule rule) {
        // The given element is already one of the parsed elements
        if (parsedElements.contains(element)) {
            return element;
        }
        // Check if the parsed EObject can be found that is equivalent to the given element
        MappingList mappingList = rule.getAllMappings();
        GraphElement origin = mappingList.getOrigin((GraphElement) element);

        if (origin != null && parsedElements.contains(origin)) {
            return (T) origin;
        }

        // Try the multi-mappings
        mappingList = rule.getMultiMappings();
        origin = mappingList.getOrigin((GraphElement) element);

        if (origin != null && parsedElements.contains(origin)) {
            return (T) origin;
        }

        // No suitable object found, we return the given one
        return element;
    }

}
