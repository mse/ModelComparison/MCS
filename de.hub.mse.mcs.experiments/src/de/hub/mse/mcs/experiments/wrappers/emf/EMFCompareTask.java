package de.hub.mse.mcs.experiments.wrappers.emf;

import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.IComparisonScope;

import java.util.concurrent.Callable;

public class EMFCompareTask implements Callable {
    private EMFCompare cloneDetector;
    private IComparisonScope emfScope;

    public EMFCompareTask(EMFCompare cloneDetector, IComparisonScope scope) {
        this.cloneDetector = cloneDetector;
        this.emfScope = scope;
    }

    public Object call() throws Exception {
        return this.cloneDetector.compare(emfScope);
    }
}

