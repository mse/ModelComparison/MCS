package de.hub.mse.mcs.experiments.wrappers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import de.hub.mse.mcs.experiments.wrappers.clone.ConQatWrapper;
import de.hub.mse.mcs.experiments.wrappers.clone.EScanWrapper;
import de.hub.mse.mcs.experiments.wrappers.clone.ScanQatWrapper;
import de.hub.mse.mcs.experiments.wrappers.emf.EMFCompareWrapper;
import de.hub.mse.mcs.experiments.wrappers.mcs.MCSCompareWrapperInt;
import de.hub.mse.mcs.experiments.wrappers.mcs.MCSCompareWrapperSep;
import de.hub.mse.modelcomp.mcsimpl.HenshinElement;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.eclipse.emf.henshin.model.resource.HenshinResource;

import de.hub.mse.mcs.data.Pair;
import de.hub.mse.mcs.experiments.main.ExperimentResult;
import de.hub.mse.mcs.experiments.main.ExperimentRunner;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphMappings;
import de.hub.mse.modelcomp.mcscomp.HenshinToJGraphParser;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker.CandidateCheckerFeatures;

public abstract class CompareWrapper {
	
	protected HenshinResource resourceA;
	protected HenshinResource resourceB;
	protected Module module;
	protected Module modulecopy;
	protected Rule ruleA;
	protected Rule ruleB;
	protected Set<EObject> elementsOfA;
	protected Set<EObject> elementsOfB;
	protected HenshinToJGraphParser parserA;
	protected HenshinToJGraphParser parserB;

	// Stores the results of EMFCompare on different permuted versions of two rules
	protected ExperimentResult result;

	protected List<CandidateCheckerFeatures> features = new ArrayList<>();;

	protected long runtime = 0L;
	protected long TIMEOUT;
	protected TimeUnit TIMEOUT_UNIT = TimeUnit.MILLISECONDS;
	protected long RESET_TIME;
	protected boolean timeout;

	private StringBuilder csvBuilder;
	private StringBuilder rtBuilder;
	private ArrayList<ExperimentResult> Results;
	private String shortName;
	private String longName;
	private int daemonCounter = 0;
	private final boolean comparesIntegrated;


	public CompareWrapper(String shortName, String longName, boolean comparesIntegrated) {
		csvBuilder = new StringBuilder(ExperimentResult.header());
		rtBuilder = new StringBuilder();
		Results = new ArrayList<>();
		this.TIMEOUT = ExperimentRunner.TIMEOUT;
		this.RESET_TIME = ExperimentRunner.CLEANUP_TIME;
		this.shortName = shortName;
		this.longName = longName;
		this.comparesIntegrated = comparesIntegrated;
	}

	public abstract void compare();

	public void setFeatures(List<CandidateCheckerFeatures> features){
		for (CandidateCheckerFeatures feature : features) {
			this.features.add(feature);
			this.longName = this.longName + FeatureBasedCandidateChecker.ccfToString(feature);
			this.shortName = this.shortName + FeatureBasedCandidateChecker.ccfToString(feature);
		}
	}

	protected void setNames(String shortName, String longName) {
		this.shortName = shortName;
		this.longName = longName;
	}

	public String getLongName() {
		return longName;
	}

	public void nextRule(String name) {
		this.result = new ExperimentResult(this.shortName + ": " + name);
	}

	public void setTimeout(long timeout) {
		this.TIMEOUT = timeout;
	}

	public boolean validateResult(int iteration) {
		return this.result.resetToIteration(iteration);
	}

	public void nextFolder() {
		this.Results = new ArrayList<>();
		this.csvBuilder = new StringBuilder(ExperimentResult.header());
		this.rtBuilder = new StringBuilder();
	}

	public void nextComparison(HenshinResource resourceA, HenshinResource resourceB, Module module, Module modulecopy) {
		this.resourceA = resourceA;
		this.resourceB = resourceB;
		this.module = module;
		this.modulecopy = modulecopy;

		Pair<Rule> rules = getRules(this.module, this.modulecopy);
		this.ruleA = rules.getFirst();
		this.ruleB = rules.getSecond();

		this.parserA = new HenshinToJGraphParser(ruleA, resourceA);
		this.parserB = new HenshinToJGraphParser(ruleB, resourceB);

		this.elementsOfA = new HashSet<>();
		this.elementsOfB = new HashSet<>();

		if (this.comparesIntegrated) {
			this.elementsOfA.addAll(parserA.getINTnodesMap().keySet());
			this.elementsOfA.addAll(parserA.getINTedgesMap().keySet());

			this.elementsOfB.addAll(parserB.getINTnodesMap().keySet());
			this.elementsOfB.addAll(parserB.getINTedgesMap().keySet());
		} else {
			this.elementsOfA.addAll(parserA.getLHSnodesMap().keySet());
			this.elementsOfA.addAll(parserA.getRHSnodesMap().keySet());
			this.elementsOfA.addAll(parserA.getLHSedgesMap().keySet());
			this.elementsOfA.addAll(parserA.getRHSedgesMap().keySet());

			this.elementsOfB.addAll(parserB.getLHSnodesMap().keySet());
			this.elementsOfB.addAll(parserB.getRHSnodesMap().keySet());
			this.elementsOfB.addAll(parserB.getLHSedgesMap().keySet());
			this.elementsOfB.addAll(parserB.getRHSedgesMap().keySet());
		}
	}

	public ArrayList<ExperimentResult> getResults() {
		return Results;
	}

	public ExperimentResult getResult() {
		return result;
	}

	protected Pair<Rule> getRules(Module m1, Module m2){
		EList<Unit> listrulesA = m1.getUnits();
		for (Unit u : listrulesA) {
			if (!(u instanceof Rule)) {
				continue;
			}
			// load rules and compare them
			Rule r1 = (Rule) u;
			Rule r2 = (Rule) m2.getUnit(r1.getName());
			return new Pair<>(r1, r2);
		}
		System.err.println("Error in getRules(Module m1, Module m2)!");
		return null;
	}

	public void print() {
		result.print();
	}

	public void appendRuleResults() {
		appendCSV();
		appendRT();
		appendResults();
	}

	public void writeCSVs() {
		writeCSVFile(ExperimentRunner.pathToMutationDir + File.separator + ExperimentRunner.folderName + "_" + shortName + ".csv", csvBuilder.toString());
		writeCSVFile(ExperimentRunner.pathToResultRuntimes + File.separator + ExperimentRunner.folderName + "_" + shortName +  ".csv", rtBuilder.toString());
	}

	public static void writeCSVFile(String filePath, String content) {
		try (PrintWriter writerSEP = new PrintWriter(new File(filePath))) {
		     writerSEP.append(content);
	    } catch (FileNotFoundException e) {
	      throw new RuntimeException(e);
	    }
	}

	public String getName() {
		return this.shortName;
	}

	public void updateNodeAndEdgeCount(ArrayList<CompareWrapper> wrappers) {
		if (this instanceof EMFCompareWrapper)
			for (CompareWrapper w2 : wrappers)
				if (w2 instanceof MCSCompareWrapperSep) {
					result.setNumberOfNodes(w2.getResult().getNumberOfNodes());
					result.setNumberOfEdges(w2.getResult().getNumberOfEdges());
				}
		if (this instanceof ConQatWrapper || this instanceof EScanWrapper || this instanceof ScanQatWrapper)
			for (CompareWrapper w2 : wrappers)
				if (w2 instanceof MCSCompareWrapperInt) {
					result.setNumberOfNodes(w2.getResult().getNumberOfNodes());
					result.setNumberOfEdges(w2.getResult().getNumberOfEdges());
				}
	}

	protected class DaemonThreadFactory implements ThreadFactory {
		private String prefix;

		private Thread lastDaemon;
		public DaemonThreadFactory(String prefix) {
			this.prefix = prefix;
		}

		public Thread newThread(Runnable r) {
			Thread daemon = new Thread(r, prefix + "-" + daemonCounter++);
			daemon.setDaemon(true);
			this.lastDaemon = daemon;
			return daemon;
		}

		public Thread getLastDaemon() {
			return lastDaemon;
		}

	}

	protected void classifyTpAndFp(Set<Pair<EObject>> matches,
								   long runtime, ExperimentResult tempResult) {
		classifyTpAndFp(matches, runtime, result, this.elementsOfA, this.elementsOfB);
	}

	protected void classifyTpAndFp(Set<Pair<EObject>> matches,
								 long runtime, ExperimentResult tempResult,
								   Set<EObject> elementsOfA,
								   Set<EObject> elementsOfB) {
		int tp = 0;
		int fp = 0;
		int fn = 0;

		for (Pair<EObject> p : matches) {
			EObject elementA = p.getFirst();
			EObject elementB = p.getSecond();
			// We only consider elements that could be parsed for other wrappers
			if (elementsOfA.contains(elementA) && elementsOfB.contains(elementB)) {
				String idA = resourceA.getID(elementA);
				String idB = resourceB.getID(elementB);
				if (idA.equals(idB))
					tp++;
				else
					fp++;
			} else {
				System.out.println(this.shortName + ": Found two unparsed elements in matches!");
			}
		}

		for (EObject objA : elementsOfA) {
			String groundIdA = resourceA.getID(objA);
			for (EObject objB : elementsOfB) {
				String groundIdB = resourceB.getID(objB);
				if (groundIdA.equals(groundIdB)) {
					boolean found = false;

					for (Pair<EObject> p : matches) {
						EObject elementA = p.getFirst();
						EObject elementB = p.getSecond();
						String idA = resourceA.getID(elementA);
						String idB = resourceB.getID(elementB);

						if (idA.equals(groundIdA) && idB.equals(groundIdB)) {
							found = true;
							break;
						}
					}
					if (!found) {
						fn++;
					}
				}
			}
		}

		tempResult.addResult(tp, fp, fn, runtime);
	}


	private void appendCSV() {
		csvBuilder.append(result.toLine());
	}

	private void appendRT() {
		rtBuilder.append(result.toRuntimeLine());
	}

	private void appendResults() {
		Results.add(result);
	}

}
