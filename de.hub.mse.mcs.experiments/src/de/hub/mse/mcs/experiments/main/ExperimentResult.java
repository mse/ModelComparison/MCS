package de.hub.mse.mcs.experiments.main;

import scala.util.parsing.combinator.testing.Str;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Container class that stores all values which are of relevance in order to evaluate the comparison of two rules. 
 * This class is meant to store the results of several comparison runs of two rules and offers methods to get the
 * average over these runs.  <br><br>
 * In addition this class offers static methods to calculate the average over several results of one set of 
 * comparisons across several rules.
 *
 */
public class ExperimentResult {
	// Name of this result object that is used in the printing of the results to identify them
	private String name;
	// The number of nodes of the rule that is compared with itself, double to represent average numbers
	private double numberOfNodes;
	// The number of edges of the rule that is compared with itself, double to represent average numbers
	private double numberOfEdges;
	// The number of experiment iterations from which the average values are calculated
	private int iterations;
	
	// List with the true positive value of each run, double for storing average
	private ArrayList<Double> tpPerRun;
	// List with the false positive value of each run, double for storing average
	private ArrayList<Double> fpPerRun;
	// List with the false negative value of each run, double for storing average
	private ArrayList<Double> fnPerRun;

	private ArrayList<Long> runtimePerRun;
	private ArrayList<Double> precisionPerRun;
	private ArrayList<Double> recallPerRun;
	private ArrayList<Double> fMeasurePerRun;
	
	private double numberOfLabel;
	
	/**
	 * Initialize an empty result object that has the given name by which a printed result line can be identified
	 * @param name The name of this result object
	 */
	public ExperimentResult(String name) {
		this.name = name;
		this.numberOfNodes = 0;
		this.numberOfEdges = 0;
		this.iterations = 0;
		
		this.tpPerRun = new ArrayList<>();
		this.fpPerRun = new ArrayList<>();
		this.fnPerRun = new ArrayList<>();
		this.runtimePerRun = new ArrayList<>();
		
		this.precisionPerRun = new ArrayList<>();
		this.recallPerRun = new ArrayList<>();
		this.fMeasurePerRun = new ArrayList<>();
		
		this.numberOfLabel = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public void setNumberOfNodes(double num) {
		this.numberOfNodes = num;
	}
	
	public void setNumberOfEdges(double num) {
		this.numberOfEdges = num;
	}
	
	public void setNumberOfLabel(double num) {
		this.numberOfLabel = num;
	}
	
	/**
	 * Add the result of another experiment run for the comparison of two rules to this object. This is used to store the results of all
	 * iterations of the comparison of two specific rules.
	 * @param tp number of true positives
	 * @param fp number of false positives
	 * @param fn number of false negatives
	 * @param runtime runtime of the experiment run
	 */
	public void addResult(double tp, double fp, double fn, long runtime) {
		iterations++;
		tpPerRun.add(tp);
		fpPerRun.add(fp);
		fnPerRun.add(fn);	
		runtimePerRun.add(runtime);
		
		precisionPerRun.add(calculatePrecision(tp, fp));
		recallPerRun.add(calculateRecall(tp, fn));
		fMeasurePerRun.add(calculateFMeasure(tp, fp, fn));
	}

	public boolean resetToIteration(int i) {
		boolean hadToReset = false;
		while (iterations >= i) {
			removeLastResult();
			hadToReset = true;
		}
		return hadToReset;
	}

	private void removeLastResult() {
		int lastRunId = iterations - 1;
		tpPerRun.remove(lastRunId);
		fpPerRun.remove(lastRunId);
		fnPerRun.remove(lastRunId);
		runtimePerRun.remove(lastRunId);

		precisionPerRun.remove(lastRunId);
		recallPerRun.remove(lastRunId);
		fMeasurePerRun.remove(lastRunId);

		iterations--;
	}
	
	public double getTPAvg() {
		return getAvgOfList(tpPerRun);
	}
	
	public double getFPAvg() {
		return getAvgOfList(fpPerRun);
	}
	
	public double getFNAvg() {
		return getAvgOfList(fnPerRun);
	}
	
	public double getPrecisionAvg() {
		return getAvgOfList(precisionPerRun);
	}
	
	public double getRecallAvg() {
		return getAvgOfList(recallPerRun);
	}
	
	public double getFMeasureAvg() {
		return getAvgOfList(fMeasurePerRun);
	}
	
	public List<Long> getRuntimes(){
		return this.runtimePerRun;
	}
	
	public long getRuntimeAvg() {
		long sum = 0;
		for (long rt : runtimePerRun) {
			sum += rt;
		}
		
		return sum/(long)iterations;
	}

	public void validate() {
		for (int i = 0; i < iterations; i++) {
			if (tpPerRun.get(i) + fnPerRun.get(i) != numberOfEdges + numberOfNodes) {
				this.print();
				throw new RuntimeException("Invalid result!");
			}
			long timeout = ExperimentRunner.TIMEOUT + 500;
			timeout += (timeout / 20);
			if (runtimePerRun.get(i) > timeout) {
				// TODO: Change
				//throw new RuntimeException("Timeout exceeded by too much! Set: " + timeout + " , Found: " + runtimePerRun.get(i));
			}

		}
	}
	
	/**
	 * Print 0 as "< 1"
	 * @param number
	 * @return "< 1" if the given runtime is 0, otherwise the String representation of the given runtime
	 */
	private String handleZero(long number) {
		return number == 0 ? "<1" : String.valueOf(number);
	}
	
	private static double getAvgOfList(ArrayList<Double> list) {
		double sum = 0;
		for(Double d : list) {
			sum += d;
		}
		return round(sum/(double) list.size(), 2);
	}
	
	/**
	 * Print the average values of this result object into the console.
	 */
	public void print() {
		//if (!name.contains("EMF") && Math.abs(getFNAvg() - getFPAvg()) > 0.2) {
		//		System.err.println(getFNAvg() + " " + getFPAvg());
		///		System.err.println(name);		
		String s =  "++++++++++++++++++++++++++++++++++++++++++++++++++\n" + name + "\n";
		s += "Nodes: " + numberOfNodes + " ; Edges: " + numberOfEdges + "\n";
		s += "AvgTP: " + getTPAvg() + " ; AvgFP: " + getFPAvg() + " ; AvgFN: " + getFNAvg() + "\n";
		s += "PrecisionAvg: " + getPrecisionAvg() + " , RecallAvg: " + getRecallAvg() + " ; RuntimeAvg: " + handleZero(getRuntimeAvg()) + "\n";
		s += "Number of Labels: " + numberOfLabel + " ; F-Measure: " + getFMeasureAvg() + "\n";
		s += "++++++++++++++++++++++++++++++++++++++++++++++++++\n";
		System.out.println(s);
		//}
	}
	
	/**
	 * @return A representation of the average values of this result object that can be included into a CSV-file as row
	 */
	public String toLine() {
		return name + ";=\"" 
				+ numberOfNodes + "\";=\"" 
				+ numberOfEdges + "\";=\"" 
				+ getTPAvg() + "\";=\"" 
				+ getFPAvg() + "\";=\"" 
				+ getFNAvg() + "\";=\"" 
				+ getPrecisionAvg() + "\";=\""
				+ getRecallAvg() + "\";=\"" 
				+ getFMeasureAvg() + "\";=\""
				+ handleZero(getRuntimeAvg()) + "\"\n";
	}
	
	/**
	 * 
	 * @return A String that can be included into a latex table in order to show the average results
	 */
	public String toLatexLine() {
		return name + " & "
				+ numberOfNodes + " & " 
				+ numberOfEdges + " & " 
				+ getTPAvg() + " & " 
				+ getFPAvg() + " & " 
				+ getFNAvg() + " & " 
				+ getPrecisionAvg() + " & "
				+ getRecallAvg() + " & " 
				+ getFMeasureAvg() + " & "
				+ handleZero(getRuntimeAvg());
	}

	public String toTimeoutBenchmarkLine(int id, long timeout) {
		StringBuilder resultBuilder = new StringBuilder();
		for (int i = 0; i < iterations; i++) {
			resultBuilder.append(id);
			resultBuilder.append(" & ");
			resultBuilder.append(name);
			resultBuilder.append(" & ");
			DecimalFormat df = new DecimalFormat("0.00");
			df.setRoundingMode(RoundingMode.CEILING);
			resultBuilder.append(df.format(fMeasurePerRun.get(i)));
			resultBuilder.append(" & ");
			resultBuilder.append(timeout);
			resultBuilder.append("\r\n");
		}
		return resultBuilder.toString();
	}
	
	/*
	 * A String representation of the runtime for all runs of each rule
	 */
	public String toRuntimeLine() {
		StringBuilder sb = new StringBuilder(name);
		for (long val : runtimePerRun) {
			sb.append(" ; ");
			sb.append(val);
		}
		sb.append("\n");
		return sb.toString();
	}
	
	/*
	 * Combine the two given ExperimentResult object, by adding the values that they contain. This is used in order to get the 
	 * combined result of the two separate comparisons of the left-hand side and right-hand side of rules. 
	 */
	public void combineResults(ExperimentResult first, ExperimentResult second) {
		iterations = first.iterations;
		setNumberOfLabel(first.numberOfLabel + second.numberOfLabel);
		tpPerRun = sumDoubleLists(first.tpPerRun, second.tpPerRun);
		fpPerRun = sumDoubleLists(first.fpPerRun, second.fpPerRun);
		fnPerRun = sumDoubleLists(first.fnPerRun, second.fnPerRun);
		tpPerRun = sumDoubleLists(first.tpPerRun, second.tpPerRun);
		
		precisionPerRun = new ArrayList<>();
		for (int i = 0; i < iterations; i++) {
			precisionPerRun.add(calculatePrecision(tpPerRun.get(i), fpPerRun.get(i)));
		}
		
		recallPerRun = new ArrayList<>();
		for (int i = 0; i < iterations; i++) {
			recallPerRun.add(calculateRecall(tpPerRun.get(i), fnPerRun.get(i)));
		}
		
		fMeasurePerRun = new ArrayList<>();
		for (int i = 0; i < iterations; i++) {
			fMeasurePerRun.add(calculateFMeasure(tpPerRun.get(i), fpPerRun.get(i), fnPerRun.get(i)));
		}
		
		runtimePerRun = new ArrayList<>();
		for (int i = 0; i < iterations; i++) {
			runtimePerRun.add(first.runtimePerRun.get(i) + second.runtimePerRun.get(i));
		}
	}
	
	public double getNumberOfNodes() {
		return numberOfNodes;
	}
	
	public double getNumberOfEdges() {
		return numberOfEdges;
	}
	
	/**
	 * 
	 * @return A header that can be included as header in a CSV-file
	 */
	public static String header() {
		return "Name;#Nodes;#Edges;TPAvg;FPAvg;FNAvg;PrecisionAvg;RecallAvg;F-MeasureAvg;newF-Measure;RuntimeAvg\n";
	}
	
	/**
	 * Calculate the average result over a given list of results and give the result the provided name
	 * @param results The list of results that are to be averages, e.g., the results over a benchmark set
	 * @param name The name of the returned result object
	 * @return A new ExperimentResult object that contains the average of the given list of results
	 */
	public static ExperimentResult getAvgOverLargeSet(List<ExperimentResult> results, String name) {
		if (results == null || results.size() == 0) {
			throw new IllegalArgumentException();
		}
		
		ExperimentResult avgResult = new ExperimentResult(name);
		
		double sumNodes = 0;
		double sumEdges = 0;
		
		for (ExperimentResult r : results) {
			avgResult.iterations++;
			sumNodes += r.numberOfNodes;
			sumEdges += r.numberOfEdges;
			
			avgResult.tpPerRun.add(r.getTPAvg());
			avgResult.fpPerRun.add(r.getFPAvg());
			avgResult.fnPerRun.add(r.getFNAvg());
			avgResult.runtimePerRun.add(r.getRuntimeAvg());
			
			avgResult.recallPerRun.add(r.getRecallAvg());
			avgResult.precisionPerRun.add(r.getPrecisionAvg());
			avgResult.fMeasurePerRun.add(r.getFMeasureAvg());
		}
		
		avgResult.setNumberOfNodes(round(sumNodes / avgResult.iterations, 2));
		avgResult.setNumberOfEdges(round(sumEdges / avgResult.iterations, 2));

		return avgResult;
	}
	
	private ArrayList<Double> sumDoubleLists(ArrayList<Double> first, ArrayList<Double> second) {
		ArrayList<Double> result = new ArrayList<>();
		
		for (int i = 0; i < first.size(); i++) {
			result.add(first.get(i) + second.get(i));
		}
		return result;
	}
	
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	private double calculatePrecision(double tp, double fp) {
		if (tp == 0) {
			return 0.0d;
		} else {
			return (tp / (tp + fp));
		}
	}
	
	private double calculateRecall(double tp, double fn) {
		if (tp == 0) {
			return 0.0d;
		} else {
			return (tp / (tp + fn));
		}
	}
	
	private double calculateFMeasure(double tp, double fp, double fn) {
		double prec = calculatePrecision(tp, fp);
		double rec = calculateRecall(tp, fn);
		
		if (prec + rec == 0) {
			return 0;
		}
		
		return (2*(prec * rec))/(prec+rec);
	}

}
