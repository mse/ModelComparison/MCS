package de.hub.mse.mcs.experiments.main;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResource;

import java.io.IOException;
import java.util.*;

/**
 * RulePermutator offers the necessary functionality in order to remove the names of the nodes and edges of Henshin rules, 
 * and to permute the order of the nodes and edges in their lists. 
 * 
 */
public class RulePermutator {
	private static final long seed = 33L;
	private static Random randomGenerator = new Random(seed);

	public static void permutator(HenshinResource resource) throws IOException {

		for (Iterator<EObject> iterator = resource.getAllContents(); iterator.hasNext();) {
			EObject obj = iterator.next();
			if (obj instanceof Rule) {
				Rule rule = (Rule) obj;

				// delete node names
				deleteNodeNames(rule.getLhs());
				deleteNodeNames(rule.getRhs());

				// permute graph elements
				permuteGraph(rule.getLhs());
				permuteGraph(rule.getRhs());
			}
		}

		//resource.save(Collections.EMPTY_MAP);
	}

	private static void deleteNodeNames(Graph graph) {
		for (Node node : graph.getNodes()) {
			node.setName("");
		}
	}

	private static void permuteGraph(Graph graph) {
				
		List<Node> nodes = new ArrayList<>(graph.getNodes());
		List<Edge> edges = new ArrayList<>(graph.getEdges());

		Collections.shuffle(nodes, randomGenerator);
		Collections.shuffle(edges, randomGenerator);

		graph.getNodes().clear();
		graph.getNodes().addAll(nodes);
		graph.getEdges().clear();
		graph.getEdges().addAll(edges);
	}
}
