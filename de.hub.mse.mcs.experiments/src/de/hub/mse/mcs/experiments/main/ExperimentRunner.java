package de.hub.mse.mcs.experiments.main;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import de.hub.mse.mcs.experiments.wrappers.clone.ConQatWrapper;
import de.hub.mse.mcs.experiments.wrappers.clone.EScanWrapper;
import de.hub.mse.mcs.experiments.wrappers.clone.ScanQatWrapper;
import de.hub.mse.mcs.experiments.wrappers.emf.EMFCompareWrapper;
import de.hub.mse.mcs.experiments.wrappers.mcs.MCSCompareWrapperInt;
import de.hub.mse.mcs.experiments.wrappers.mcs.MCSCompareWrapperSep;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResource;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.trace.TracePackage;
import org.eclipse.uml2.uml.UMLPackage;

import GraphConstraint.GraphConstraintPackage;
import de.hub.mse.henshin.mutator.edit.Mutator;
import de.hub.mse.mcs.experiments.wrappers.*;
import de.hub.mse.mcs.experiments.wrappers.clone.EScanWrapper;
import de.hub.mse.mcs.experiments.wrappers.clone.ScanQatWrapper;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker.CandidateCheckerFeatures;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import pivot.PivotPackage;
import symmetric.SymmetricPackage;

/**
 * The Runner runs a comparison of all Henshin rules in the directories with EMFCompare and the MCS algorithms. The results of each benchmark set
 * are written to the given result directory, after all rules in the set have been compared with each other.
 */
public class ExperimentRunner {
    // The directory where all benchmark sets can be found
    private static String henshinBaseDir;
    // A list of the names of the benchmark sets, e.g. names of the folders that contain the rules
    private static String[] benchmarkSets;
    // Path to a directory where the results of all sets can be stored.
    private static String pathToResultDir;
    public static String pathToMutationDir;
    // Path to a directory where the runtimes of all sets are stored.
    public static String pathToResultRuntimes;
    // folder that is currently investigated by the algorithms
    public static String folderName;
    // If the connected maximum common subgraph should be used instead
    private static boolean useMCCS = false;
    // Number of comparison iteration per rule
    private static int runsPerRule = 10;
    // Number of mutations that are considered per rule
    private static List<Integer> mutateCounts = List.of(0, 1, 3);;
    //Time in microseconds, till a run with a method gets a timeout
    public static long TIMEOUT = 1_000;
    // Time in milliseconds that is used to clean up unused objects and threads
    public static long CLEANUP_TIME = 10;
    public static boolean VERBOSE;
    // False: Repeat the experiments, True: Have a quick test run with lower TIMEOUT and iterations
    private static boolean testRunOnly = false;
    private static boolean validateResults = true;

    static {
        // Initialize the meta-models
        FeatureModelPackage.eINSTANCE.eClass();
        UMLPackage.eINSTANCE.eClass();
        SymmetricPackage.eINSTANCE.eClass();
        GraphConstraintPackage.eINSTANCE.eClass();
        PivotPackage.eINSTANCE.eClass();
        EcorePackage.eINSTANCE.eClass();
        TracePackage.eINSTANCE.eClass();
    }

    public static void main(String... args) throws InterruptedException {
        String[] possibleSets = new String[]{"fmedit-atomic", "fmedit-complex", "fmrecog-atomic", "fmrecog-complex", "ocl2ngc", "umledit-generated", "umledit-manual", "umlrecog-generated", "umlrecog-manual"};
        if (args.length < 1) {
            benchmarkSets = possibleSets;
        } else {
            int setID = Integer.parseInt(args[0]);
            benchmarkSets = new String[]{possibleSets[setID]};
            System.out.println("Running experiments only on " + possibleSets[setID]);
            Thread.sleep(3000);
        }
        // Default values
        henshinBaseDir = ".." + File.separator + "benchmark_sets";
        VERBOSE = false;
        validateResults = false;
        if (testRunOnly) {
            runsPerRule = 1;
            mutateCounts = List.of(0, 1, 3);
            benchmarkSets = new String[]{"fmedit-atomic", "fmedit-complex", "fmrecog-atomic", "fmrecog-complex", "ocl2ngc", "umledit-generated", "umledit-manual", "umlrecog-generated", "umlrecog-manual"};
            TIMEOUT = 100;
            VERBOSE = false;
            useMCCS = false;
            validateResults = true;
        }

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyy_HH-mm-ss");
        String resultDirBase = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Results";
        createDirectory(resultDirBase);
        pathToResultDir = resultDirBase + File.separator + dateTime.format(formatter);
        createDirectory(pathToResultDir);

        run();
        System.out.println("All done!");
    }

    public static void run() {
        ArrayList<CompareWrapper> wrappers = new ArrayList<>();
        String[] mcsModes;
        if (useMCCS) {
            mcsModes = new String[]{"connected", "notconnected"};
        } else {
            mcsModes = new String[]{"notconnected"};
        }

        for (String c : mcsModes) {
            MCSCompareWrapperSep sepstrict = new MCSCompareWrapperSep(c);
            sepstrict.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.SEPARATE, CandidateCheckerFeatures.TYPE_EQUALITY)));
            MCSCompareWrapperSep seplib = new MCSCompareWrapperSep(c);
            seplib.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.SEPARATE, CandidateCheckerFeatures.TYPE_COMPATIBILITY)));

            MCSCompareWrapperInt intstrict = new MCSCompareWrapperInt(c);
            intstrict.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.INTEGRATED, CandidateCheckerFeatures.TYPE_EQUALITY)));
            MCSCompareWrapperInt intlib = new MCSCompareWrapperInt(c);
            intlib.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.INTEGRATED, CandidateCheckerFeatures.TYPE_COMPATIBILITY)));
            MCSCompareWrapperInt intstrictstrict = new MCSCompareWrapperInt(c);
            intstrictstrict.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.INTEGRATED, CandidateCheckerFeatures.TYPE_EQUALITY, CandidateCheckerFeatures.ACTION_EQUALITY)));
            MCSCompareWrapperInt intlibstrict = new MCSCompareWrapperInt(c);
            intlibstrict.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.INTEGRATED, CandidateCheckerFeatures.TYPE_COMPATIBILITY, CandidateCheckerFeatures.ACTION_EQUALITY)));
            wrappers.addAll(List.of(intstrict, intlib, intstrictstrict, intlibstrict, sepstrict, seplib));
        }
        wrappers.add(new EMFCompareWrapper("noUIDS"));
        wrappers.add(new EMFCompareWrapper("UIDS"));
        wrappers.add(new ConQatWrapper());
        wrappers.add(new EScanWrapper());
        wrappers.add(new ScanQatWrapper());

        for (String foldername : benchmarkSets) {
            ExperimentRunner.folderName = foldername;
            System.out.println("------------------------------------------------------------------------------------------------------");
            System.out.println("------------------------------------------------------------------------------------------------------");

            for (int mutateTimes : mutateCounts) {
                pathToMutationDir = pathToResultDir + File.separator + mutateTimes + " mutations";
                createDirectory(pathToMutationDir);
                pathToResultRuntimes = pathToMutationDir + File.separator + "runtimes";
                createDirectory(pathToResultRuntimes);
                for (CompareWrapper w : wrappers) {
                    w.nextFolder();
                }

                File folder = new File(henshinBaseDir + File.separator + foldername);
                for (File file : Objects.requireNonNull(folder.listFiles())) {
                    String name = file.getName();
                    if (name.endsWith("henshin")) {
                        for (CompareWrapper w : wrappers) {
                            w.nextRule(name);
                        }
                        for (int round = 1; round <= runsPerRule; round++) {
                            System.out.println();
                            System.out.println("--------------------------------------------------------------------------------");
                            System.out.println("Starting round " + round + " of " + runsPerRule + " for " + name +
                                    " with " + mutateTimes + " mutations.");
                            boolean success;
                            do {
                                try {
									for (CompareWrapper w : wrappers) {
										boolean hadToReset = w.validateResult(round);
										if (hadToReset) {
											System.err.println("Had to Reset the last result of " + w.getName());
										}
									}
									HenshinResourceSet resourceSet = new HenshinResourceSet(henshinBaseDir + File.separator + foldername);
									HenshinResourceSet resourceSetCopy = new HenshinResourceSet(henshinBaseDir + File.separator + foldername);
                                    HenshinResource resource = (HenshinResource) resourceSet.getResource(name);
                                    HenshinResource resourceCopy = (HenshinResource) resourceSetCopy.getResource(name);
                                    Module module = resourceSet.getModule(name);
                                    Module modulecopy = resourceSetCopy.getModule(name);

                                    RulePermutator.permutator(resource);
                                    RulePermutator.permutator(resourceCopy);
                                    Rule rule = (Rule) modulecopy.getUnits().get(0);

                                    Mutator mutator = new Mutator(rule, mutateTimes, !VERBOSE);
                                    mutator.mutate();

                                    ArrayList<CompareWrapper> shuffledWrappers = new ArrayList<>(wrappers);
                                    Collections.shuffle(shuffledWrappers);
                                    for (CompareWrapper w : shuffledWrappers) {
                                        if (VERBOSE) {
                                            System.out.println("Starting " + w.getName());
                                        }
                                        w.nextComparison(resource, resourceCopy, module, modulecopy);
                                        w.compare();
                                    }
                                    success = true;
                                } catch (Exception e) {
                                    System.err.println("Exception caught due to mutation into an inconsistent " +
                                            "rule state: " + e.getMessage());
                                    success = false;

                                    System.out.println("Restarting round " + round + " of " + runsPerRule + " for " + name +
                                            " with " + mutateTimes + " mutations.");
                                }
                            } while (!success);

                        }

                        for (CompareWrapper w : wrappers) {
                            w.updateNodeAndEdgeCount(wrappers);
                            if (validateResults && mutateTimes == 0) {
                                w.getResult().validate();
                            }
                            w.print();
                            w.appendRuleResults();
                        }
                    }
                }

                StringBuilder avgBuilder = new StringBuilder(ExperimentResult.header());
                StringBuilder latexBuilder = new StringBuilder();

                for (CompareWrapper w : wrappers) {
                    w.writeCSVs();
                    avgBuilder.append(ExperimentResult.getAvgOverLargeSet(w.getResults(), w.getLongName()).toLine());
                    latexBuilder.append(ExperimentResult.getAvgOverLargeSet(w.getResults(), w.getLongName()).toLatexLine())
                            .append(" \\\\\n");
                }
                CompareWrapper.writeCSVFile(pathToMutationDir + File.separator + foldername + "_AVG.csv", avgBuilder.toString());
                CompareWrapper.writeCSVFile(pathToMutationDir + File.separator + foldername + "_AVG.txt", latexBuilder.toString());
            }
        }
    }


    private static void createDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists())
            directory.mkdirs();
    }

}
