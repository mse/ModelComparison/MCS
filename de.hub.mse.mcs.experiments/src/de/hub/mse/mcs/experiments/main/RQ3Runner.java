package de.hub.mse.mcs.experiments.main;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import de.hub.mse.mcs.experiments.wrappers.CompareWrapper;
import de.hub.mse.mcs.experiments.wrappers.mcs.MCSCompareWrapperInt;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.resource.HenshinResource;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;
import org.eclipse.emf.henshin.trace.TracePackage;
import org.eclipse.uml2.uml.UMLPackage;

import GraphConstraint.GraphConstraintPackage;
import de.hub.mse.modelcomp.mcsimpl.FeatureBasedCandidateChecker.CandidateCheckerFeatures;
import de.imotep.featuremodel.variability.metamodel.FeatureModel.FeatureModelPackage;
import pivot.PivotPackage;
import symmetric.SymmetricPackage;

/**
 * The Runner runs a comparison of all Henshin rules in the directories with EMFCompare and the MCS algorithms. The results of each benchmark set
 * are written to the given result directory, after all rules in the set have been compared with each other.
 */
public class RQ3Runner {
    // The directory where all benchmark sets can be found
    private static String henshinBaseDir;
    // A list of the names of the benchmark sets, e.g. names of the folders that contain the rules
    private static String[] benchmarkSets;
    // Path to a directory where the results of all sets can be stored.
    private static String pathToResultDir;

    // Path to a directory where the runtimes of all sets are stored.
    private static String pathToResultRuntimes;
    // Number of comparison iteration per rule
    private static int totalRuns;
    //Time in microseconds, till next method is tried
    public static long TIMEOUT;
    // Time in milliseconds that is used to clean up unused objects and threads
    private static boolean VERBOSE;

    private static boolean validateResults = true;
    // The minimum timout that could be used
    private static int timeoutMin = 1;
    // The maximum timeout that could be used (inclusive)
    private static int timeoutMax = 1000;
    // 1ms = Every value between min and max could be taken randomly, 2ms = Every other value could be taken, etc..
    private static int timeoutStep = 1;
    // List for storing the possible timeout values
    private static List<Integer> timeouts;

    static {
        // Initialize the meta-models
        FeatureModelPackage.eINSTANCE.eClass();
        UMLPackage.eINSTANCE.eClass();
        SymmetricPackage.eINSTANCE.eClass();
        GraphConstraintPackage.eINSTANCE.eClass();
        PivotPackage.eINSTANCE.eClass();
        EcorePackage.eINSTANCE.eClass();
        TracePackage.eINSTANCE.eClass();
    }

    public static void main(String... args) {
        // Default values
        henshinBaseDir = ".." + File.separator + "benchmark_sets";
        totalRuns = 10000;
        VERBOSE = false;
        validateResults = false;
        benchmarkSets = new String[]{"timeout-rules"};
        timeouts = getTimeouts();

        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyy_HH-mm-ss");
        String resultDirBase = System.getProperty("user.dir") + File.separator + ".." + File.separator + "Results";
        createDirectory(resultDirBase);
        pathToResultDir = resultDirBase + File.separator + dateTime.format(formatter) + "-TimeoutBenchmark";
        createDirectory(pathToResultDir);

        run();
        System.out.println("All done!");
    }

    public static void run() {
        CompareWrapper mcsWrapper = new MCSCompareWrapperInt("notconnected");
        mcsWrapper.setFeatures(new ArrayList<>(List.of(CandidateCheckerFeatures.INTEGRATED, CandidateCheckerFeatures.TYPE_EQUALITY)));

        for (String foldername : benchmarkSets) {
            ExperimentRunner.folderName = foldername;
            System.out.println("------------------------------------------------------------------------------------------------------");
            System.out.println("------------------------------------------------------------------------------------------------------");

            createDirectory(pathToResultDir);
            pathToResultRuntimes = pathToResultDir + File.separator + "runtimes";
            createDirectory(pathToResultRuntimes);

            // Initialize the next folder
            mcsWrapper.nextFolder();
            StringBuilder resultStringBuilder = new StringBuilder("runID & Rule & F-Measure & Timeout");

            Map<String, HenshinResourceSet> resourceMapA = new HashMap<>();
            Map<String, HenshinResourceSet> resourceMapB = new HashMap<>();

            File folder = new File(henshinBaseDir + File.separator + foldername);
            for (File file : Objects.requireNonNull(folder.listFiles())) {
                String name = file.getName();
                if (name.endsWith("henshin")) {
                    boolean permuteSuccess;
                    do {
                        try {
                            HenshinResourceSet resourceSet = new HenshinResourceSet(henshinBaseDir + File.separator + foldername);
                            HenshinResourceSet resourceSetCopy = new HenshinResourceSet(henshinBaseDir + File.separator + foldername);
                            HenshinResource resource = (HenshinResource) resourceSet.getResource(name);
                            HenshinResource resourceCopy = (HenshinResource) resourceSetCopy.getResource(name);

                            RulePermutator.permutator(resource);
                            RulePermutator.permutator(resourceCopy);

                            // Add the permuted resource sets to the lists
                            resourceMapA.put(name, resourceSet);
                            resourceMapB.put(name, resourceSetCopy);
                            permuteSuccess = true;
                        } catch (Exception e) {
                            System.err.println("Exception caught: " + e.getMessage());
                            permuteSuccess = false;
                            System.out.println("Restarting resource permutation for " + name +
                                    ".");
                        }
                    } while (!permuteSuccess);
                }
            }

            for (int round = 1; round <= totalRuns; round++) {
                TIMEOUT = getRandomTimeout(timeouts);
                mcsWrapper.setTimeout(TIMEOUT);
                System.out.println();
                System.out.println("--------------------------------------------------------------------------------");
                System.out.println("Starting round " + round + " of " + totalRuns + " with a timeout of " + TIMEOUT + "ms.");


                for (File file : Objects.requireNonNull(folder.listFiles())) {
                    String name = file.getName();
                    if (name.endsWith("henshin")) {
                        // Set the next rule
                        mcsWrapper.nextRule(name);

                        boolean success;
                        do {
                            try {
                                HenshinResourceSet resourceSet = resourceMapA.get(name);
                                HenshinResourceSet resourceSetCopy = resourceMapB.get(name);
                                // Load the Resource
                                HenshinResource resource = (HenshinResource) resourceSet.getResource(name);
                                HenshinResource resourceCopy = (HenshinResource) resourceSetCopy.getResource(name);
                                Module module = resourceSet.getModule(name);
                                Module modulecopy = resourceSetCopy.getModule(name);

                                if (VERBOSE) {
                                    System.out.println("Starting " + mcsWrapper.getName());
                                }
                                mcsWrapper.nextComparison(resource, resourceCopy, module, modulecopy);
                                mcsWrapper.compare();

                                success = true;
                            } catch (Exception e) {
                                System.err.println("Exception caught: " + e.getMessage());
                                //if (VERBOSE) {
                                e.printStackTrace();
                                //}
                                success = false;

                                System.out.println("Restarting round " + round + " of " + totalRuns + " for " + name +
                                        ".");
                            }
                        } while (!success);

                        if (validateResults) {
                            mcsWrapper.getResult().validate();
                        }
                        mcsWrapper.print();
                        resultStringBuilder.append(mcsWrapper.getResult().toTimeoutBenchmarkLine(round, TIMEOUT));
                    }
                }
                CompareWrapper.writeCSVFile(pathToResultDir + File.separator + foldername + "_TIME.txt", resultStringBuilder.toString());
            }
        }
    }

    private static List<Integer> getTimeouts() {
        List<Integer> timeouts = new ArrayList<>();
        for (int i = timeoutMin; i <= timeoutMax; i += timeoutStep) {
            timeouts.add(i);
        }
        // The cance of taking a timeout below 50 is higher to get a smoother curve, in principle we could've
        // taken every value between min and max once.
        for (int i = timeoutMin; i <= 50; i += timeoutStep) {
            timeouts.add(i);
        }
        for (int i = timeoutMin; i <= 50; i += timeoutStep) {
            timeouts.add(i);
        }
        for (int i = timeoutMin; i <= 50; i += timeoutStep) {
            timeouts.add(i);
        }
        for (int i = timeoutMin; i <= 50; i += timeoutStep) {
            timeouts.add(i);
        }
        return timeouts;
    }

    private static void createDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists())
            directory.mkdirs();
    }

    private static int getRandomTimeout(List<Integer> timeouts) {
        Random rand = new Random();
        return timeouts.get(rand.nextInt(timeouts.size()));
    }

}
