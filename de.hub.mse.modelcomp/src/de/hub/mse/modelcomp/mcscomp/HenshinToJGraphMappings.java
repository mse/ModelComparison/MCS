package de.hub.mse.modelcomp.mcscomp;

import java.util.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Node;

import de.hub.mse.modelcomp.mcsimpl.HenshinElement;

/**
 * Container class that manages the mappings of nodes and edges from Henshin to JGraphT objects
 *
 */
public class HenshinToJGraphMappings {
	private Map<Node, HenshinElement> henshinToJGNodeMap;
	private Map<Edge, HenshinElement> henshinToJGEdgeMap;
	private Map<HenshinElement, Node> jGtoHenshinNodeMap;
	private Map<HenshinElement, Edge> jGtoHenshinEdgeMap;

	public HenshinToJGraphMappings(Map<Node, HenshinElement> nodeMap, Map<Edge, HenshinElement> edgeMap) {
		this.henshinToJGNodeMap = nodeMap;
		this.henshinToJGEdgeMap = edgeMap;
		this.jGtoHenshinNodeMap = new HashMap<>();
		this.jGtoHenshinEdgeMap = new HashMap<>();
		
		this.henshinToJGNodeMap.keySet().forEach(n -> jGtoHenshinNodeMap.put(henshinToJGNodeMap.get(n), n));
		this.henshinToJGEdgeMap.keySet().forEach(e -> jGtoHenshinEdgeMap.put(henshinToJGEdgeMap.get(e), e));
	}
	
	public HenshinElement getNode(Node node) {
		return henshinToJGNodeMap.get(node);
	}
	
	public HenshinElement getEdge(Edge edge) {
		return henshinToJGEdgeMap.get(edge);
	}

	public Collection<EObject> getAllContents() {
		Collection<EObject> contents = new HashSet<>();

		contents.addAll(henshinToJGNodeMap.keySet());
		contents.addAll(henshinToJGEdgeMap.keySet());
		return contents;
	}

	public Node getNode(HenshinElement ele) {
		return jGtoHenshinNodeMap.get(ele);
	}
	
	public Edge getEdge(HenshinElement ele) {
		return jGtoHenshinEdgeMap.get(ele);
	}
	
	public Set<HenshinElement> getJGNodeKeySet(){
		return jGtoHenshinNodeMap.keySet();
	}
	
	public Set<HenshinElement> getJGEdgeKeySet(){
		return jGtoHenshinEdgeMap.keySet();
	}
}
