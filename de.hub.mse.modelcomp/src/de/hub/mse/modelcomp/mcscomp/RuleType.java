package de.hub.mse.modelcomp.mcscomp;

public enum RuleType {
    LHS,
    RHS,
    INTEGRATED
}
