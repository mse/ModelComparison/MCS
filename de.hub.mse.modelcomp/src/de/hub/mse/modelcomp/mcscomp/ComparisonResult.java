package de.hub.mse.modelcomp.mcscomp;

import de.hub.mse.mcs.data.Pair;
import de.hub.mse.modelcomp.mcsimpl.HenshinElement;
import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedPseudograph;

import java.util.HashSet;

public class ComparisonResult {
    private DirectedPseudograph<HenshinElement, HenshinElement> graphA;
    private DirectedPseudograph<HenshinElement, HenshinElement> graphB;
    private HenshinToJGraphMappings mapA;
    private HenshinToJGraphMappings mapB;
    // The MCS of graphA and graphB
    private Graph<HenshinElement, HenshinElement> mcs;
    // The HashSets of Pairs mapping nodes and edges of the two graphs
    private HashSet<Pair<HenshinElement>> nodeMapping;
    private HashSet<Pair<HenshinElement>> edgeMapping;

    public ComparisonResult(DirectedPseudograph<HenshinElement, HenshinElement> graphA,
                            DirectedPseudograph<HenshinElement, HenshinElement> graphB,
                            HenshinToJGraphMappings mapA,
                            HenshinToJGraphMappings mapB) {
        this.graphA = graphA;
        this.graphB = graphB;
        this.mapA = mapA;
        this.mapB = mapB;
    }

    public DirectedPseudograph<HenshinElement, HenshinElement> getGraphA() {
        return graphA;
    }

    public DirectedPseudograph<HenshinElement, HenshinElement> getGraphB() {
        return graphB;
    }

    public HenshinToJGraphMappings getHenshinToJGraphMapA() {
        return mapA;
    }

    public HenshinToJGraphMappings getHenshinToJGraphMapB() {
        return mapB;
    }

    public Graph<HenshinElement, HenshinElement> getMcs() {
        return mcs;
    }

    public HashSet<Pair<HenshinElement>> getNodeAtoBMapping() {
        return nodeMapping;
    }

    public HashSet<Pair<HenshinElement>> getEdgeAtoBMapping() {
        return edgeMapping;
    }

    void setNodeMapping(HashSet<Pair<HenshinElement>> nodeMapping) {
        this.nodeMapping = nodeMapping;
    }

    void setMcs(Graph<HenshinElement, HenshinElement> mcs) {
        this.mcs = mcs;
    }

    void setEdgeMapping(HashSet<Pair<HenshinElement>> edgeMapping) {
        this.edgeMapping = edgeMapping;
    }
}
