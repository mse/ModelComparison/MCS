package de.hub.mse.modelcomp.mcscomp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Mapping;
import org.eclipse.emf.henshin.model.MappingList;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResource;
import org.jgrapht.graph.DirectedPseudograph;

import de.hub.mse.modelcomp.mcsimpl.HenshinElement;

/**
 * A parser for parsing a Henshin Rule to a JGraphT graph representation. 
 *
 */
public class HenshinToJGraphParser {

	private HenshinResource resource;
	private Rule rule;

	private DirectedPseudograph<HenshinElement, HenshinElement> jGraphLHS;
	private DirectedPseudograph<HenshinElement, HenshinElement> jGraphRHS;
	private DirectedPseudograph<HenshinElement, HenshinElement> jGraphIntegrated;

	private HashMap<Node, HenshinElement> lhsnodes_map;
	private HashMap<Node, HenshinElement> rhsnodes_map;
	private HashMap<Edge, HenshinElement> lhsedges_map;
	private HashMap<Edge, HenshinElement> rhsedges_map;
	
	private HashMap<HenshinElement, HenshinElement> rhsToLhsNodeMap;

	private HashMap<Node, Node> multimapping_lhs;
	private HashMap<Node, Node> multimapping_rhs;

	private boolean integratedGraphCreated;

	/**
	 * Initialize a Converter for the given rule and resource.
	 * 
	 * @param rule
	 * @param resource
	 */
	public HenshinToJGraphParser(Rule rule, HenshinResource resource) {
		this.rule = rule;
		this.resource = resource;
		this.integratedGraphCreated = false;
		createLHSandRHS();
		createIntegratedGraph();
	}

	public DirectedPseudograph<HenshinElement, HenshinElement> getLHS() {
		return this.jGraphLHS;
	}

	public DirectedPseudograph<HenshinElement, HenshinElement> getRHS() {
		return this.jGraphRHS;
	}

	public DirectedPseudograph<HenshinElement, HenshinElement> getIntegratedGraph() {
		if (!integratedGraphCreated)
			createIntegratedGraph();
		return this.jGraphIntegrated;
	}

	public Map<Node, HenshinElement> getLHSnodesMap() {
		return lhsnodes_map;
	}

	public Map<Node, HenshinElement> getRHSnodesMap() {
		return rhsnodes_map;
	}
	
	public Map<Node, HenshinElement> getINTnodesMap(){
		HashMap<Node, HenshinElement> integratednodes_map = new HashMap<>();
		for (Node node : lhsnodes_map.keySet()) {
			HenshinElement element = lhsnodes_map.get(node);
			if (jGraphIntegrated.containsVertex(element)) {
				integratednodes_map.put(node, element);
			}
		}
		
		for (Node node: rhsnodes_map.keySet()) {
			HenshinElement element = rhsnodes_map.get(node);
			if (jGraphIntegrated.containsVertex(element)) {
				integratednodes_map.put(node, element);
			}
		}
		return integratednodes_map;
	}

	public Map<Edge, HenshinElement> getLHSedgesMap() {
		return lhsedges_map;
	}

	public Map<Edge, HenshinElement> getRHSedgesMap() {
		return rhsedges_map;
	}
	
	public Map<Edge, HenshinElement> getINTedgesMap(){
		HashMap<Edge, HenshinElement> integratededges_map = new HashMap<>();
		for (Edge edge : lhsedges_map.keySet()) {
			HenshinElement element = lhsedges_map.get(edge);
			if (jGraphIntegrated.containsEdge(element)) {
				integratededges_map.put(edge, element);
			}
		}
		
		for (Edge edge: rhsedges_map.keySet()) {
			HenshinElement element = rhsedges_map.get(edge);
			if (jGraphIntegrated.containsEdge(element)) {
				integratededges_map.put(edge, element);
			}
		}
		return integratededges_map;
	}

	private void createLHSandRHS() {

		// load LHS and RHS graphs
		Graph henshinGraphLHS = rule.getLhs();
		Graph henshinGraphRHS = rule.getRhs();

		// Create instances for LHS graph and maps
		this.jGraphLHS = new DirectedPseudograph<>(HenshinElement.class);
		lhsnodes_map = new HashMap<>();
		lhsedges_map = new HashMap<>();

		// Fill LHS JgraphT graph and maps
		initilializeNodesAndEdges(henshinGraphLHS, lhsnodes_map, lhsedges_map, jGraphLHS);

		// Create instances for RHS JgraphT graph and maps
		this.jGraphRHS = new DirectedPseudograph<>(HenshinElement.class);
		rhsnodes_map = new HashMap<>();
		rhsedges_map = new HashMap<>();

		// Fill RHS JgraphT graph and maps
		initilializeNodesAndEdges(henshinGraphRHS, rhsnodes_map, rhsedges_map, jGraphRHS);

		// Create the mapping for the multirule
		multimapping_lhs = new HashMap<>();
		multimapping_rhs = new HashMap<>();
		for (Node n : lhsnodes_map.keySet()) {
			multimapping_lhs.put(n, n);
		}
		for (Node n : rhsnodes_map.keySet()) {
			multimapping_rhs.put(n, n);
		}
		List<Rule> multirules = rule.getAllMultiRules();
		for (Rule r : multirules) {
			handleMultirule(r);
		}

		List<NestedCondition> nc = henshinGraphLHS.getNestedConditions();
		for (NestedCondition n : nc) {
			handleNestedCondition(n, lhsnodes_map, lhsedges_map);
		}
		calculateRhstoLhsMap(rule.getMappings());
	}
	
	/**
	 * Convert the nodes and edges of the given Henshin Graph into
	 * StringLabeledObjects. Afterwards the converted objects are added to the given
	 * DirectedPseudoGraph and the mappings of HenshinNode -> JGraphTNode,
	 * HenshinEdge -> JGraphTMap are created.
	 * 
	 * @param henshinGraph The Henshin Graph that is to be converted
	 * @param nodeMap      An instance of HashMap in which the mappings of Henshin
	 *                     to JGraphT Nodes are put
	 * @param edgeMap      An instance of HashMap in which the mappings of Henshin
	 *                     to JGraphT Edges are put
	 * @param graph        An instance of DirectedPseudograph to which the converted
	 *                     nodes and edges are added
	 */
	private void initilializeNodesAndEdges(Graph henshinGraph, HashMap<Node, HenshinElement> nodeMap,
			HashMap<Edge, HenshinElement> edgeMap,
			DirectedPseudograph<HenshinElement, HenshinElement> graph) {
		// Load the list of nodes
		List<Node> henshinNodes = henshinGraph.getNodes();

		// Convert all Henshin Nodes to StringLabeledObjects for the graph
		for (Node henshinNode : henshinNodes) {
			HenshinElement jgraphtNode;
			
			if (henshinNode.getType().getName() == null) {
				System.err.println("Found a node without type! Setting empty String as type...");
				jgraphtNode = new HenshinElement("");
			} else {
				jgraphtNode = new HenshinElement(henshinNode.getType().getName());
			}
			jgraphtNode.setName(henshinNode.toString().substring(5));
			jgraphtNode.addID(resource.getID(henshinNode));
			jgraphtNode.setActionType(henshinNode.getAction());
			
			nodeMap.put(henshinNode, jgraphtNode);
			graph.addVertex(jgraphtNode);
		}

		// Get the outgoing edges of all Henshin nodes and convert them to
		// StringLabeledObjects for the graph
		// Because we need the StringLabeledObjects of the nodes, this can only be done
		// after all nodes have been converted
		for (Node henshinNode : henshinNodes) {
			HenshinElement jgraphtEdge;
			for (Edge henshinEdge : henshinNode.getOutgoing()) {
				if (henshinNode.getType().getName() == null) {
					System.err.println("Found edge without type! Setting empty String as type and name...");
					jgraphtEdge = new HenshinElement("");
					jgraphtEdge.setName("");
				} else {
					jgraphtEdge = new HenshinElement(henshinEdge.getType().getName());
					jgraphtEdge.setName(henshinEdge.getType().getName());
				}
				
				jgraphtEdge.addID(resource.getID(henshinEdge));
				jgraphtEdge.setActionType(henshinEdge.getAction());
				
				edgeMap.put(henshinEdge, jgraphtEdge);
				graph.addEdge(nodeMap.get(henshinEdge.getSource()), nodeMap.get(henshinEdge.getTarget()), jgraphtEdge);
			}
		}
	}

	private void createIntegratedGraph() {

		jGraphIntegrated = new DirectedPseudograph<>(HenshinElement.class);
		//MappingList mapping = rule.getMappings();

		// copy the nodes and add the action type
		jGraphLHS.vertexSet().forEach(v -> jGraphIntegrated.addVertex(v));
		// Add all nodes of the RHS graph that are not part of the LHS graph -> Action.Type != null
		jGraphRHS.vertexSet().stream().filter(v -> v.getActionType() != null).forEach(v -> jGraphIntegrated.addVertex(v));
		
		jGraphLHS.edgeSet().forEach(e -> jGraphIntegrated.addEdge(jGraphLHS.getEdgeSource(e), 
																	jGraphLHS.getEdgeTarget(e), 
																	e));
		// Add the edges of the RHS graph
		for (HenshinElement e : jGraphRHS.edgeSet()) {
			if (e.getActionType() != null) {
				HenshinElement sourceRHS;
				HenshinElement targetRHS;
				
				if (jGraphIntegrated.containsVertex(jGraphRHS.getEdgeSource(e))) {
					sourceRHS = jGraphRHS.getEdgeSource(e);
				} else {
					// The source of the edge is the corresponding node object of the LHS graph
					HenshinElement ele = jGraphRHS.getEdgeSource(e);
					sourceRHS = rhsToLhsNodeMap.get(ele);
				}
				
				if (jGraphIntegrated.containsVertex(jGraphRHS.getEdgeTarget(e))) {
					targetRHS = jGraphRHS.getEdgeTarget(e);
				} else {
					// The target of the edge is the corresponding node object of the LHS graph
					HenshinElement ele = jGraphRHS.getEdgeTarget(e);
					targetRHS = rhsToLhsNodeMap.get(ele);
				}
				try {
					jGraphIntegrated.addEdge(sourceRHS, targetRHS, e);
				} catch(Exception ex) {
					throw new NullPointerException("The node for the given edge does not exist!");
				}
			}
		}

		this.integratedGraphCreated = true;
	}

	private void handleNestedCondition(NestedCondition nc, HashMap<Node, HenshinElement> nodeMap,
			HashMap<Edge, HenshinElement> edgeMap) {

		Graph ncgraph = nc.getConclusion();

		MappingList mappings = nc.getMappings();

		List<Node> ncnodes = ncgraph.getNodes();
		ArrayList<Node> newNodes = new ArrayList<>();

		// add nodes of the application condition
		for (Node henshinNode : ncnodes) {
			if (!nodeMap.containsKey(mappings.getOrigin(henshinNode))) {
				HenshinElement jgraphtNode;
				
				jgraphtNode = new HenshinElement(henshinNode.getType().getName());
				jgraphtNode.setName(henshinNode.toString().substring(5));
				jgraphtNode.setActionType(henshinNode.getAction());
				jgraphtNode.addID(resource.getID(henshinNode));

				nodeMap.put(henshinNode, jgraphtNode);
				multimapping_lhs.put(henshinNode, henshinNode);
				jGraphLHS.addVertex(jgraphtNode);
				newNodes.add(henshinNode);
			} else {
				// Add the ID of the node to the JGraphT Node
				nodeMap.get(mappings.getOrigin(henshinNode)).addID(resource.getID(henshinNode));
				multimapping_lhs.put(henshinNode, mappings.getOrigin(henshinNode));
			}
		}

		// add edges of the application condition
		for (Node henshinNode : ncnodes) {
			HenshinElement jgraphtEdge;
			for (Edge henshinEdge : henshinNode.getOutgoing()) {
				if (newNodes.contains(henshinEdge.getTarget()) || newNodes.contains(henshinEdge.getSource())) {
					jgraphtEdge = new HenshinElement(henshinEdge.getType().getName());

					jgraphtEdge.setName(henshinEdge.getType().getName());
					jgraphtEdge.setActionType(henshinEdge.getAction());
					jgraphtEdge.addID(resource.getID(henshinEdge));

					edgeMap.put(henshinEdge, jgraphtEdge);

					if (newNodes.contains(henshinEdge.getTarget()) && newNodes.contains(henshinEdge.getSource())) {
						// If both nodes are part of the nested condition
						jGraphLHS.addEdge(nodeMap.get(henshinEdge.getSource()), nodeMap.get(henshinEdge.getTarget()),
								jgraphtEdge);
					} else if (newNodes.contains(henshinEdge.getSource())) {
						// If only the source node is part of the nested condition
						jGraphLHS.addEdge(nodeMap.get(henshinEdge.getSource()),
								nodeMap.get(mappings.getOrigin(henshinEdge.getTarget())), jgraphtEdge);
					} else if (newNodes.contains(henshinEdge.getTarget())) {
						// If only the target node is part of the nested condition
						jGraphLHS.addEdge(nodeMap.get(mappings.getOrigin(henshinEdge.getSource())),
								nodeMap.get(henshinEdge.getTarget()), jgraphtEdge);
					}
					
				} else {
					if (jGraphLHS.containsEdge(nodeMap.get(mappings.getOrigin(henshinEdge.getSource())),
							nodeMap.get(mappings.getOrigin(henshinEdge.getTarget())))) {
						jGraphLHS
								.getEdge(nodeMap.get(mappings.getOrigin(henshinEdge.getSource())),
										nodeMap.get(mappings.getOrigin(henshinEdge.getTarget())))
								.addID(resource.getID(henshinEdge));
					}
				}
			}
		}
	}

	private void handleMultirule(Rule multirule) {

		Graph lhs_henshin = multirule.getLhs();
		Graph rhs_henshin = multirule.getRhs();

		MappingList mappings = multirule.getMultiMappings();

		List<Node> lhsnodes = lhs_henshin.getNodes();
		List<Node> rhsnodes = rhs_henshin.getNodes();

		ArrayList<Node> newNodes_LHS = new ArrayList<>();
		ArrayList<Node> newNodes_RHS = new ArrayList<>();

		// add multinodes
		addNodesToJGraph(mappings, lhsnodes, newNodes_LHS, lhsnodes_map, multimapping_lhs, jGraphLHS);

		addNodesToJGraph(mappings, rhsnodes, newNodes_RHS, rhsnodes_map, multimapping_rhs, jGraphRHS);

		// add edges going to or from the multinodes
		for (Node n : lhsnodes) {
			List<Edge> outgoing = n.getOutgoing();
			for (Edge e : outgoing) {
				if (newNodes_LHS.contains(e.getTarget()) || newNodes_LHS.contains(e.getSource())) {
					handleEdge(e, lhsedges_map, jGraphLHS, lhsnodes_map, multimapping_lhs);
				} else {
					jGraphLHS.getEdge(lhsnodes_map.get(multimapping_lhs.get(e.getSource())),
							lhsnodes_map.get(multimapping_lhs.get(e.getTarget()))).addID(resource.getID(e));
				}
			}
		}
		
		for (Node n : rhsnodes) {
			List<Edge> outgoing = n.getOutgoing();
			for (Edge e : outgoing) {
				if (newNodes_RHS.contains(e.getTarget()) || newNodes_RHS.contains(e.getSource())) {
					handleEdge(e, rhsedges_map, jGraphRHS, rhsnodes_map, multimapping_rhs);
				} else {
					Node n1 = e.getSource();
					Node n1real = multimapping_rhs.get(n1);
					HenshinElement n1slo = rhsnodes_map.get(n1real);
					Node n2 = e.getTarget();
					Node n2real = multimapping_rhs.get(n2);
					HenshinElement n2slo = rhsnodes_map.get(n2real);
					HenshinElement edge = jGraphRHS.getEdge(n1slo, n2slo);
					edge.addID(resource.getID(e));
				}
			}
		}
		calculateRhstoLhsMap(multirule.getMappings());
	}

	private void handleEdge(Edge e, HashMap<Edge, HenshinElement> lhsedges_map, DirectedPseudograph<HenshinElement, HenshinElement> jGraphLHS, HashMap<Node, HenshinElement> lhsnodes_map, HashMap<Node, Node> multimapping_lhs) {
		HenshinElement lhsedge;
		lhsedge = new HenshinElement(e.getType().getName());

		lhsedge.setName(e.getType().getName());
		lhsedge.addID(resource.getID(e));
		lhsedge.setActionType(e.getAction());

		lhsedges_map.put(e, lhsedge);
		jGraphLHS.addEdge(lhsnodes_map.get(multimapping_lhs.get(e.getSource())),
				lhsnodes_map.get(multimapping_lhs.get(e.getTarget())), lhsedge);
	}

	private void addNodesToJGraph(MappingList mappings, List<Node> lhsnodes, ArrayList<Node> newNodes_LHS, HashMap<Node, HenshinElement> lhsnodes_map, HashMap<Node, Node> multimapping_lhs, DirectedPseudograph<HenshinElement, HenshinElement> jGraphLHS) {
		for (Node n : lhsnodes) {
			if (lhsnodes_map.containsKey(multimapping_lhs.get(mappings.getOrigin(n)))) {
				multimapping_lhs.put(n, multimapping_lhs.get(mappings.getOrigin(n)));
				lhsnodes_map.get(multimapping_lhs.get(n)).addID(resource.getID(n));
			} else {
				HenshinElement newNode = new HenshinElement("Multirule: " + n.getType().getName());
				newNode.setName("Multirule: " + n.toString().substring(5));
				newNode.addID(resource.getID(n));
				newNode.setActionType(n.getAction());

				lhsnodes_map.put(n, newNode);
				jGraphLHS.addVertex(newNode);
				newNodes_LHS.add(n);
				multimapping_lhs.put(n, n);
			}
		}
	}

	private void calculateRhstoLhsMap(MappingList mappings) {
		if (rhsToLhsNodeMap == null) {
			rhsToLhsNodeMap = new HashMap<>();
		}
		
		for (Mapping m : mappings) {
			rhsToLhsNodeMap.put(rhsnodes_map.get(m.getImage()), lhsnodes_map.get(m.getOrigin()));
		}
	}
}
