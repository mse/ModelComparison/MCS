package de.hub.mse.modelcomp.mcsimpl;


import de.hub.mse.mcs.data.LabeledObject;
import org.eclipse.emf.henshin.model.Action;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the LabeledObject interface to represent nodes and edges of Henshin rule graphs.
 *
 */
public class HenshinElement implements LabeledObject {
    
    private String label;
    private String name;
    private ArrayList<String> id;
    private Action.Type actionType;

	public HenshinElement(String label){
        this.label = Objects.requireNonNull(label);
        this.name = "";
        this.id = new ArrayList<String>();
        this.actionType = null;
    }
    
    public String getLabel() {
    	return label;
    }
    
    @Override
	public String getGroupLabel() {
		return label;
	}

    @Override
    public String toString(){
    	return "<< " + actionType + " >>\n" + name + ":";
    }
    
    public void setLabel(String s) {
		this.label = Objects.requireNonNull(s);
    }
    
    public void setName(String s) {
		this.name = Objects.requireNonNull(s);
    }
    
    public String getName() {
    	return name;
    }
    
    public ArrayList<String> getID() {
    	return id;
    }
    
    public void addID(String s) {
    	id.add(s);
    }
    
    public void setActionType(String action) {
    	try {
			this.actionType = Action.Type.parse(action);
		} catch (ParseException e) {
			throw new RuntimeException("Undefined action type: " + action);
		}
    }
    
    public void setActionType(Action.Type action) {
    	this.actionType = action;
    }
    
    public void setActionType(Action action) {
    	this.actionType = action == null ? null : action.getType();
    }
    
    public Action.Type getActionType() {
    	return actionType;
    }
    
    public static HenshinElement getRandomStringLabeledObject(Set<String> alphabet){
		if(alphabet == null || alphabet.isEmpty()){
			throw new RuntimeException("Invalid alphabet");
		}
		int i = (int) (Math.random() * alphabet.size());
		Iterator<String> it = alphabet.iterator();
		for(int c=0; c<i;c++){
			it.next();
		}
		return new HenshinElement(it.next());
    }

}
