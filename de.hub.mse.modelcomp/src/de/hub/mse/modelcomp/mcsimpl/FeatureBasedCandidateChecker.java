package de.hub.mse.modelcomp.mcsimpl;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import de.hub.mse.mcs.algorithm.CandidateChecker;

/**
 * Feature-based implementation of the CandidateChecker interface.
 */
public class FeatureBasedCandidateChecker implements CandidateChecker<HenshinElement, HenshinElement> {

	private List<CandidateCheckerFeatures> features;
	private List<EPackage> metamodels;

	public enum CandidateCheckerFeatures {
		SEPARATE, INTEGRATED, TYPE_EQUALITY, TYPE_COMPATIBILITY, ACTION_EQUALITY
	}
	
	public static String ccfToString(CandidateCheckerFeatures c) {
		switch (c) {
			case SEPARATE: return "";
			case INTEGRATED: return "";
			case TYPE_EQUALITY: return "TE";
			case TYPE_COMPATIBILITY: return "TC";
			case ACTION_EQUALITY: return "AE";
		}
		System.err.println("This Feature is not known in ccfToString(CandidateCheckerFeatures c)");
		return "";
	}

	public FeatureBasedCandidateChecker(List<CandidateCheckerFeatures> features, List<EPackage> metamodels) {
		this.features = features;
		this.metamodels = metamodels;
		
		if (!this.metamodels.contains(EcorePackage.eINSTANCE)) {
			this.metamodels.add(EcorePackage.eINSTANCE);
		}

		checkValidity();
	}

	@Override
	public boolean checkNodes(HenshinElement node1, HenshinElement node2) {
		boolean candidates = true;

		if (features.contains(CandidateCheckerFeatures.TYPE_EQUALITY)) {
			candidates = candidates && equalTypes(node1, node2);
		}
		if (features.contains(CandidateCheckerFeatures.TYPE_COMPATIBILITY)) {
			candidates = candidates && compatibleNodeTypes(node1, node2);
		}
		if (features.contains(CandidateCheckerFeatures.ACTION_EQUALITY)) {
			candidates = candidates && equalActions(node1, node2);
		}

		return candidates;
	}

	@Override
	public boolean checkEdges(HenshinElement edge1, HenshinElement edge2) {
		boolean candidates = true;

		if (features.contains(CandidateCheckerFeatures.TYPE_EQUALITY)) {
			candidates = candidates && equalTypes(edge1, edge2);
		}
		if (features.contains(CandidateCheckerFeatures.TYPE_COMPATIBILITY)) {
			candidates = candidates && compatibleEdgeTypes(edge1, edge2);
		}
		if (features.contains(CandidateCheckerFeatures.ACTION_EQUALITY)) {
			candidates = candidates && equalActions(edge1, edge2);
		}

		return candidates;
	}

	private boolean equalTypes(HenshinElement node1, HenshinElement node2) {
		return node1.getGroupLabel().equals(node2.getGroupLabel());
	}

	private boolean compatibleNodeTypes(HenshinElement node1, HenshinElement node2) {
		if (equalTypes(node1, node2)) {
			return true;
		}

		// Get the node type from metamodels and be sure they are resolved
		
		
		
		EClass nodeType1 = getNodeTypeByName(node1.getGroupLabel());
		EClass nodeType2 = getNodeTypeByName(node2.getGroupLabel());
		if (nodeType1 == null) {
			throw new RuntimeException("Unknown type: " + node1.getGroupLabel());
		}
		if (nodeType2 == null) {
			throw new RuntimeException("Unknown type: " + node2.getGroupLabel());
		}

		// now check supertype relationship
		if (nodeType1.getEAllSuperTypes().contains(nodeType2) || nodeType2.getEAllSuperTypes().contains(nodeType1)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean compatibleEdgeTypes(HenshinElement edge1, HenshinElement edge2) {
		// no type inheritance for edge types in EMF, so we can only check equality
		return edge1.getGroupLabel().equals(edge2.getGroupLabel());
	}

	private boolean equalActions(HenshinElement node1, HenshinElement node2) {
		return node1.getActionType() == node2.getActionType();
	}

	private void checkValidity() {
		// SEPARATE XOR INTEGRATED
		if (features.contains(CandidateCheckerFeatures.SEPARATE) && features.contains(CandidateCheckerFeatures.INTEGRATED)) {
			throw new RuntimeException("Invalid feature combination for candidate checker: " + features);
		}
		if (!features.contains(CandidateCheckerFeatures.SEPARATE) && !features.contains(CandidateCheckerFeatures.INTEGRATED)) {
			throw new RuntimeException("Invalid feature combination for candidate checker: " + features);
		}

		// TYPE_EQUALITY XOR TYPE_COMPATIBILITY
		if (features.contains(CandidateCheckerFeatures.TYPE_EQUALITY) && features.contains(CandidateCheckerFeatures.TYPE_COMPATIBILITY)) {
			throw new RuntimeException("Invalid feature combination for candidate checker: " + features);
		}
		if (!features.contains(CandidateCheckerFeatures.TYPE_EQUALITY) && !features.contains(CandidateCheckerFeatures.TYPE_COMPATIBILITY)) {
			throw new RuntimeException("Invalid feature combination for candidate checker: " + features);
		}

		// ACTION_EQUALITY => INTEGRATED
		if (features.contains(CandidateCheckerFeatures.ACTION_EQUALITY) && !features.contains(CandidateCheckerFeatures.INTEGRATED)) {
			throw new RuntimeException("Invalid feature combination for candidate checker: " + features);
		}
	}

	private EClass getNodeTypeByName(String typeName) {
		String tN = typeName;
		if (typeName.startsWith("Multirule: ")) {
			tN = typeName.substring((new String("Multirule: ")).length());
		}
		
		for (EPackage ePackage : metamodels) {
			EClassifier eClassifier = ePackage.getEClassifier(tN);
			if ((eClassifier != null) && (eClassifier instanceof EClass)) {
				return (EClass) eClassifier;
			}
		}

		return null;
	}
}
